-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 14, 2017 at 12:19 PM
-- Server version: 5.5.50-cll-lve
-- PHP Version: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `k7548658_market`
--

-- --------------------------------------------------------

--
-- Table structure for table `app_version`
--

CREATE TABLE `app_version` (
  `id` bigint(20) NOT NULL,
  `version_code` int(11) NOT NULL,
  `version_name` varchar(50) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `created_at` bigint(20) NOT NULL,
  `last_update` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `app_version`
--

INSERT INTO `app_version` (`id`, `version_code`, `version_name`, `active`, `created_at`, `last_update`) VALUES
(5, 5, '5', 1, 1485795799112, 1485795799112),
(6, 6, '6', 1, 1485795799112, 1485795799112),
(7, 7, '7', 1, 1485795799112, 1485795799112),
(8, 8, '8', 1, 1485795799112, 1485795799112),
(9, 9, '9', 1, 1485795799112, 1485795799112),
(10, 10, '10', 1, 1485795799112, 1485795799112),
(11, 11, '11', 1, 1485795799112, 1485795799112);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` bigint(20) NOT NULL,
  `name` varchar(50) NOT NULL,
  `icon` varchar(100) NOT NULL,
  `draft` tinyint(1) NOT NULL,
  `brief` varchar(100) NOT NULL,
  `color` varchar(7) NOT NULL,
  `priority` int(11) NOT NULL DEFAULT '0',
  `created_at` bigint(20) NOT NULL,
  `last_update` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `icon`, `draft`, `brief`, `color`, `priority`, `created_at`, `last_update`) VALUES
(11, 'Delivery Order TSO, Lt 11 & Samsung', 'Delivery Order Kafetaria TSO.png', 0, 'Pesanan mudah dan cepat Kafetaria TSO lt.11\nPemesanan dilayani jam 09.00 - 11.00 & 14.00 - 16.00', '#66dd6a', 2, 1495150893458, 1495854002700),
(12, 'Herbal dan Makanan Kesehatan', 'Herbal.png', 0, 'Tersedia kurma dan herbal untuk menyambut ramadhan', '#579fda', 3, 1495151016598, 1495336344346),
(13, 'Kue, Snack, Makanan dan Minuman', 'Snack dan Minuman.png', 0, 'Menyambut santap buka puasa dan kue-kue lebaran', '#dddc0b', 1, 1495152413661, 1495680116235),
(14, 'Perlengkapan Kerja & Rumah Tangga', 'Perlengkapan Kerja  Lainnya.png', 0, 'Perlengkapan kerja dan rumah tangga', '#4db1a9', 4, 1495152557687, 1495855524709),
(15, 'Fashion', 'Fashion.png', 0, 'Koleksi baju menarik untuk kamu dan keluarga', '#ff5c5c', 5, 1495152819006, 1497454635749),
(16, 'Properti', 'Property.png', 0, 'Investasi properti untuk kamu', '#4db151', 6, 1495152929269, 1495152929269),
(17, 'Alquran, kitab dan buku-buku lainnya', 'Buku.png', 0, 'Tambah wawasan dengan membaca buku', '#7fc600', 8, 1495153021937, 1495793566794),
(18, 'Travel dan Umroh', 'Travel.png', 0, 'Umroh Bersama MTT', '#ff4747', 10, 1495153115031, 1495793491164),
(19, 'Otomotif', 'Otomotif.png', 0, 'Dapatkan kendaraan layak pakai dengan harga terjangkau', '#269cff', 7, 1495677476934, 1495855786766),
(20, 'Usaha Jasa', 'Usaha Jasa.png', 0, 'Berbagai Jenis Usaha Jasa', '#4db151', 3, 1495793261212, 1495793403906),
(21, 'Kerajinan', 'Kerajinan.png', 0, 'Hasil Kerajinan , Produk Kreatif', '#ff5294', 9, 1496199771596, 1497454121297),
(22, 'Hobi', 'Hobi.png', 0, 'Aneka kebutuhan untuk hobi', '#4db151', 4, 1497454671645, 1497454671645);

-- --------------------------------------------------------

--
-- Table structure for table `config`
--

CREATE TABLE `config` (
  `code` varchar(50) NOT NULL,
  `value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `config`
--

INSERT INTO `config` (`code`, `value`) VALUES
('CURRENCY', 'IDR'),
('FEATURED_NEWS', '5'),
('PHONE', '6281285688212'),
('SHIPPING', '[\"Transfer T-Cash 081211110677\",\"T-Cash Saat Penerimaan\",\"Tunai Saat Penerimaan\"]'),
('TAX', '0');

-- --------------------------------------------------------

--
-- Table structure for table `currency`
--

CREATE TABLE `currency` (
  `id` bigint(20) NOT NULL,
  `code` varchar(3) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `currency`
--

INSERT INTO `currency` (`id`, `code`, `name`) VALUES
(1, 'AFA', 'Afghanistan afghani'),
(2, 'ALL', 'Albanian lek'),
(3, 'DZD', 'Algerian dinar'),
(4, 'AOR', 'Angolan kwanza reajustado'),
(5, 'ARS', 'Argentine peso'),
(6, 'AMD', 'Armenian dram'),
(7, 'AWG', 'Aruban guilder'),
(8, 'AUD', 'Australian dollar'),
(9, 'AZN', 'Azerbaijanian new manat'),
(10, 'BSD', 'Bahamian dollar'),
(11, 'BHD', 'Bahraini dinar'),
(12, 'BDT', 'Bangladeshi taka'),
(13, 'BBD', 'Barbados dollar'),
(14, 'BYN', 'Belarusian ruble'),
(15, 'BZD', 'Belize dollar'),
(16, 'BMD', 'Bermudian dollar'),
(17, 'BTN', 'Bhutan ngultrum'),
(18, 'BOB', 'Bolivian boliviano'),
(19, 'BWP', 'Botswana pula'),
(20, 'BRL', 'Brazilian real'),
(21, 'GBP', 'British pound'),
(22, 'BND', 'Brunei dollar'),
(23, 'BGN', 'Bulgarian lev'),
(24, 'BIF', 'Burundi franc'),
(25, 'KHR', 'Cambodian riel'),
(26, 'CAD', 'Canadian dollar'),
(27, 'CVE', 'Cape Verde escudo'),
(28, 'KYD', 'Cayman Islands dollar'),
(29, 'XOF', 'CFA franc BCEAO'),
(30, 'XAF', 'CFA franc BEAC'),
(31, 'XPF', 'CFP franc'),
(32, 'CLP', 'Chilean peso'),
(33, 'CNY', 'Chinese yuan renminbi'),
(34, 'COP', 'Colombian peso'),
(35, 'KMF', 'Comoros franc'),
(36, 'CDF', 'Congolese franc'),
(37, 'CRC', 'Costa Rican colon'),
(38, 'HRK', 'Croatian kuna'),
(39, 'CUP', 'Cuban peso'),
(40, 'CZK', 'Czech koruna'),
(41, 'DKK', 'Danish krone'),
(42, 'DJF', 'Djibouti franc'),
(43, 'DOP', 'Dominican peso'),
(44, 'XCD', 'East Caribbean dollar'),
(45, 'EGP', 'Egyptian pound'),
(46, 'SVC', 'El Salvador colon'),
(47, 'ERN', 'Eritrean nakfa'),
(48, 'EEK', 'Estonian kroon'),
(49, 'ETB', 'Ethiopian birr'),
(50, 'EUR', 'EU euro'),
(51, 'FKP', 'Falkland Islands pound'),
(52, 'FJD', 'Fiji dollar'),
(53, 'GMD', 'Gambian dalasi'),
(54, 'GEL', 'Georgian lari'),
(55, 'GHS', 'Ghanaian new cedi'),
(56, 'GIP', 'Gibraltar pound'),
(57, 'XAU', 'Gold (ounce)'),
(58, 'XFO', 'Gold franc'),
(59, 'GTQ', 'Guatemalan quetzal'),
(60, 'GNF', 'Guinean franc'),
(61, 'GYD', 'Guyana dollar'),
(62, 'HTG', 'Haitian gourde'),
(63, 'HNL', 'Honduran lempira'),
(64, 'HKD', 'Hong Kong SAR dollar'),
(65, 'HUF', 'Hungarian forint'),
(66, 'ISK', 'Icelandic krona'),
(67, 'XDR', 'IMF special drawing right'),
(68, 'INR', 'Indian rupee'),
(69, 'IDR', 'Indonesian rupiah'),
(70, 'IRR', 'Iranian rial'),
(71, 'IQD', 'Iraqi dinar'),
(72, 'ILS', 'Israeli new shekel'),
(73, 'JMD', 'Jamaican dollar'),
(74, 'JPY', 'Japanese yen'),
(75, 'JOD', 'Jordanian dinar'),
(76, 'KZT', 'Kazakh tenge'),
(77, 'KES', 'Kenyan shilling'),
(78, 'KWD', 'Kuwaiti dinar'),
(79, 'KGS', 'Kyrgyz som'),
(80, 'LAK', 'Lao kip'),
(81, 'LVL', 'Latvian lats'),
(82, 'LBP', 'Lebanese pound'),
(83, 'LSL', 'Lesotho loti'),
(84, 'LRD', 'Liberian dollar'),
(85, 'LYD', 'Libyan dinar'),
(86, 'LTL', 'Lithuanian litas'),
(87, 'MOP', 'Macao SAR pataca'),
(88, 'MKD', 'Macedonian denar'),
(89, 'MGA', 'Malagasy ariary'),
(90, 'MWK', 'Malawi kwacha'),
(91, 'MYR', 'Malaysian ringgit'),
(92, 'MVR', 'Maldivian rufiyaa'),
(93, 'MRO', 'Mauritanian ouguiya'),
(94, 'MUR', 'Mauritius rupee'),
(95, 'MXN', 'Mexican peso'),
(96, 'MDL', 'Moldovan leu'),
(97, 'MNT', 'Mongolian tugrik'),
(98, 'MAD', 'Moroccan dirham'),
(99, 'MZN', 'Mozambique new metical'),
(100, 'MMK', 'Myanmar kyat'),
(101, 'NAD', 'Namibian dollar'),
(102, 'NPR', 'Nepalese rupee'),
(103, 'ANG', 'Netherlands Antillian guilder'),
(104, 'NZD', 'New Zealand dollar'),
(105, 'NIO', 'Nicaraguan cordoba oro'),
(106, 'NGN', 'Nigerian naira'),
(107, 'KPW', 'North Korean won'),
(108, 'NOK', 'Norwegian krone'),
(109, 'OMR', 'Omani rial'),
(110, 'PKR', 'Pakistani rupee'),
(111, 'XPD', 'Palladium (ounce)'),
(112, 'PAB', 'Panamanian balboa'),
(113, 'PGK', 'Papua New Guinea kina'),
(114, 'PYG', 'Paraguayan guarani'),
(115, 'PEN', 'Peruvian nuevo sol'),
(116, 'PHP', 'Philippine peso'),
(117, 'XPT', 'Platinum (ounce)'),
(118, 'PLN', 'Polish zloty'),
(119, 'QAR', 'Qatari rial'),
(120, 'RON', 'Romanian new leu'),
(121, 'RUB', 'Russian ruble'),
(122, 'RWF', 'Rwandan franc'),
(123, 'SHP', 'Saint Helena pound'),
(124, 'WST', 'Samoan tala'),
(125, 'STD', 'Sao Tome and Principe dobra'),
(126, 'SAR', 'Saudi riyal'),
(127, 'RSD', 'Serbian dinar'),
(128, 'SCR', 'Seychelles rupee'),
(129, 'SLL', 'Sierra Leone leone'),
(130, 'XAG', 'Silver (ounce)'),
(131, 'SGD', 'Singapore dollar'),
(132, 'SBD', 'Solomon Islands dollar'),
(133, 'SOS', 'Somali shilling'),
(134, 'ZAR', 'South African rand'),
(135, 'KRW', 'South Korean won'),
(136, 'LKR', 'Sri Lanka rupee'),
(137, 'SDG', 'Sudanese pound'),
(138, 'SRD', 'Suriname dollar'),
(139, 'SZL', 'Swaziland lilangeni'),
(140, 'SEK', 'Swedish krona'),
(141, 'CHF', 'Swiss franc'),
(142, 'SYP', 'Syrian pound'),
(143, 'TWD', 'Taiwan New dollar'),
(144, 'TJS', 'Tajik somoni'),
(145, 'TZS', 'Tanzanian shilling'),
(146, 'THB', 'Thai baht'),
(147, 'TOP', 'Tongan paanga'),
(148, 'TTD', 'Trinidad and Tobago dollar'),
(149, 'TND', 'Tunisian dinar'),
(150, 'TRY', 'Turkish lira'),
(151, 'TMT', 'Turkmen new manat'),
(152, 'AED', 'UAE dirham'),
(153, 'UGX', 'Uganda new shilling'),
(154, 'XFU', 'UIC franc'),
(155, 'UAH', 'Ukrainian hryvnia'),
(156, 'UYU', 'Uruguayan peso uruguayo'),
(157, 'USD', 'US dollar'),
(158, 'UZS', 'Uzbekistani sum'),
(159, 'VUV', 'Vanuatu vatu'),
(160, 'VEF', 'Venezuelan bolivar fuerte'),
(161, 'VND', 'Vietnamese dong'),
(162, 'YER', 'Yemeni rial'),
(163, 'ZMK', 'Zambian kwacha'),
(164, 'ZWL', 'Zimbabwe dollar');

-- --------------------------------------------------------

--
-- Table structure for table `fcm`
--

CREATE TABLE `fcm` (
  `id` bigint(20) NOT NULL,
  `device` varchar(100) NOT NULL,
  `os_version` varchar(100) NOT NULL,
  `app_version` varchar(10) NOT NULL,
  `serial` varchar(100) NOT NULL,
  `regid` text NOT NULL,
  `created_at` bigint(20) NOT NULL,
  `last_update` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `fcm`
--

INSERT INTO `fcm` (`id`, `device`, `os_version`, `app_version`, `serial`, `regid`, `created_at`, `last_update`) VALUES
(4, 'Device Name', '6.0.1', '1.0', 'GGC00C0888E426A', 'APA91bEj7qmlVePXUpG4UjKOtyqG5x9hpeZ4tMhPDsJgDRWL76psPGtckLK3uMtmpLFj3RSFfgaVoBMCKhg5iR8RnPZPjeuml8Llgkc', 0, 0),
(6, 'Xiaomi Redmi Note 3', '5.1.1', '11 (11)', '5c72490a', 'f9ql-KKNgoo:APA91bGeQb6ghEtbAoCMCANcwzya3IfE0vLiX6IoQuKUjhi8Wky1iMPIElfBedYh9Z9W_63i3zIPYt3-Rlw2uQSLVjtRxE-8Ul8_UsXVEGMBpOHglyBL_prlFKKbxrmNL_kVJkrNBdz2', 1494862150136, 1497305393279),
(7, 'HUAWEI MT7-L09', '5.1.1', '2 (2)', 'P6Q7N14B12000206', 'cSzgknUyAoE:APA91bH61pfokcfpv1t0hFx5RRGLtOp2zrmSzIWg3nKb9Eo7g6xvji8XKUrQVqf768_2p1iDPOBh7Z_Smy9TyBp-A1in9dysuBLpGvdN3UywgKTOc0yy2Uv379qk2T63OGeZzU-fQepI', 1495093538493, 1495093538493),
(8, 'samsung SM-T116NU', '4.4.4', '2 (2)', '30040997e6212200', 'egFWwV0Mz84:APA91bFK_ZMitoZdc2L12Waxtnu9vAfJ-20aZV3fO0BALu6h4R3r8TDbIXxLJ7ObhFlUlBePwxrGHMrme42TN2yD_qk4kEysHfJJueG-M1W_4DMSAwdtBRZYtBFi7BuXu4HYAw3Da9b_', 1495118299052, 1495337192324),
(9, 'Google Pixel', '7.1.2', '2 (2)', 'FA71P0302749', 'feCno99Va3E:APA91bHt7fs2qWJBRWuwnLPvMbNHwFXylaF3mhl628fVXmT7T5NTzIXVQ2OY6IH_fZzuQm10FmbtZ7GHZ1GS19WJh2sPYpLKp6qEno-SaNOyj0JsGN7hph-__8e0kwe8Qxg9_FnANG-9', 1495165125201, 1495337187722),
(10, 'Xiaomi MI MAX', '6.0.1', '11 (11)', '9e1e8be', 'cLPCc1tXQU8:APA91bFXW8FXxHOrlFZU7qCH8l9oLjpJqdneTzV2oX5B6jP1afoBbSQNITO_iCuTquZlszpSBuNH7ooTD3Zh7wjlkMzXAlNn8ld-uwqEu4-at7ODfIkGphbMw6R8bBUgszliZPpT7ai6', 1495166611506, 1496717469431),
(11, 'Xiaomi Redmi Note 3', '5.0.2', '2 (2)', 'BEPBBUS499999999', 'dHiC9dI3M0w:APA91bFdoApOHFnBPqnBp4zIjRjOMnG_o3By3I2XEuB-ym015NwDywRP2Q3GTOPfuNAkUeCHyMlbhNt0viX1sjTIbd82xK9X5D7w5gOPThDIFDUN2-en6NaAb-Bo1YU6Me16zWTQ8T8x', 1495183733015, 1495183733015),
(12, 'asus ASUS_Z012DB', '7.0', '6 (6)', 'GAAZGF0046438HJ', 'cm0q85_zksA:APA91bFvZyHldC8iLkYcNzUe8MfAIKXMLglwUxjxJpR_GDHic4z2Ruf6y5uXRcOFT9z7jKPFjXeKmC3LYaDE57CtEDGtXO4T3ARX8AZNdtsOBvbGLTsharlqU1nRJxlhhFLbU3BxdNtc', 1495185327337, 1495499105653),
(13, 'Xiaomi Mi Note 2', '6.0.1', '5 (5)', 'da95c4aa', 'fQiL9riU34k:APA91bENTqI67ip1AYWEG4XyTBspHt8VIoa6Ke6RtZPraZzWon8EVHTya-sZ9mmzi_bSiX-vRyiZSeJODs1SOOtMxtjfUHCFmInU30esYfh8-aTKqIAP3Y-SSosQsMMeIKJAt4NreeXG', 1495186875687, 1495422701581),
(14, 'asus ASUS_Z00RD', '5.0.2', '6 (6)', 'G1AZCY12Z350', 'dLA9h0JKz9U:APA91bGm3NOGTXILvAAlR6-pbByV1iR144VD7eHGZuL_WtZiYkm9OsILYOfyG3qfuRZsGk9bjxeORmuapgmwsNzqXYgbTZfGbORoqPcZLULraYmepZ4c04K4IYl4PM87Y8L4-GSy55s2', 1495327747987, 1495591894973),
(15, 'samsung SM-A500F', '6.0.1', '10 (10)', 'f3f0ee52', 'dOJTnP082dE:APA91bGYPhuoZXYjJYsnQGejU2EAXWL5mzlioM2ZOXMZBXnNeN4Y-TRLe74VtSuc4G4Wf2s8PbG_lKzOds7U51CZB8WA6mEDjDQaXnMuFnU61MOPbumEAWDQ28ao4-zC_kpxV33nZOJ6', 1495340277022, 1496017075463),
(16, 'asus ASUS_X008D', '6.0', '4 (4)', 'GBAXGY14235149P', 'cgIF-ScrM4k:APA91bG0ifsSzTG1VuE5fhaEsfB9g2vv1cZJlcGOErIo2tXSuMlxq9WGcMFmx2_ovBTbDHnz0G6j4sdTKbkJnOMakRFClCrKu8apehm3JavObeReFdSCIyuipb0b9M5DEfkYTPb1s4Iv', 1495380319491, 1495380319491),
(17, 'LGE Nexus 5X', '6.0.1', '11 (11)', 'unknown', 'BLACKLISTED', 1495381000083, 1497422879436),
(18, 'LGE Nexus 5X', '7.1.2', '4 (4)', '00c740438ab51c50', 'd4FV-4KQOy8:APA91bGId2VWwBxWysUWEfm8GeBYCOgwY3h6NnMfzjf4eLl2fcQ-8xMkoxUS8EIqUeE7GFeWp51i2OInyF5l8N7P1VlPgbDFEVYTBCdorSdTQvCUAzDazNgu3NvezsihwJw1Lvez-U3Q', 1495387104022, 1495387104022),
(19, 'LGE Nexus 5', '6.0.1', '5 (5)', '096eb04c021f56bb', 'fDiG5QxHFm8:APA91bFy0vV2lTGyPhx8VOFDLVECchkk9yaD7wOASPgM4rh_nQNugplLKCNDGtHePXyd_9uOPULImS-QNolCrETz29_rNVi24pGTqFEykecrei8Hgv-v8B3xLaNYjSv_SabXG4DJqCxa', 1495411352908, 1495411352908),
(20, 'LENOVO Lenovo A6000', '4.4.4', '5 (5)', '660ee695', 'fKDK-wc8etI:APA91bGk06a0E9nNP1mPW0jgffioGDkFzxb7TNUuYX_Yejb5xcmtpJtPkhaUJI2lIFeP-hYRkI2GbFOPFftRVIPQfjVTNNOdpZwv7d1jCkCiRPh9AYpU3j0UfDYyID-FYUILmQWUPAeR', 1495418236477, 1495418236477),
(21, 'samsung SM-G935F', '7.0', '5 (5)', 'ad09160350d80c8287', 'fW69VT4_cEw:APA91bEBmNTosKJ8hkEhVT5mBEThXPQwlWN_CZM754QBzNfNAob8zbo_uHCfPoif5n3DkkjpODMReKxDUcHDzwQdgXw4cfw2dtYTP7Kz-60DG_mrR70nneisZ0PvlvMTE7MyhK8kILt8', 1495418353953, 1495418353953),
(22, 'HUAWEI MT7-L09', '4.4.2', '5 (5)', 'P6Q7N14B12000981', 'fKoJrGkApR0:APA91bE2InaVH7UVV6wumOa2N5l_kU_lKZrQJwTKoPtyBu_4DTQU-FVvEtMLbM5GbMRspRUmzg4N2qiocK-bnBwawBnf9dfVg8MPQO7TNeuf_8XP08Q-koJvwIAO50MwrUxkbfAxbcwW', 1495419205608, 1495419205608),
(23, 'samsung SM-P355', '6.0.1', '11 (11)', '3b7b2881', 'dpqmFTsne1Y:APA91bF4MlzPZ3CeU70ZiBcID28Ox0J3qD59VL3R6sle0BtcW889qJoIvkwnxakOgXrGp5aUhGt7UaLNxPljgmrYe_eZyRDEMHeAcooXGPQZ3okKuKh8xgyBUIq52gepxlZVmfDieHXT', 1495419534528, 1497335074005),
(24, 'samsung SM-A800F', '6.0.1', '5 (5)', '4100a76be4b1b173', 'ewuwtc8rfbA:APA91bGLEAQya2wJ4HcamxrYE-ntN70qJwAs6neuXAB-chUncWhys5ZUSYr6cl7J2ET5DSunM8SfrOdEjRNoakDefL688jhpn0PjtmCAOcueec6BUUYVw85ggWNo3Cj-gyp7MDSW-adz', 1495419778594, 1495419778594),
(25, 'Xiaomi HM NOTE 1LTE', '4.4.4', '11 (11)', 'eb8d38d', 'eHkpxxbcGQM:APA91bGmONIxrse7cf_7-t15H5XvJg5vxfAnfxPX3O6OzZuOwloFSC3jzLI-hEFsC4FBMBnIwNe_gfXD8u2aHxFx9COF68PTcpSHRHMg_tuBEAzqNvnkUjAI_WUV4tbwNdTIFO4G5SE3', 1495419850042, 1497073846093),
(26, 'asus ASUS_Z017DB', '7.0', '5 (5)', 'GAAZGF00D386B9M', 'ejz2ozFVQbQ:APA91bG74ZMpEftN2pYFRG9pqXzUvRlNK0Ef9Uwuw3cicsMlw3_WF_fobmwFSYqBGTVdqX0fhJQo0TPbaaYtFKzB8OAVwwc4N46P0U_LtvrZNSOo2odzBS2Fziqdd1ZYyBxdGU406M4s', 1495419875300, 1495419875300),
(27, 'Xiaomi Mi 4i', '5.0.2', '5 (5)', 'f757d0e9', 'esqy1awIA90:APA91bF-B-MXz8luW-9lmNFXQvCOD9Bk8AkHf6apjTnXeNKmnqjg_3cYhxsl6XvaQ79UPy5nAliveyHCqdHbmuJnv8MTW-T6k4zbUVgm6GzLsJBBWwp7XE9eiqExfVLQTciCYTCXN14y', 1495419990420, 1495419990420),
(28, 'samsung SM-G950F', '7.0', '5 (5)', 'ce0317135cba6c870c', 'dCREjmehOQE:APA91bFFGgPUxHcIafmpieqGfgu2HDENSpT-qS5rl4nbyE0BV4NjCiuUZzVr9Ij5eNftnx3IAg0BCaiJ59CBDCh1vToCos6pyyP3zYwO44NqlXWhBZR9dWtRdxVdRBQyiBC-Zs8FKMCt', 1495420706002, 1495420706002),
(29, 'Xiaomi Redmi Note 4', '6.0.1', '5 (5)', '5715caaa0903', 'fz81vtabed8:APA91bFsa5DeHpT28uWaEmuCK6AO2ylkdfyJg_Ys0lfsm7TVkpfHaOJ1LRbbOPcU4hJPII-ErJ5xQ1SL_sdYwxVraWkkL5sd4HTd9tZl3vcW5xKm242iT3LRyGYeWLxy8_voiU2LSCJd', 1495421272951, 1495421272951),
(30, 'samsung SM-G930F', '7.0', '5 (5)', '9885e6375238533046', 'dhN0-WT3vd8:APA91bGBdcuk2sooq3C8xmzt8oh0T-56LhJDGwgZl1g1vqarJF_8vim-7fVWPoBLOX6tJ4bhMZoLd8V-Z1Wd0ZrYVuMtbR2KLxIBBu_SwUnKnWmcXgnXLTlY9OqoJTam-6moQ80jHv2u', 1495421406075, 1495421406075),
(31, 'LENOVO Lenovo P1ma40', '5.1', '5 (5)', 'DICAHESSRCDUC6GI', 'cYZCSYJg2DI:APA91bEmhadGvQXz4ggz0wxvKWtxQ7p_OcK_0dcnIIJjSDwUEg_Y-lHjwR5gWr-pBX8tLIWeZRAGxQZc9CxCAvyLnzJgytRycOyuG6UNE9FlY35xVgmc5vGR_V_W6sdOcJODG91tdKSM', 1495421658191, 1495421658191),
(32, 'samsung SM-A9000', '5.1.1', '5 (5)', '4d009e0c43618055', 'e-c05WCla24:APA91bE60uDd0g_0gOsm4sSW4Ca6xDMumHIVeoDNBb6XBEAuup1nbzcgEYqa5A3LJkU6NJN0eRC4WYU2pQBzFecbJvoM44behxdk47wo-Vlbg_KYK5iLE_3eD2G_WcQiRKb_AaI604mn', 1495421812353, 1495421812353),
(33, 'samsung SM-G355H', '4.4.2', '5 (5)', '4203efbcce4bb100', 'dzmmYUtIwuM:APA91bGpEELcALVOZuELnSxn-6qd-qGIFT4ZkgH0s6xhjCpqOzZvaVV_xz6idZ1OhT05pG_KSNmu24vJ5rgysvqxV-YQ4j1jQRw2WtamiSS4_aTFB45kLHU338tKt5T7W75X_zdavhh5', 1495421931834, 1495421931834),
(34, 'samsung SM-G610F', '6.0.1', '5 (5)', '330070795b37c339', 'ek33rZ9CjW0:APA91bEuMSac7NnGZNVK_uirMScsciAdFjYlxSjZiDPZaL6X9kfMgAc3cc6s6VBR6I9p5-0BDOs_IucGjyzndYzoZOeJzmfY74yHMbfoCd7RsgtFlI2be9_VkQnJUCe0RN2g9yeVqXx8', 1495422189866, 1495422189866),
(35, 'LGE LG-D855', '5.0', '5 (5)', 'LGD855820e00a7', 'dJS0KXz5Vr0:APA91bE5RA5WAdfD9olWFCTu0xy1qYF_-qAvqS2OA7swQ1CduyLcP5h5TFIcvbktqFL1x3qsGZcDIHp0aXaLxhGDo863HFNjxrVzm_IdbJwEeGd1EvPtWcJ_xLNnC0TVJsv1c5V6ZVSD', 1495422217953, 1495422217953),
(36, 'Xiaomi Redmi Note 3', '5.1.1', '6 (6)', '9d17c0f7', 'fA0HsaJE5GI:APA91bHYRaese1XwiJD9xlFQXr82D45teZtTKCX_fRD2SxaUka4bhCKmGar95LVNu2RbAFNcFiB2M2c069l8a-o6JphYDdI_U6nCbJIuPc_cqerA_aZzrV5bPrFhXuT5Wg0_w3HfU5aq', 1495426248862, 1495426248862),
(37, 'Xiaomi MI MAX', '6.0.1', '6 (6)', 'df44fe90', 'dVH0I6GG9_4:APA91bEDxIk6RG-SRi2P8p9aFQnxARvWYYJBiZAETX_z754Na27ORyshipxU0IfqEc2BUcRuT5_tmxu2gv2yqu2XkwFRAasp01emeZvM4l4R6a7ex2pLRnqFHpDa3no1sB2h20TVYvw3', 1495426299768, 1495426299768),
(38, 'samsung SM-G7102', '4.4.2', '6 (6)', 'b1ec637f', 'dUhR_ULVqlM:APA91bFOj4-B_o8WgQjU8M-nyVM2K_4k0fsJZexPrRhDkY-GSSdSILI64wTDgvFMNC5uMmk41X0indT1LruElNtFAhysn9eQEGRjkprIYVEDj19swJ1t043iHzz-HMzxjprSWRl3REH6', 1495426379624, 1495426379624),
(39, 'HUAWEI NXT-L29', '6.0', '6 (6)', 'QHC0216313004731', 'eQgJMetruCA:APA91bE85nYVdstfvDj0Ei_EdFoEXhnZqPpGTW3KX5uGDAbnE1Ak849ynQAfE0O71edoBUfN2jeAG_hXzg7xfJzpO46rYLEfLhhyQqFeL5X-sYX4u3OHV-pV6GyC-vKXrNJf30JclaEm', 1495428454421, 1495428454421),
(40, 'HUAWEI GRA-UL00', '6.0', '11 (11)', '7VL7N15903000788', 'c9sNl0bRDgc:APA91bGcpgtFZnAh1tMTlShPpWK2f-uMuq5mq6DRNJ0ihsERn2VLRFqcxqQ-y9A8AwTAaPQkGRCPL99A5zYn6rr6fzqvx-3TEp9NpF-pJca1YbRt8VkzmuWEQhtqN6x8xs9Dwvp662Yp', 1495431192699, 1496736991147),
(41, 'samsung SM-N920I', '5.1.1', '6 (6)', '83f04a384137544c', 'cKQ1zNQ3hKM:APA91bHhVo3ZVZaZuBYRENFv0KmDwBX29VVvRdivKA0Oemk1hZzpqDyHbIG_wcYfPN_7Rn0HQPKf0gneAf8C-_e54njMhiF93Pdz2kPd9X9-edn5u7RQkZbPYrX_dVFYFpzSaA4oir_5', 1495450822206, 1495450822206),
(42, 'ZTE N939Sc', '5.1.1', '6 (6)', '40ded43c', 'c93VJAqEx_w:APA91bHq3XtkfaDqE4iQ0o-Ta7oSWdBkyMfEk9gGyjrDHpFZsm-QtQOPnYKgjSEShdZGSu1D23v6h0-9Ajs4kXxj7xZTMrMTUGG_WSfpZTlY6n2aAQxy3O5pfJOrvMGUjsYFJZojMZNv', 1495466725414, 1495466725414),
(43, 'OPPO F1f', '5.1.1', '6 (6)', '2284e449', 'e_PcPWVIt4k:APA91bGorn3iNcBbI_fPx-UQfrbmA8k_yYgUQgtb8-7RZfP2a6k2yixHf_P2tPGeSWDbJ8p7FVvpUm4nEOZN8gNzA4g3qKEUGMyxLY_dSgF_DS28futGJMQ7AHNbs_Mj9IUyJft00jai', 1495491346177, 1495491346177),
(44, 'samsung SM-G950F', '7.0', '6 (6)', 'ce0317133db5863504', 'd9yd9Pbz-9c:APA91bE_pW1-baZmHlGsRpO15QKAu1vuKJMmDIDegCARJN8ysYjzM4vbT3IrOQPrMXkloBjBXWP166mlSpNzPgEJwZkDbwF22SErJyyrJI-VuuFqbmxUSOFJS7w1j8h1rsMSuwVnMw0T', 1495491878705, 1495491878705),
(45, 'samsung SM-G935F', '7.0', '6 (6)', 'ce041604a2e83a1204', 'dnNrBQhZg8g:APA91bGQVmEiqx-g_tYZEut2b4JtN0BKOAqrxNIe-Cgdd49S7LaL3QTmaJc0ZLa1wAuxz_W-y_JJjHMi0OqMXYDJ1RdKEJmiWhU4yhDilOWtN0jRta2wHJC5qHbQiWv-y4zI6n2yemEj', 1495491918513, 1495491918513),
(46, 'Xiaomi Redmi Note 4', '6.0', '6 (6)', 'W8YTSGWSCAF6WKYT', 'dvC1hm6fksc:APA91bGSqwOpW-A8vkP8mV3RbGqFAal9sUXkeJZ0MiZdSf8HeYdFn4Ji90faEuL4FDKpVX_H-r_2mkrcXMDmxo1YFSAjmcW6dHy5E069WsLgqw2hzKsdK4XNNz0IfJzuUSl0Yd2zXN5X', 1495492186000, 1495492186000),
(47, 'Foxconn V55C', '6.0', '6 (6)', 'V2AGLMB682002825', 'cGCEf87LNOc:APA91bG3MjBtko0Ryq_qkrmCNDg4CzMbUShpVme260Xu__n77c55xweWIUtnRWdD3qP9zKDeWTiIBZvFSByYnGjCDHy--b0XTELqO1z17ylLI4Gd4JMT1bb2vok_egy9oa2Q5dHEACez', 1495492738278, 1495492738278),
(48, 'samsung SM-T285', '5.1.1', '6 (6)', '310092d924b14300', 'eJq4NUdXeTk:APA91bExth-5vCjy9qRYR70W29EZvTeJiRX0Hnfa6YpAnTizKrlKD15fdQWmsznm1hmWTVA2uBRqPAW9hY2Gy6DH7CopPDBaW-1wBE7OkZT2mbo8nLrkc3eKtWd1sMWSRmVus3MiuTOD', 1495499813659, 1495499813659),
(49, 'Xiaomi Redmi 3', '5.1.1', '6 (6)', 'b0bb55527cf3', 'dYUDCrj4kUU:APA91bGIDXj4fJTpVAClCppAhRzHevOikpkEItlIYJMiDnOJUMUd-TDYvF7ovd56gvuqioqFNJbR7EjDo4niFvR9xH1r4XB6rpojEzX7CQMMyUxPLxU1VmWQGXNr3NqaRSfG2K_KN_AM', 1495502104087, 1495502104087),
(50, 'samsung GT-I9506', '5.0.1', '6 (6)', 'ba9bbe40', 'dsUAFePuNHs:APA91bGMqqyi_O7JYYbLemClkp7Xy4UFsAZln1Dyw6gUcPkBA6yrexViwJevjzidgvF-hEcvHrAX53dh8yKfvxhLO0lVZ2R1bthdhGuFyXN86LvkAXX8hn20Hwj5LvDftIpLQerfinvv', 1495505555853, 1495505555853),
(51, 'Xiaomi Redmi 3', '5.1.1', '6 (6)', '81e329c57cf3', 'e1CBSkwYQZQ:APA91bEZOze3a4SD0w39Isg7GRjFvl9tCwmxZllTZqnzSND2Y9irxWATGggkLVySr5fq960dA1Y2uHDQAgPljqmryKsNNxSjqYgEKB2vQ84tb5tqBdz1Ks3mDngPVsnS14Yf8_XDgzyz', 1495508686843, 1495508686843),
(52, 'motorola XT1575', '6.0', '5 (5)', 'TA39400QGD', 'dsgxxNataqs:APA91bFyZUEu2KPJETCYTdii4eTDqyRY1CbmU-dIDPLq4hdM67kmOir8JgCdQFgKd0vDDnuo3wvUa4A7Aj1GAa8BVI0vioC9-T2v6YoPkCWdXcZoTTiO5W9si3-oCFStnwoLCbZmnBPN', 1495513846323, 1495513846323),
(53, 'LGE LG-H870DS', '7.0', '6 (6)', 'LGH870DS340ba712', 'ewa8YjSZvA4:APA91bG5fe09i1xi11XZZlGYME63aHTItE6A9CYy8VzQgEdn4cYMJErYI8Zxh9NQFryLJ94Mn7ow6U4P3OTE8eagwFmAmV858GRKhuXQxgNanFVLGioBp2I-d5WcBrjqvqgjNVhzIPab', 1495513923357, 1495513923357),
(54, 'Xiaomi MI 5', '6.0.1', '6 (6)', '5cd40403', 'deKwR9xDJgY:APA91bE1UshRv8fyoSE3Y7_0fzmIGsKYFa993fdypvjw-zHBu9awj6jrJvpN899jaLxVyRQhv-mxMMQgwa3SpsEMWgv4ZfpWTTsTgZpbZSLs5ccZdf1WsLgAT_QvlesiPUnKpITKM7SL', 1495518955713, 1495518955713),
(55, 'samsung SM-G900H', '4.4.2', '6 (6)', '4d00af4b4a06319f', 'dZgz9HuplHY:APA91bH6UTxqensOk_z4Ij9x18CgVFF_XGeNBuS4f5bB3j75uYGVM-LjGx8sMuCvPPgJvX6Pw6EbuDQY-IQmafSKvFcdO1pvqS5HDvYw_b94kIjgKpElYSMyKpc3RgeFc0DPgZ211tK3', 1495533908105, 1495533908105),
(56, 'samsung SM-N910H', '6.0.1', '6 (6)', '410072b9c8549117', 'cDgD11MnFPA:APA91bEwx8w4MrWekt_yrupQYBRiGt1OQnSFI6m8MWr5DR5CtedgC561_xnX1vQVWJtRMzn0AYSbp9s7snx3ttgF7nCxhzrTRbmpRHhFZrR2IL-DJB-NbI5TECEiOVRLo90V2lpG_4Kd', 1495535768430, 1495535768430),
(57, 'Xiaomi Mi 4i', '5.0.2', '6 (6)', '2f58ce8c', 'dB8O2avIHNw:APA91bGkAGti2iC9lfoiNXWR5ctap1hSdBwr57SoKsKo03W3hrwe9hC9kWx65WHxhmhVLG8Gkr8PsLL_lUlapaFLkNq7kRH20eJYm93vxzG9_MCW3tMY2MSUc1_UBYFYO6e6Pj1SWjI6', 1495536545534, 1495536545534),
(58, 'OPPO F1f', '5.1.1', '6 (6)', '103cdcc0', 'ePJ8qo_6Ypk:APA91bG-5Pm2XDcNIakIv_eTaOr359GLjlfYi64FIyHfqKqJ4Zfl58LWOfYjyfA84FPQobxVuHHq2U9ZQrfQfuxwjVFqmssjc1mXkI867d0bsp9SU1-TogcIC5E-PLxI8APA-vWtWW6P', 1495600202487, 1495600202487),
(59, 'LAVA iris 870 4G', '6.0', '6 (6)', 'L40IG0G080I0406A', 'f_ShK0W1Kjc:APA91bHmry8wFBtNYX0xCnMrCFR-mtAdhA_-hrGlFoXTO2Ki8vf9PC4PLTObUkhq4UGOBa0oXCN0oYbIzlr7plXnolbR0_RuZefycxG5fJwnox8_SHkWP2RcWsSEXduld9V2Xj7ZsCMq', 1495609324627, 1495609324627),
(60, 'samsung SM-G930F', '7.0', '6 (6)', '9885f738485a524149', 'fX-BENQtYUU:APA91bGqtY6KcpQPa_rn8xcONVoampXBErWwdCxxEkgEuAwUHS0MAY_JmXGGtb6_Im5jMDg1DEU8iiAweeKqK1fgg0-FJ1WH3Nv9tSNqMxgfPu3kyIrD6RXUoRNd7jkBecUFWor7u6Cu', 1495624625985, 1495624625985),
(61, 'samsung SM-A720F', '6.0.1', '6 (6)', '5210d46847b5c34d', 'cuOVOphEw0g:APA91bHnlnmic8bcsq9w8YHljLnmb3KK6h7IMK6qXxa5eTYEOoPhQ7eS_61KTWvctKZgTFW6C2PrxLX_Z1ZqLFIDAUHagNsHY6mRC-4h5dVJ8Ez14o0YCe6d20paWu2_aV8WtilHc8WP', 1495625408003, 1495625408003),
(62, 'Xiaomi MI 5', '6.0.1', '6 (6)', 'c8525c95', 'fECBfpBW11k:APA91bGDWMc0M590x1tnTJ6l6x3FggOQNyosuu6pO54imLgPJeiOX9l9qlQVfTe_terqz6iXIBXZ4edAzvUljFannaQDSiRKOlTMAhAKWZASRdBragkA1eXVcEQjb65h0EM8nHEURQfh', 1495625447015, 1495625447015),
(63, 'samsung SM-G930F', '7.0', '6 (6)', 'ad09160358ab9ebb4d', 'eKM-IexSDSg:APA91bHVEZl42outI6UOIY0fGa3tHDfXB-_NfPckf68XpTT6jeO4PpupZzZtRRcnzjvW1ByaLwcSkMoilPose3JslbmB-HjHOZEtovBY1bRwxPctHoS81PQDsKJb7GB47nFluLp4V5uQ', 1495625496567, 1495625496567),
(64, 'samsung SM-N9005', '4.4.2', '6 (6)', '08abd00d', 'f_-sf9lJdxw:APA91bELNqtAHc-sEHuBJo1CITU9z-qs4xfOgqwgdnv7m87oS7XvaJeiWMiqUfq68aR8aWdCBpepBemA92ch13YlGlnf4v5S1KBdcAH2uQQbJ602QKi1Li-x8YLMDtHvQA9TJ7QpPl5k', 1495639649131, 1495639649131),
(65, 'samsung GT-I9060I', '4.4.4', '6 (6)', '4d008343c8473200', 'frx4JFmoTbs:APA91bEypUp2dUCJC1uGD1_ZQC8yA1jvB9NrOTmp3UB-Rla6FONoA-MaCBWuEfFzPN_-h4efzQnSigj5eL6GoL-go9Hl20O0LPw3Ezub-6wVtmVtkuA5ErsOyof7InGIQefnKlEfAxEP', 1495663721905, 1495663721905),
(66, 'samsung SM-N750', '5.1.1', '6 (6)', '320452d995cac047', 'fPQOymY9eCY:APA91bFNG2gB4-RCBDbiF-S1GxSZQPanDG2FgI4iCqxQ-_6H9Ku1w0qc4x9Wvh3isNHwG_RhhCX9MylND0MhmirmaYT2kZoanQUNI9R5SsmpvUc_GJkUNgTh-Mn-WlfIONdps6B05Mjm', 1495666388387, 1495666388387),
(67, 'OPPO A1601', '5.1', '6 (6)', 'GAOVOJAE6HZDP7N7', 'd5uFnX9Rdx0:APA91bGWBIP_5wTjxVJxc90AkvC_GnCpTczjcfy9qbsaAoERy_bB3Wo5vBMuSb8GLY8g5u5aDppbvjyCTGQSHze5fX3mvb7PvShNyPMyfkAM7C0tszpgwqzwmJl-qZ_UnbbkBNJxgaYo', 1495681142601, 1495681142601),
(68, 'LGE Nexus 5X', '6.0.1', '6 (6)', '00d42d52b25cd109', 'dr33CUnJ_84:APA91bF8FtRrgGjAMyzj4885mR89iPG8kKnAjjOwi3S-C0ZZvG8OQdFCYfOXuvbb8fdKt17KgUCTbHOJ6NyEHYcL2zXnTGhwfzzxMHqocyf5lU1C562xmtqRronouwdOyzsEJyTRYfdw', 1495690764602, 1495690764602),
(69, 'Xiaomi MI 5', '6.0', '6 (6)', 'dd97b8d6', 'f8AZhCit5Dk:APA91bHVdr5OeoFXw1K8ZSHWD9HnsNAchZJNUGnnsg7ACwj-kVcPd5z72kn9srh9bkN0ZtfD3cx_76ugdeVVSxDOCDyl8VGtgXHaA2IiZTYGjilRjZmOHtqzBs48qplOh5H2U43aWia7', 1495716326657, 1495716326657),
(70, 'LENOVO Lenovo P1a42', '5.1.1', '6 (6)', '7e53ca67', 'c2WfAh1SV00:APA91bFMwZaSOItTlVNFki53FW-b8COzBzhledTsCUWsPoWBNng1iUWoQyRntGKYbJdSWz3D_xRDc4CZYz5JSLZJ7mdC7xO_BlkYfI5e4TSPDGr0AA6LOrcXvrfAT99k26XP_vlhKR33', 1495722142625, 1495722142625),
(71, 'OPPO F1f', '5.1.1', '6 (6)', '26c5de69', 'f_dXdVfG1PQ:APA91bHwpRGx8zBoCCf8PXqbRmSy2wnlVUewwA69of-9KPofF8pkFpJ9M1HDAT5ZnFC601LH6lPFo0dbTZFvqKKVHaqVOgjO5_k4NnXoqK57MBDQst_tVlk8tQdJF9kAkkL1ZG2Swy5t', 1495753309972, 1495753309972),
(72, 'Xiaomi Redmi Note 4', '6.0', '6 (6)', 'AASSOR7PFAZSCY6H', 'fHyy2KlGCXg:APA91bFofJzW8oNb_xLV2Uj06ZX3Qp6a2_TdbE0JYC4e5eTGo9PEo1V3aXB2UW0VNKZeQWF3cmGNxO7_3vhaS8H_XoKwkcw-OQi543vgEB2NS5xz-cjQf4wsLBdteaMAhXgaFNWdHthO', 1495856097233, 1495856097233),
(73, 'asus ASUS_Z00AD', '5.0', '6 (6)', 'G2AZFG01R5363BZ', 'e7dGoPa4ehI:APA91bHx7UnzhOBGNQEpy8QOiHYleEypTohQRptGpjNuj1eDSlco_FIN6wS7AL6zjOfOocphROZYv29Bx58GR3R52fXns_yF5GOowe0Z5P-kGAwxGX9i2cvE1ZiAi5sYMpz_Ooj7QDe2', 1495867876787, 1495867876787),
(74, 'samsung SM-N9208', '6.0.1', '6 (6)', '1115fb58fb0e0905', 'e8UNrt45cAo:APA91bHQvT3-maF1peoUXpgSqJ6HeOTg_4B9O7ajWJVv4NjYNSXoeImi5cyILYZGS490JOeXBjIecwOSytyaW7m69bNrH90YqJnlBePV1DqXOs-EbNV9h5KnkCL4sIvDB6mfJzLhj3Kc', 1495870090519, 1495870090519),
(75, 'samsung SM-G530H', '5.0.2', '6 (6)', 'd78bcab6', 'dldkF9JY5HQ:APA91bGlWz1d6TfI7jsSnpHNrGwRwIlJgMw8geANV1itJgyW-uYKm1mBLPohIzcwpvYjKYmhNEzI9rdjL1TrhBHWNSHoV5ZovpsyKxHksxjUiFJiS2nk2oIetcCWEH9ug6YmVoYuvAtH', 1495871237030, 1495871237030),
(76, 'Xiaomi MIX', '6.0.1', '6 (6)', 'ecce0e87', 'fKn4jfRc1uo:APA91bEqty0qLtETGZsdjchQdBMVGYjwbjHvmOyUW6yf4s_xFyAdNb6w-4aZk1Bd89v5YdsSabEtsraq53BSu8IncgOpvmChOVkmj-hLT2xX1Sh2yXi_WpRbKDQ-6yZCKLmXdL6SuOiH', 1495872426950, 1495872426950),
(77, 'samsung SM-A310F', '6.0.1', '6 (6)', '3100a7a96b98a2dd', 'dX_buTMv6v0:APA91bHgdHdVLh1jgEbnQdBzV04X05zALWiE1K8T4Sgsmrr-sOmPkWuEQTiN_awBcReIjZdUy5J5EN6cP8KWYYa-hSYg8L5uCD7Ikc3VqMhqLEdtjEUrct___JDCgV5YxyiYCEOq6b7C', 1495872489522, 1495872489522),
(78, 'samsung GT-I9060I', '4.4.4', '11 (11)', '4b1341391b61334e', 'efYYwaeYhiU:APA91bEL9EhubZA-vH3pbTE2cDIFchVFr6UrxCg4CbsBMeg83HyIroaD95vhNl7964KbiTfcLZX7ULuWlsjHTIDpzZbgu46_23A4rS9TY_DqPskRSQCYo854e00AtKVgzzhwkg9NyU95', 1496024095380, 1496024095380),
(79, 'samsung SM-J320G', '5.1.1', '11 (11)', '4200594f96af3300', 'cA77csOViQQ:APA91bGLGxEL5i3ZHdCdz7pXI9vuYITG5SDc7d25YFpYrweCx9cIVoxZH4pv4Mixl3ltSOOjEAUlmhrEM2cErexPmfoNqHcHFKVZFs7NmE1J9JEvq4btpXOydT9qqGbR8zGwHrxG348f', 1496027666232, 1496027666232),
(80, 'Sony E5663', '5.1', '11 (11)', 'YT9119A3ZN', 'cmaPGSXhXAE:APA91bEixUUz65oX5fhztux6Teozw_CmkHL009QpDAgxCZogeUWsHJyLwYPSji5yPx7zRmTo5DKLLV9LSK5S2iASlyTiJW74M-V0K10akO_XbW36OAVDiOMDJ5-aUTiTrXE17qr-dn02', 1496030915186, 1496030915186),
(81, 'Xiaomi MI MAX', '6.0.1', '11 (11)', '91ebf28e', 'c_WeT6ocP6o:APA91bHNVc-jPy7mn50qoniD5KatejYihrSLIBt5lCyeO1qkNy5Iqfz4OQN1-YjbfGFDPvhLs6PMQtI0c4nrGjF5RtXMrmYct9oYld5ECX0GA0FkKERg8sWqJJduo18ZWOTlSAUDKDfP', 1496033366118, 1496033366118),
(82, 'samsung SM-G935F', '7.0', '11 (11)', 'ce11160baa972d1704', 'd7M0srjsvjI:APA91bFWXzRhiCnY_Xsn27F9b9FL1iMd9SNItS6s77LEEVWSWrvDDTZma_gpa5jSERxHycsDm2fQwlp-JeJD5JEUd8E15ZK_YWHeM_NL3zDA-wkCA85szMduIHvdndnUNDfeswpH9p67', 1496035237489, 1496035237489),
(83, 'samsung SM-G935F', '7.0', '11 (11)', '9886334b3257455349', 'eCWW2_sT1-A:APA91bHS-6Bf8R_i7lT5mn074tc6JFP5FrKO5sbJUeWXw2gDfJi8Y3ud-8WzezyOO2UipQLtValhHTodQLpSAIr8-Azpdx17plsFCqc7IBpt1oBXpAZ4nrckGCTWpw-tx5xmYZCnArgb', 1496115975243, 1496115975243),
(84, 'samsung SM-A310F', '6.0.1', '11 (11)', '5203db9bec0ea389', 'dktY2V97MAo:APA91bF6B4wF861IX8Tcdcdsh88VglQIbglZHIz_-j7TYOa4HOxL5qMq1lVfwfrQn33k1ig2D3i-aJe5CwjHP5KybQm6sXvHbPQII5Lz8I848mdyAVy6Cg0PgTnwEOceMUQVcLNxzHNh', 1496116025223, 1496116025223),
(85, 'asus ASUS_T00N', '6.0.1', '11 (11)', 'E5ATCT084029', 'dK8QbYeB2-M:APA91bEgJPqWYFBoYFT_8xuovugVFY8VMDUZ22lVBxO-0Zt4PFPd-HB-ScPmJhbNzIaBnIiLTTQtgBp-XD_KmIuSVrc3NkLx3US7Np8VVhQD1XjxXkNoAy6KWSN540LkxizMw1_iFTaV', 1496129321564, 1496129321564),
(86, 'Xiaomi 2014817', '4.4.4', '11 (11)', '3a27a58', 'ccC0cto0HH4:APA91bHSA8Re3NRgPztYw_eR2KfNqnI_-ejFuZTxNb7LsIfHO6UuvCcVo2YM37y67Lm-s0sYqiOkRpw8etrN12z4NMXfTudrB3NuHFdthUfNCLk99Du7abpdmxI04Mq_5ht3mhAF9JJ5', 1496158515803, 1496158515803),
(87, 'samsung SM-G925F', '7.0', '11 (11)', '02157df29b1b6d35', 'dn_oF3QTgOI:APA91bHZ7vw0u85VyeWgwPDUAJbBkWqnW8UUIHiOt61WtGGYpmQbNWwxiZef4kCyhKtkYGxQN5hw3R4mi0PcTKDLcrsayqPYngDxSOnBNMjA0TKBG4CGeZq6ziiiEEMzv5no1TVA4seB', 1496182668368, 1496182668368),
(88, 'samsung SM-G930F', '7.0', '11 (11)', '9885e6395850494741', 'eGermO0SKCA:APA91bEu1KsGSYhFlb8ctvbaO4KZK8plngrF5m7mYZzw4Au16KXAy8gr2ewhQuDHj3jX5YELw7Bj5vCT5KqtekWOPzWzpLdJ79pHcsAcHFTELKmEM7aJ3C_UqHhAM4Ybi_oIOo48YiHj', 1496184336634, 1496184336634),
(89, 'samsung SM-G935F', '7.0', '11 (11)', 'ce051605b8604a0a04', 'cBpQVTquPog:APA91bEp6ltmir1oUyIu9l0mwtDcNbeq-tqibYhvOPqb-KdOrt01Vgha_m9sPMV_zr19uxR50zHPkGmcnR-p7DRbUTi8uhF6UCKZTs01GkEgm4lwelIRhoG6g04Ips02RcoC4fya0IPL', 1496185662688, 1496185662688),
(90, 'HUAWEI CRR-UL00', '5.1.1', '11 (11)', 'JDM5T16108000634', 'c085eyiVEbA:APA91bEKUvQT8NqXSRDkd3LKPxCcgfu44JTXcfsz70v2XYjXFiuQBW2ZVNJRdrR89Z_gT7KeyGLWCZuENQemmomNnFyThGq37cM4eCiEIx3duSRkoLaXPq0TeL26vXJGrEPt73Pii-33', 1496186703040, 1496186703040),
(91, 'asus ASUS_T00F', '4.4.2', '11 (11)', 'E7AZCY334550', 'f_cJTOV5V4k:APA91bGJuTLOPllPnoPxbivL0FxWa31b79na1SxFat7yCyfsAe-L5wA3SsVT3kFhKzly1xJnlw_VH2BLQju8c_c9mmIkNvXCB8BOhlbe6WskelTT57hSQ6NDqCvsnbNOwnMwl1NJYOy8', 1496188053186, 1496188053186),
(92, 'samsung SM-G935F', '7.0', '11 (11)', 'ce0516054cc66a3a03', 'ccxEH6NDoy0:APA91bHII-T7I9vTDhYtetp7WSOdG9HwEo_xzX_uUv0NG2CrHF-IKG4H7Bu2bwizmnB4pciECYPROfP3raMD8yrDBhp2d4xMCJGXinTbNS0kIOYTes5UfxhkP4BuBdN8PtjJwUadVmBS', 1496188474169, 1496188474169),
(93, 'samsung SM-G935F', '7.0', '11 (11)', 'ce051605b230692604', 'cBWzoxIflA0:APA91bHpW7Q2kRWUvPEBrkJklvV0f3VCQd3-PFlj3wgY05gqgimG5YmWT3GBHWNafJoRrTOHAe_M4AgAxy_zH58voL9Q_SvhVhlgdFKycnIVpqjjY43CMARyPjfdFBZPoD_I5I4k8Eko', 1496189812070, 1496189812070),
(94, 'samsung SM-J510FN', '6.0.1', '11 (11)', '6d6be06e', 'efBnPEHgYMM:APA91bGvmGjctvEJhZDnRpip0umZ4dKCi1y8gvauFptlwhU1e1SbutzDKM7yokDHXkdgZWfyfpvnyPuPmUnVSeBcYCUxiEIZ7yN1xQlvJxRyrr21vW4EJgBEbgNzl4yFHWUTpy6y_q2c', 1496194068951, 1496194068951),
(95, 'Xiaomi Redmi Note 3', '5.0.2', '11 (11)', 'K7WCWKGQZH4HDE79', 'fZrRryUtCaI:APA91bECUBNSWgE11a1Z-bOvExa12U19pfS9qwyuU9d-SSxdrvDEIVsqsn1FfUiV2LSgr5GJ9r4ySvI-D3bQre0qCFL18qBDzSD2Pj22NcLULKrE4pgvxHR-ZsWzhUfW45m2XqfOAvp9', 1496197427535, 1496197427535),
(96, 'samsung SM-G935F', '7.0', '11 (11)', 'ce11160b821bc40105', 'eXysdczocqs:APA91bHm2DeILL80hOnwGYuLHr7_4pp0BwyRrpv4RTMaUGdZq7K4HV1dohS_DU7QLd9ta3THvhMMaIoHtt0pLlnTQoDz6nyfutTqpfHcQEqtEp46hqKvSSlAILFpBMNb3QwsHF94UQqA', 1496199907856, 1496199907856),
(97, 'Soundphone S2', '6.0.1', '11 (11)', '7e6ad48f', 'dDywZFGS-ps:APA91bEbb1rbOwlefa8QEEpESd7rcTjU7Uv8Mtp38CgryMbCS0HpBYPyVPmrm0JN7i8kH1gH2dGfIyCsmd03HAdm5FvT2OoaE1l4TkE44Avw28wEdEAyB_54VdslLg868RUh6kNn1s0l', 1496200527752, 1496200527752),
(98, 'LGE LG-H845', '6.0.1', '11 (11)', 'LGH845d48fb758', 'e4E4tWjAHk4:APA91bESLfIgZWt7e11eN9Ozxp4WjLAqzLLdsCdTIOmcAe2KO_0nr2J3U75h9yic7VCOQ-pwnErdqQeuGUFkWle2a5IsUb5i4nEua1TuOHwqdgRNaWiIVd5aUtWhsYKJkDuMTV2E25UU', 1496201183485, 1496201183485),
(99, 'samsung SM-G930F', '7.0', '11 (11)', '98862748463333364c', 'fRd2hD_K6zM:APA91bF_NVLHGruI-KQj9wx7ZrXgkv1IMZwPdz9ojdZ6O1WJ7oboWwD2HrJkeXXbECbx9y4t24MgzeoAlkdhYb1sE-nxXlArwL0MQxMVM3soRJGzIT8BqOZsFG3_yrj34VBzuJ5V-Gbm', 1496201587990, 1496201587990),
(100, 'Xiaomi Redmi Note 4', '6.0', '11 (11)', 'EYJJGIAECYAYDAD6', 'camk1BShHFg:APA91bHZENEeC0PgA0lx_erFVIfhMRskxgFjtVnbqkEtjy8Hbl6tTPM6cWKkZ0-Clx4-hZMfERErGHXoetlTpHECtToShq1lTheVhVM-MiyU3n1TtN-VweLG4d1OeC7MdAO4_9cJfi76', 1496205521441, 1496205521441),
(101, 'Xiaomi Redmi 3', '5.1.1', '11 (11)', '28ab9f15', 'cmHni0NQHOI:APA91bH_ipob54DHyriXuanPXi8hkUqgEobHWs7cIdssEbdl4L5pCuJ7ZCu2BY6uZVEKp1ix8_XxfVcb-wJY9Ewd4VV8Xn1txZZcXGUJ0vnTyI9h5ydwp97_QdFyBaSkX_KpA9jcLwir', 1496216842362, 1496216842362),
(102, 'asus K016', '5.0', '11 (11)', 'ECNKCX257605', 'db4nH87HaiE:APA91bEUV2I7Ftk0ZkKXah7yYbmIeslJ4HOSPm-8k57n1zpoTO6tLKmWbpBFNkjL2qvPMGikT-XW2IC6VexdHWTxcjp_WuVl1TSPxdYulB14rpd6VyW2fFZjRlDyPXS1ChNGLDK8aaQ5', 1496310708989, 1496310708989),
(103, 'Xiaomi Redmi Note 3', '6.0.1', '11 (11)', 'd095d611', 'ffof3JH4iz4:APA91bFwGBUD5gCOnACmkQYmwHlQArgCY_Go_0ubUSCg2Lqu33OrCdYqioindf6-RYbV8nmqfq1U9_FJBQJklXsxErPYqI5GTsobsnJaCWDMbgY2Bg6yvyS7t577_-cUtHuYrUzriWbj', 1496379135495, 1496379135495),
(104, 'Xiaomi Redmi Note 3', '5.1.1', '11 (11)', 'b8918f09', 'dtjp-WVitqc:APA91bG0cxmhjiCyErqZpDhlzB39J846RG776e8Lrrd4-29Dt06ZZXzk1u2ypdkybWnSw8lzCs94rbDwGH6JTCyUgrqaDxLOx6yzRqAz39sGm1AyGGqOdNOZi--688PgHiomNs_rqEeS', 1496379235729, 1496379235729),
(105, 'asus ASUS_Z00AD', '5.0', '11 (11)', 'F5AZFG095724', 'd29PcRCt_as:APA91bEp-5TxTAwmeZTx7PEHdHUPrJVeuDhQlrMtoz_5HKMz-89J1IvWXubOu40kbCLf4jdlLFDOEmcqVv5ehJVP9SlY0FcQX_RZLMjpzzItqoxagPpAs6gHOAzGKmahWds6wJfLIKwu', 1496382381363, 1496382381363),
(106, 'samsung SM-A310F', '6.0.1', '11 (11)', '52036e5aeeca83c5', 'fs3L3DKip_Y:APA91bFFllfZgxUtp2YlmMb7k2uIGPH93JU3LNvqprhZDc4vZkjCLueK7PJA8sX3wK7G7P2uemGVxfxgymrG7-1E63peu7MNZzgyvffUr_1BzGWhC131XUhI3nF6ZpRpKZrH_hmzGQlt', 1496386408677, 1496386408677),
(107, 'Xiaomi Redmi Note 4', '6.0.1', '11 (11)', '108394429804', 'dmUcGfCIMTk:APA91bEXed6P1bliMM62oIRa40P4JwDYeeM2-R2W0JEo118qpxfZA7QJrtNTp-DcgQnnbi8yf7Mj5mL7p54rfWEcKqyPggslxabw6lt-CtT04AUtyJ9eAx6EEmphuyY1RIoNDWt9a0J4', 1496387932031, 1496387932031),
(108, 'OPPO A37f', '5.1.1', '11 (11)', '5366a813', 'ecEQo1i7I2k:APA91bF9aMYFTZ7l50bvmcBqiiWsUQOZoCP0jEyEzdSHBUdXhOm_cbWF7QhPa2NkWPgIcAipXsta1YnEUgXgt7Ews0AcVkJhgKCQ8zsJpNtKs-uz9T3h26M4XPCpSiJGMrnLfBMh-be-', 1496388226369, 1496388226369),
(109, 'samsung SM-A320Y', '6.0.1', '11 (11)', '5203d0dd4082c35d', 'efzqlKArmcU:APA91bG9d9VMwtgm0AJiz10L_uegYh_-8mPut1Op5mb6gpXmSX6o38DtTXIGsVtYzCkCdiH6e1L0nIZ-J5M_6D0Ixg8iVMvlMS_Y9Yc90kZIQzQ3k0bS6yEXFc3V26kTTgZmdi7o3M_X', 1496388302188, 1496388302188),
(110, 'OnePlus ONE E1003', '5.1.1', '11 (11)', '44ae0f65', 'fT2DPlMi3pM:APA91bEo8DbbX9w3vO_2w94G_c3zWIdeijQwjabfJQH7A5CxYFtl3V4URXwxM4LSJg-cVz__QlEdY8CJ7NUrQD3VeID_AYG7sOJpIolKGdQWmaWgFRXB9NfD8KQHP3HLSRpUTVwzRQs6', 1496388429878, 1496388429878),
(111, 'samsung SM-G935F', '7.0', '11 (11)', 'ad01170206a6071ae3', 'dLvfKR4UtW8:APA91bGvN5El5dYHwP0NOzfHXAADu8BUHQ8pJ8XdcaUF93zBP72XIWI-RwjMSehEMqNoTVmDLwHMY2b5kh4nMs5Ozf-vTp5xSRvXGVRxPY6_Qx-uwP9AX2dd3mjMOsWbehDWrKNaJmEh', 1496417607640, 1496417607640),
(112, 'Xiaomi Redmi Note 3', '5.0.2', '11 (11)', 'GYUGHAYD5TMJSWIF', 'evbB3n5AY5s:APA91bFyWrFhnBC7cW19mjKp044WGYIUkSDKQ0Qews5LBpMZ-p51cGjzHmM-_Hp6frQH0eA6BmQxzU8SKCNy28M6ERs3xBgpokJiheQnL3_s0Yc74Y_2HTxxn4c0Wi1g2tJOnIJtp7uC', 1496726495894, 1496726495894),
(113, 'samsung SM-C900F', '6.0.1', '11 (11)', '629cecb3', 'dR4ZEtEIY58:APA91bFQFep254xrqnTZLrW5m98uAr5dXmFx1duW5jpmrOweWFl9G7o25Jyg06qckWn5Ba7hWrAqjH5lWUJoXwPkK28rWRmW3uGR0pgjB2S4QYRwD3eV5Yd4PrmpKotQdrIrMlbEUsYh', 1496749152632, 1496749152632),
(114, 'LENOVO Lenovo P70-A', '4.4.4', '11 (11)', 'L7BMQSUC7HBQRCDA', 'cS_d0mV5DzE:APA91bGwCvoeqdYX3iAv0kIVFBr_-UXbjwIua6_0mdq3u66G7KoaHkSJSn07Er7xj6wrFUWl54UWMOeq_GOl1qeAfMXdXKfivTOdmAB6JA28qLyRUV7RP4LbYSkYquPWHKGqLg2nZJCM', 1496752838136, 1496752838136),
(115, 'samsung SM-J500G', '5.1.1', '11 (11)', '4631fee4', 'dyxe6IFrMl0:APA91bHTJlnuERAUZobfSVG_uiVF9d8ceRsIMYUHf8gNrV9PjYvXSTYskiQDro7pupcbF5e1kp9IoPavMRzx-aOV7zSDako9q7-c1er2gObLCPy_86DmckaqPbyXnAEJmdozorg8a-73', 1496875772587, 1496875772587),
(116, 'samsung SM-N9208', '7.0', '11 (11)', '85334b4433443647', 'dgmwzNFL1bs:APA91bHj1bmZxaFxcQOuw47oDIJLXdmt5mpJJhLu3q0zlJUbaunmuEYWBAa_aSkjmJb8rDa2gZbJJLOa0ZX1WClW-EpOln7aty0IHiikufBAUiSmfgUd3Qb4oedCJGqmEwrd65Yr_oN-', 1496999555699, 1496999555699),
(117, 'LENOVO Lenovo A7010a48', '6.0', '11 (11)', 'OJ6DBE7PHUCUR4W8', 'fhYZxM5RZAY:APA91bGelZPPp6hp3nbYcKHm3JGEP5FY_f4tgNdy6Yknbdyzo7y7i-0rKT_UfdDb3uhWDg2daWKk8Zb-rhfYhFhfDRlrUNTpEo7uyEwLzO1dAqYp1AlDiTBBARuSFlVPVtoZOxDog4wD', 1497002702913, 1497002702913),
(118, 'Xiaomi Mi 4i', '5.0.2', '11 (11)', 'fb26e4ba', 'c-HIKD32Oy0:APA91bGQbGKCRinsgGL5lPMXr2pu_qbY1bjVie7RC1w-MtHeYo17VY9U3EEHN4Ccv0XTSTXY-XAaMJoP1X6SJsyjpqCd5Vmw90Yl0xr3yf9qYryMJw6JMJizojrNapRHpm7RnDs02A7M', 1497004839685, 1497004839685),
(119, 'LGE LG-H818', '6.0', '11 (11)', 'LGH818123095a9', 'fdkORSBtIXA:APA91bG7l8YTDQf6MxKN8XdsO_m5ZSgf6Cdhih0u6oseW4mpiI-_MHegyFb7vv29HhI6Bpjkrb2mytaXb_iuhqdSAwnMfF4Upegh0ZTjgUQSK-7o-cxJqpduaCMiLJvjT72kHrcqC-Ji', 1497141804248, 1497141804248),
(120, 'samsung SM-G950F', '7.0', '11 (11)', 'ce031713330e1cab0c', 'fnDNF21QerY:APA91bFATUCAqFCM81JOrZfS_eiv69za3n12oG0rAfa2DJ4w9ksgZADdNNnLP-YFbptWKU5sdQKfaymJRjkNt0ku68nGFTkZ-cXr8OabTROqaF3FZQwuj8NXI5hAoB11H4auRyg_kIJW', 1497195142984, 1497195142984),
(121, 'ADVAN S4F', '4.4.2', '11 (11)', 'S101X5A150106321', 'dN7p_V9SUl4:APA91bE3L_mzGL4CxY14xDX5OEhPzcsr211NOSU-2p1PNiNSCf0h7sVDLOmsBkZVTLlfD0uvoCT2pgSk7DkM7p1IKa22X2sGccs5g4pRjSOfqr5elxslvuhX4oSSdUFIB-ZFeoC16UJ7', 1497200239058, 1497200239058),
(122, 'samsung SM-G935F', '7.0', '10 (10)', 'ce04160484c9393b03', 'cG1sSgULQC4:APA91bFa-KbDBlRaW4sr6NWghjmLGPxEfVOzzeZ3tQ_4L3GEqJglVzHrybROjRaPVKgYS5JlALJfrL2WDzxQEK7WLVIxPteA7HI5E7WooiRUhCjXEGZgrtY3HKlZ-9RXn-Z3YPbsZeBg', 1497230229258, 1497230229258),
(123, 'samsung SM-N900', '5.0', '11 (11)', '4d005df0521780bf', 'dRFIYg6xBKc:APA91bGjDDPV-9w8IMvV4P3HsPn382Z42EJ7fvFFVuxtDZ6-DNz-yYGPlkRZyeHNDiWrSsRtiw0Xo_EVC47GUsZ1_5SWM5pScyJ8qHLYy0NMp7Y8HooYawIGMVNhoeNFF9V1euIlSscM', 1497245218896, 1497245218896),
(124, 'samsung SM-G930F', '7.0', '11 (11)', '9885e64d4b3441374d', 'eCfCIKrulh0:APA91bHzmI3Fjdceq1gB6TtJ80vBE_5ttd3D2NZ44AFsEkxohJsODewMiubkHAcWzpP7Hls-1Lbchuj3CX7U6SttcBMP6-APjCCATi5siugZLuMx0JX1I1lxBeApiP9QN8l6C0P64MeX', 1497248669295, 1497248669295),
(125, 'Xiaomi Redmi 3', '5.1.1', '11 (11)', '515577e67ce3', 'c5hQD00z4DY:APA91bF3Obr6SgauBwFAkuecfj7_HYSZ0t5Zp57CpJpge5G01IQexhc3793Rs1JO4OCxQzUbcids9wSM1WsoZi7rCD7S36AAXCmR1TyZNnz4AEQq1ytFTSHmf_Itp7PiY_Cu8hgPvKmO', 1497255961114, 1497255961114),
(126, 'samsung SM-A520F', '6.0.1', '11 (11)', '5210e041c07714ab', 'cPOj6rrIjXk:APA91bFq4eI3quTYeSSvbKVzSHh1KQHRkSos__Jb-4LiX0CaCybccJR87DAlU9gulXd6maQDsM3cibNyLxNV6oFraRWhq6uFLsqbOoVNBprX2lLv4LZAgx2jKDxVZHd3dmjq34wahC1-', 1497257172732, 1497257172732),
(127, 'Xiaomi Redmi Note 3', '5.1.1', '11 (11)', '5ae9256d', 'dtvfsXhEDNw:APA91bGs8gchOACR4j0wGfdXC_b1twJ6rRHzx7srkRlHLVZ71R1JqzGd9G8PPdcQL64ZjJLwIZcq6pAQmi6KCacdd8KfB3eC5eteOxeWd6aPfY7FIA5q6hc1hzydM16tYp6wv_Kid5RH', 1497275214773, 1497275214773),
(128, 'OPPO F1f', '5.1.1', '11 (11)', '296470f7', 'elp9TJIsRw4:APA91bE-8_Gfi6aWVqMA438RYJMorzn6oGL_1B7Lt2SjZeP-oOXoQb1hEPkGaJU3WUcAz4mF5ocoqoYURyNYfEV9VbMhJv2MBosNAqMe1ap3h-NzZsfG34Vt0TM6Hv2IPXokzEPut24E', 1497280952663, 1497280952663),
(129, 'asus ASUS_X008DA', '6.0', '11 (11)', 'GCAXGF00D563WK3', 'dWNa2yrow70:APA91bHrD2kIY1q4dW3gekAnwoZ8nubPaxlRvbNGwh6836xZLypfpNlXPOUsAHDLLxX74PGql8ugjy6M27NI-EIRZa4oG1bS95zXr0IzBytTH-iqbnpcoaJTSLRFB2vMgUk99crI2low', 1497331107048, 1497331107048),
(130, 'LENOVO Lenovo A7010a48', '6.0', '11 (11)', 'FUJVIJ9SE6KVK7MR', 'efQg5hVW-kc:APA91bE4yjiIQlTM-XJGauhyNIFmvnvpx5jSHQSolc9DSR5KECp4gMYcELVHucTiIRORtrTE_UbVF7M65QMQvVa4XRj8xy44ArtX_0RlEbTDII_hLT7LvGJm-kiDdzBFe1orjT6mpWSI', 1497331499326, 1497331499326),
(131, 'LGE LG-H990', '7.0', '11 (11)', 'LGH990ba176332', 'eDt6jo7eGS0:APA91bHnvBf-a-8A_UjfMk-zBBq_Y-h0JLoTKQAYwV3VREkTfrc6fmrbL3ety8HYbCycPGw_wFCi5bMowCOIXxsIvFEZ5ulS-I4SQbxGuAU8EVsCOhqHsCgXBoKFKqnwSWYlgU9OMX10', 1497333944998, 1497333944998),
(132, 'Xiaomi MI 5s', '6.0.1', '11 (11)', '59fe41c2', 'ds13D6Up3rQ:APA91bGB1LBMd5jocaCwECNzlgFubnPsn0b99kImcP4jxZaO-Ten6bNGBv8vGIAF7P06hR6_0HmBxbbGI-RZqoE1T1Qfr951xO-_bEoe1AUeF9bc5RfFDkokWbrDZ84ei6FrCawDLnBD', 1497333991374, 1497333991374),
(133, 'samsung SM-N9208', '7.0', '11 (11)', '83f0304a4e345846', 'cTNwuXhXEnY:APA91bHzpvQlabnuzTqA6XRWsXFvQ19cNNAVpUlBU6iq1VdRdfrAlUspJ_c-xvY7o-ZVs2hdE3gxxAaZyxFgyFg0SJ09AP33vzBAOqUj4C0ZzIXq6KPsCwHi--Nlu4Y-4QN66VgexKqp', 1497334006310, 1497334006310),
(134, 'samsung SM-G570Y', '6.0.1', '11 (11)', '4200c198fe79b3df', 'fbB8HSN_BH0:APA91bF5XY_uG8qFTtbmODJiBHcWM_NSb_cyuFTvtmyyNNiJmEyxLeEa_FXiPzRx2BUOnprjXxr5fQMo_KUcwhlzYn_L7TfazOA1Q2sXKvjuw1aL5X6F6xqT5P2UZbkEAoTagv7Dfv4P', 1497334018549, 1497334018549),
(135, 'vivo 1611', '6.0.1', '11 (11)', '3c5de099', 'dfA-gNhTx6o:APA91bElzhbJo0_GMSXEw2CxIaS4UfTaEFtjZeqrezo74Douxd2SttIn6WLX7txxTa0k8O4Mt9wMsNFQka5EQYTH_A7S232-m1zQAvVMLN_6-JyBZDqj8Wf-dQ8D95vSLlTN66AEK2G6', 1497334065001, 1497334065001),
(136, 'LGE LG-H845', '7.0', '11 (11)', 'LGH8451fab352a', 'coqo8687ayo:APA91bH0s3J9AEHylZoAJLgVPbQIfe6jRDI_14AYavSY6TaqLKP2-lQMr4zol2scqWklg0ESht1oOGPTg3xKk3cYRV4gCooRYhzOGa9P1gXseqZJnP09tcGCHNdAn5wBx1gXh_CC2qf0', 1497334089320, 1497334089320),
(137, 'samsung SM-A500F', '6.0.1', '11 (11)', 'bf4a5669', 'dhljbJf1OME:APA91bGYYFQ_-XRtyjhivuUlPxU5ce20qNmM_0twjK9gIVbtCwPTh2eSAeMQeWQtceMXqFVj49Na0K8ND1vx6K0NzDf_XeFy8hGHYY8JUmrmXsvVkjKv46YWWtXBeRfAt0KJu_WzqXWt', 1497334115923, 1497334115923),
(138, 'OnePlus A0001', '6.0.1', '11 (11)', '625ae260', 'eEKB2SmEZVw:APA91bHVWA0J8nuMVENYYLSUTkwrxF2c4nd9vgM3Xuunl7r37PVG5SgGPpFcJZ-szMWFvxaegd1wF5lgJw2KWWKX7odhSRFkHzeEY_BAzuFbK4ipMOd9nDdByxbp9os0MAwKVfYuU8mL', 1497334135703, 1497334135703),
(139, 'OPPO CPH1609', '6.0', '11 (11)', 'T8DM8DRKAMSOPJ4L', 'eVUHF0RmYrg:APA91bFly089ItdVFMhTBXGqg1FR6kRmL2zVACoclhw0xlrQaiovxQKmeiPFCX3OrQmM4aH-vcimcmr5XdN8CH8lrLRJEvVB-LCYBVnPJ_iS6O7Tsfypkp1fsoirgNB6y4vv5l_63kC5', 1497334175241, 1497334175241),
(140, 'LGE LG-H990', '7.0', '11 (11)', 'LGH990f2be14b5', 'erWZ1L18EII:APA91bHRGYK2gSwgUQJKeWLq1d2l-yXv-Ud4sd6pQncpdjKcNjfG43-3wL5pM4Jd5fnS6HQZsOfI2yrtZpYBuwFsyHvYO9rlRx7QDEMeInbSQqoRkGi1OZDromzTD_-sp3rbYFyp1MIZ', 1497334202751, 1497334202751),
(141, 'Hisense L697', '5.1.1', '11 (11)', '4218ce43', 'cUpw7aa-erw:APA91bHSFLUq5zBVRcDwFgDOqEZdYT0n4J-J0wtSvhpXRvo0iVNtSt9pMKxiqDvL6mo0gToinjewuV8MNEB3qIvHRWP29E410RYyXQMqZLFLtbg6vkRI-nxVFQErWNKAxEJtYfx0-wa8', 1497334220191, 1497334220191),
(142, 'samsung SM-G930F', '7.0', '11 (11)', '9885f7524f39503930', 'fEIZ7y61ojE:APA91bEW00HWBZur7WJPwiyLk3Eu86XC1MNkuDXjiDmQQIoajpjb5Vbii0Vem1fWtZonz4Xriicy6vsj6cdMkX__UNnuhHbu8Fyt2qhSQ5qCNbcDWFYXVSPdbk4DlexTkFT-V1SKbIcU', 1497334220248, 1497334220248),
(143, 'asus ASUS_Z00AD', '5.0', '11 (11)', 'F4AZFG162483', 'cTu1hN2ZpAA:APA91bH64sVKoa3GyW5JzZBMZ5rDykRKnE86AtfcIYgDcf0MW53vWyUKZJvsVtL5y-HmpAVnecjcWPsQshsypKtWMGicZXaZIvepwdSnnGoqCiT6PpuF3aOHHvAnluaWmNsWNwBXlePB', 1497334221917, 1497334221917),
(144, 'LENOVO Lenovo A7000-a', '6.0', '11 (11)', 'W45L79AMONSCKRKZ', 'cjZDdw15t0c:APA91bEA3u3Q-d4sbMi2_vaNLJYiFMZSrlOAeMUjbyzI7qYCrUMLYMaiU5CMwzJVeoK6YpQun6mMxUmOWxnDxBafyA5yX4i4KvQpVMqyvvKH7vbG4t3LW0vxDvOKvoKNHBuGQkZx9kMH', 1497334229910, 1497334229910),
(145, 'samsung SM-A710F', '6.0.1', '11 (11)', '33009a6ace46a2a1', 'c637P4OZis4:APA91bGfd0D1zUYawarUyQYQF2UL_QljbeotS14ATCGLx2SXb6P2qUXMkr_y0LYzYi-vRPE7pqNc8bSUpp6l6mwKLtD7IkuY6TqnoBQvpv9TcyX-EoI2w1CUD96CroS0hIMDnDeq2g2L', 1497334239706, 1497334239706),
(146, 'Xiaomi Redmi 3S', '6.0.1', '11 (11)', 'e4c9e0617d53', 'dyaUvSDVT34:APA91bF-4DqQFgZT69xPeIbhOBBD5VElYbk3b5ryJyVXZYP8np0GemlsU4NuOpsXFx2YFMvY2sivPSz2U_RsW1-mNE7BOHJtWqAi7KB_kMejuZsUJZcMMsjMksUARTB3hHxjKblfHXH6', 1497334279370, 1497334279370),
(147, 'LENOVO Lenovo P1a42', '5.1.1', '11 (11)', 'c01fd8bd', 'ffPzzDhkhSs:APA91bGW7uEbwviEieXrMthBBH5rbpQo7hV5lEEjMdVSjEHTI5kDOytBA1sM7dy8oK7T77rTvOic1T71Q9eEABrwhqvOyCNGHXVid-MOhI5SzQqzmtyutUy0QVddNeQZQ_9DxPwVOMAk', 1497334343553, 1497334450860),
(148, 'HUAWEI VIE-L29', '6.0', '11 (11)', 'YED7N16A17000929', 'fhx_KSYNM7U:APA91bFSeCOBs9Loal7_6aE35zWa9BBnRL4-NREugfqACEjAN89unOR3E41UDMKx0OjaKDQUwBONzAEKgP36X7DxO_3N6HKlSaG0Hb8AsUxpJHZIXly8fDZXb2iO75OwyU-xHHQDCR40', 1497334431932, 1497334464624),
(149, 'Sony D6503', '6.0.1', '11 (11)', 'CB5A28AJDR', 'e44G8MbL60M:APA91bHS3-z05gPT7sHa1ZibjjOwuYV-j95di4lOPsI_j84E2pe3mSX7JGe9Qz4tDWVTpuB555q2eXBo_DD_IfqpB0ya1e3_lBtYHE2mLVouSMbid2CJeaF5TFxg-RD-Qlj-ZyxjIT-Z', 1497334439737, 1497334475639),
(150, 'OPPO CPH1605', '5.1', '11 (11)', 'YTY5LJ5TRSKVDUN7', 'c6ULknRp41Q:APA91bFRM22JpikakDd31tNdSlynn87BZeDUNmb1Q35gFoH9QguJHbOgP6HQJpmDo7kfIfaJQnOM1HWBnQWltcwynw2fM6nxUCxPZ4W_iC-TnMvY--5vIOW2Ejrk2Dyyh8E1dQ-bhm2o', 1497334440331, 1497334440331),
(151, 'OPPO X9009', '5.1', '11 (11)', '45QWHIAMFAM7FQJN', 'cZKZ1NBkfXk:APA91bHEiN1WYzGRCobTBkZcuJP0ZUEAr_OiSHpyaoA-60fCsaFRt8xn5yQ-dee1TYjvNzIhtSJ_PyhcBrXVnYu-xkphdxEyGXwcVx47ERbsafVicfIpzSTR_ZHrzO3CVa3YUHlvGPj0', 1497334515842, 1497334515842),
(152, 'vivo Y35', '5.0.2', '11 (11)', '579b0da0', 'dUejxZh2Rrg:APA91bGK_DyKQw7Zo2_EP1eK2foF2dEdzkmPSTt9j3M8kVWxAQkK1QcgGmhjRTm09zidPkP3T7ZXWm1uedI_3Kw6J4XCLcCP9H2E3RhdZjPSMLj6xvWnJUe42rDtpB4GhQzbhjwADGLo', 1497334521075, 1497334521075),
(153, 'samsung SM-A520F', '6.0.1', '11 (11)', '5210fa7dea3b143d', 'dIPoFsswUIs:APA91bESkfOcf1EdWhBPA6HkMbHHzDnxcTbSKHAXht5CDRMkWZ2iyXr3DySqqFdtHfg8YpVzMjwuGlbC80Z1whzEonxBWbqumRsdVr3nVuKm2wi4F62ZNNLJXQqI-TWijY5O-aVijZlO', 1497334539676, 1497334539676),
(154, 'Xiaomi Redmi 3S', '6.0.1', '11 (11)', '3a6190f7d53', 'fi0p7Vj13eY:APA91bErDyrovsnAdq6c7eiPxeFb_kBHKLKvK_RPCU2RDmRqsbZqP0cKgjxC_NiZJGWmQ00LvJvcI38fPXSyw8jl-WdPX6axuT-lgKqg-BIlsynRGL1FPc3dDJzKQvx1u-xl5_9jJNTo', 1497334554782, 1497334554782),
(155, 'samsung SM-N9208', '7.0', '11 (11)', '1115fb704ed80a04', 'fTq717NJ-JY:APA91bGWH6FH8JfD0f7hMPjqcp-ozkYSw8HGdfArCK9WLaqAX4Oxz_pr2POlzZrUgXgmDqT8Z128DIMC937BbuJZF8fcQDclHClm7Q_8XFnD72QkxhwgY04pSjxv-1gPRdsN-iW9X4g_', 1497334555995, 1497334555995),
(156, 'samsung SM-G935F', '6.0.1', '11 (11)', 'ad08160330e91d1a2c', 'fmf1Dh_G2b0:APA91bFxoxrwPZfknTMb6tlxVfNDKYYJzeANCTgCE4pqOml_kMzQ0QgmAV9Bz3vd-dl1HIGP0osZPr_lOizuN8sSIUnp4z6OB2DUS-QoDP6e5dSNI7CQDYUtdGTMzue3u0lrWEAdIpRs', 1497334559361, 1497334559361),
(157, 'samsung SM-G935F', '6.0.1', '11 (11)', 'ce10160a40ebd12604', 'd-XLr12BUoc:APA91bFA9kZCcWl3ndL0A8eeSJa227b_-TKawYm25vxG8FSapVBoH07g2JeEtAV3DCAsPpgqdKYdBMgRyTRIsyHBWJ1D8bEiGw80n7725bM3Txlp8cylpNTexGolLhdiLOgllxOaEkwz', 1497334564951, 1497334564951),
(158, 'samsung SM-G610F', '6.0.1', '11 (11)', '3300b041d1b193cd', 'ewFei53xCVY:APA91bHW86F7Ru7suUrJBTlELRz7aUNH_Z-_xC8bxtiOaE2R_vFvJKZoCexCFJprQdMhD-9G3xbKWUJj98MPeQzuoThZyUPIQypbcErwEkOU7-_f34z7bNKU5VOUoY3dRSiJo-IMQj2N', 1497334576870, 1497334576870),
(159, 'Xiaomi Redmi Note 4', '6.0', '11 (11)', 'AIIRKZDMP7IRAYTK', 'dDiNDMJhPgY:APA91bGPPsGnc26tK8PtBk82LYiLfAI-FwrRjVL_HFQmfQVb5NlRQQWIGhaLLwPMaQg92LmodKQp91FUuzI4mQJTE2eHWj50O9R_jvmPiUyXyJDHTz4FelaAMizdrbDBYUmFtttqfljj', 1497334585740, 1497334585740),
(160, 'samsung SM-N9208', '7.0', '11 (11)', '0315353031383631', 'c8EoFkZQgac:APA91bEKgWIklUkHmLNCQyQ6yPcz4OeniaxTHpSbTmdlYgX8nGtdHwNpgF_1OBK-h7IxcyclyZL5djVp6JDVtJRVCGdOn3KdLVdIDhyfeD9tizgDf9pudTzRz6lRyvSlKMzRAKjvoqpH', 1497334590953, 1497334590953),
(161, 'samsung SM-G935F', '7.0', '11 (11)', '9886674242524b4544', 'eDZosDeOK28:APA91bEk6FnX5nVno8oOe6302BNA96bNmzaWwwMIpex4dwDEHaG3aeaSgAAiCNMMspsqJloWhIdNhDozmpUOsBorOx0g6NFgkNOft05mDXkwWsaPjDd0W1J5LBi7Rlh0xdwg2ox1lkcR', 1497334604584, 1497334604584),
(162, 'samsung SM-N910H', '6.0.1', '11 (11)', '4100983fe4749163', 'e_KDDF4P9IA:APA91bGm6U45aYBLlL37VscurDmDMvZsaMGdZuDcbR1B6iY1PBJbcOsSn6pWOjcD7R47kO8OauXOqA5ec7wLFq-Fnu6mMixXFOEOsgWX131O2gWWRSal7mWKyBYszkC8aGDz7Cs-ciON', 1497334618270, 1497334618270),
(163, 'Xiaomi Redmi Note 4X', '6.0.1', '11 (11)', '8dc4d2f29804', 'eH0E2rxrKIs:APA91bEYU6Hl2UGCpmwOdmseYeiSz9HPKJxf63i41BA4yXTDlmd6gSgZJjjndzZw49zpj6POKpITaS9CcVC_i2KM0t5ljN-aKpMP8IIFQpeYm4-gi11ilG_7lSBfQbz4_36YDuUXgjlo', 1497334636030, 1497334636030),
(164, 'samsung SM-N910H', '6.0.1', '11 (11)', '41008719e4938193', 'd9DXAiD1tSQ:APA91bFbmL2ZqPXUxHJU984VkQOUXoHUSZDIqOVvHEdcz3YqUVReHxl3s_zvqBGmWZuYQ2T16RhKwRb90kEfgCBmNfeuSiIFdk4tSLG5Dm2AZH-dcjRjibDmTXwlAyi56XlsFhl5QiFd', 1497334637938, 1497334637938),
(165, 'Xiaomi MI 5', '7.1.2', '11 (11)', 'e787b887', 'dzQGVoMUQuM:APA91bG8YswLjn2AKFcMIFfwxzxyOeH5B4ogI69y29xgwmLr1zbnKHU0fgLJH_DGTMDBefq3izXDcbPz1uvB4FkcIeiHIvkLLCcOaQhWAq4CVQx368bQJAhY36TYtaeKvOq3BfjPmo8l', 1497334640017, 1497334640017),
(166, 'samsung SM-G935F', '7.0', '11 (11)', '9885f332483552594e', 'fJoMNEdMUQY:APA91bEPD24sNMtFhPdbj6wTv1coTEFFGP1kb7c4-MCsUUFv-aDwccujBVEDY7349AgL1DUuB8VyYdf8gbdzuQ199naaZWjOqu6AykH1UcfVz_0uZmY49MBP-T7VaKzqwktaur026IV_', 1497334698184, 1497334698184),
(167, 'Sony F3216', '6.0', '11 (11)', 'WUJ01NRWMB', 'fXCqli5d2JY:APA91bFPNgTILrL43erPEz7YD-c96HnaPpM0SbrWfHK3E3sTDCBIf0Rq-BMXnl9yLwdte8qHpz9HmbYSCCt0H5O4ehMWeL8IMSygKNuWOMnUzhdwGOr3LfAQIT8Agw6hAkrw7WvUdsot', 1497334705555, 1497334705555),
(168, 'Xiaomi 2014817', '4.4.4', '11 (11)', '3a254f3', 'e9rQXNlRIgg:APA91bGGiNBHZ6xP5UOXLY5b_AzPg0oGkFB9BfuyHQq68fP3AtgjXRHAD-eCplHaMi-4kcmoA2-VYgliUkY2nujhRQnV0Nl_WoxNjD525rwhXFF--wVA46g4itXcjHHIB-CkF9RLys0v', 1497334705828, 1497334705828),
(169, 'samsung SM-G935F', '7.0', '11 (11)', 'ce041604a28f963404', 'f7ib0lLAydk:APA91bFzNk_SKsoxYV9Fq6jgCyayZ32ZWBvkmmYydx8tnQMvqiFRwVVDFZJDvil3-L1gsRusDK3wlRV_2QG8_Dtt176QSzTZpJ8Zypp-6J5S44TmygRXrcJe1MznXa_tw5crmcg4D5uN', 1497334776960, 1497334776960),
(170, 'samsung SM-A520F', '6.0.1', '11 (11)', '5210ea37be91b365', 'e-btpHzNsRk:APA91bE4ffOA_oU6m9wWlyaumAor2UW0P3OThHYAvKSNRuVR4G5DAXmtCYRThUI0qE2YPAdQDYpwZmdFavDUEj2WvGiYOUX4C0TZCUP-5dgdbJGMPg1GBXyGuytSDYjDHE8xrlfI-xiN', 1497334795214, 1497334795214),
(171, 'motorola Nexus 6', '7.0', '11 (11)', 'ZX1G425HRP', 'eGnRZ_mf8To:APA91bFB2qquslIECBkezB0jkXKYt_F6WCzjG6KUWJynZyk58aPLsodDLl9cAfeXgQIZX3NVccPd2xKCA8u8Tz4wGuTQ8XfuegZJV5aU54lH97avyHSCFEYy02aby1jQtIRIEkVVLWyE', 1497334841821, 1497334841821),
(172, 'samsung SM-G920F', '5.0.2', '11 (11)', '04157df43073e509', 'e7nCxvQxK64:APA91bF6vDd3YVZ994wUk-t2qkDTJ2oejqpEXG-8eO6aEIxy8aWmOJaJMyebxkt_7WcjYSYcjLybqocmZQ5_ALU-OZhhJ5Vc4UrA1oepreTq9ZHXFKP5lNYAqh1v6e5JC3W5CD9ryuRS', 1497334845701, 1497334845701),
(173, 'samsung SM-G935F', '7.0', '11 (11)', 'ce04160468cf9b0304', 'dpQxOmOkue4:APA91bFPmSfIw2Q4bCyZONVs2IawNL2rJah3QDTUxEK9Z5VMShmRcd-pY5CrnewRhgM29ibLjX7XFdwqRHzEnFjQPufnCv3H5-0tGy1RMFMYEAtKRSUAzqKYfB0VVphn8aUdkjDEZYxB', 1497334856478, 1497334856478),
(174, 'Xiaomi Redmi Note 3', '5.1.1', '11 (11)', '1d8fc537', 'd1ZfSsr62FI:APA91bHnVLRCQXP68j50lY68rFVc6ElAa00gMyxH2a8LPuZf-UJsH6uE2-Ea30WmQt6rpDhXq2CGkyo1ov1lq7jf6ky_v-5sK1r6oqb5bGHhphxlStE-H3146L_X-K-YqC03Lr-nYvgp', 1497334862735, 1497334862735),
(175, 'samsung SM-A510F', '6.0.1', '11 (11)', '33006547a103b217', 'eoIGfn0_wUA:APA91bGEC05SHyZc7uo6sAWQRpw91PbyTdMADQA8-Zp8vyUAtDnFIXdlkuM0E3j_NFKPv8fT4JdYZj8KYFZkKxCzvXyqauUdO7HpQnOJ7_v3kiMFUtJn4Fx7tchEiTNKZ1s2TyygjPUI', 1497334882119, 1497334882119),
(176, 'samsung SM-G920F', '5.1.1', '11 (11)', '1115fb7162692505', 'eN4ReMT1h1Y:APA91bGn1KM2y2b97ytcoQt5nIzLZ2ZjAV44ojA_eYPTkAsZoC9V_oTrnIXxM0yp78-tM_6P7w-6sBqjpv8ppW5WZF55mae2mmPjA6xbPEyiK6wvD7juarsHBW-c9qYLVIC7YDYuqf_h', 1497334963196, 1497334963196),
(177, 'samsung SM-G935F', '7.0', '11 (11)', 'ce0416042529160302', 'cF_GevvRyCc:APA91bFD-tBiFFKU6w6uKozN-c3hSwI4eL_yPeopYZ3tazrwHko7Qg85MjT9pGmVPT-_DCb-cn9Mf-6A_tckscOljPiJ-zWB16PJPNB7DPrb23glCSEAXpPKtgMzRnvFjpQiL8uS1RqJ', 1497335022789, 1497335022789),
(178, 'samsung SM-N910H', '5.0.1', '11 (11)', '4100e423f2df9109', 'cOMlr6s45DY:APA91bElRUZuLUN6hNoEiizdmkEaOAx6J9e7TUi5Q7TnmHeeAUx_cIu11HQkIIdoh5CDP7wT-IFRSF2Vm3_mZuqYu8DRNRCZENFF8d-QBjSjE0rDX-MmPrD7D7ML73G8Ii16r1txh8GT', 1497335028617, 1497335028617),
(179, 'samsung SM-N9005', '5.0', '11 (11)', '10a9786a', 'cwg18sdVLzE:APA91bHaFQdu75fiVwgYPJWjW2C21tgFTx8VDLo1DCV7_B4QMU0WXLvlLdr2K1BeG8E42QT4yGbqtsego-ie79COCTabjSYGTkXzYZo9ZXiWNLis0tBrpf08NIyDm2BV67gZQm3eXMt7', 1497335063242, 1497335063242),
(180, 'samsung SM-G920F', '6.0.1', '11 (11)', '02157df28c490820', 'enGIcHAwaUQ:APA91bGWlgpussJ3nyeMDj_d3e3U2poVnXkwVPvS8gJrvRemAu8Ar0Ufx1230cdxmWGJl4uJhXyymzpDcAK1KY5VoHgPif8Jm6eSq_CFmPHJ4CxjABcdJiruN9vuDgWj5Br0aQxbtLWc', 1497335102431, 1497335102431),
(181, 'Xiaomi Redmi Note 3', '6.0.1', '11 (11)', '9f22c00e', 'dCyhJB4J68w:APA91bHpJAwUDryqabaA46Ju0ZXGUYN6KZMywULRDygg9QhYwnU3hqKCYwPRVnOxCMQ6UFieRa84ys-ymR78gzbxMXKy6Vg0Mr2VpyPLYREOBSjAEqjUegTN6ptZVd1v6a5-Q4dRogzR', 1497335211454, 1497335211454),
(182, 'samsung SM-A310F', '6.0.1', '11 (11)', '3100e0d18ea132c9', 'fG3OiLptqOg:APA91bFiVRK8C-2KDs7lxTNCE3FsOwCLrTzQm-VCD2PxqGqNkX9FcQcM2cnv7kRwYAY5Mx_VCp4RAkgB3KPTvG7eiTlHYv4m3tfPrsBMI49ZdDOR82Ll8YDe-WgHrf5I2mUHkoRv9S--', 1497335244638, 1497335244638),
(183, 'LENOVO Lenovo P70-A', '5.1', '11 (11)', 'LBMN8995SCTGO7PR', 'euIS4ej9iuU:APA91bEdzzXEXW7329bSscoxXecjr6CurjwqfP0H3QD0K5mZtZlCsLj-GAUXBJU8Rpez8MWEvWHM-wHulp9CBSWbby_HexJ9Hd-RU_R7Udewn-xaJ_geZuqROpfFr20uk0TCkQ4ThaBU', 1497335348071, 1497335348071),
(184, 'Xiaomi MI 4LTE', '4.4.4', '11 (11)', '5b766131', 'fOUfAIiJyCI:APA91bEnKQkP4FYqTof2eVQ62_czEwoqtiDmqgk1xJWUauZsEvuYAiqIKSCjBePbYMPrLtDDLFFQQAg1ffm1KEQgY8uA3534M5ZjiFkq41dXInWJzeZ3qcTQr7ZlOL-GF11Hd71-aDIx', 1497335403997, 1497335403997),
(185, 'samsung SM-G935F', '6.0.1', '11 (11)', 'ad07160328acd59302', 'cN8QyvteFas:APA91bEqStZfmPN9FCdAwtiK4md5uiNzZQTU1fcL7H_kO6l98K-C0P8hPOFwUc0W7Vaw4J64GaiMF746OZV-7bHEXrRW-zbU-35wXUyLsqcZOhc_qhYP1GU_k9IOf2g8aT7O6y7Ki7jV', 1497335623396, 1497335623396),
(186, 'samsung SM-N910G', '6.0.1', '11 (11)', '1b32d821', 'f-pfTM-Wv4o:APA91bF3Aj14ckHWgA9AbL9Bo_0dEFeZ3paW0K48C0CZPnnfumv3AbMrXddjLIKDi7echcDonRRcIDSQsyMH3H0t09ixmggSag2lfeGeGWMEtLgA3Az9ENcP-zwkqIYFl4M0dyFlYTD6', 1497335865547, 1497335865547),
(187, 'samsung SM-A510F', '6.0.1', '11 (11)', '330082adbe2bc291', 'fFGnx2aoRP4:APA91bEGnbMbjIsfD80w7ee6V7rbH9z1JWUP-_ZrroWqQiB0iaFbg2nDSV3vNr_LjXU0OTSZVholsTw3TO9NoK8B_zmS_vVg8wjB9v3FDoQ1u0vvsQCalwYxFnxmrgY9FU_wOpYGgBGP', 1497337005000, 1497337005000),
(188, 'Xiaomi Redmi Note 4', '6.0', '11 (11)', 'AAMB7HEYD6USYSTO', 'cFGn-dIn7QU:APA91bEAf4-3ehsZuEsENLvrQ0bxXEkMZFdfdF7Z6CLGUzV4GE2Nnw6aC39KN7loR5SLT6r2M5XXkLVCn6bsLU8-RY4eywEAAz16WeZMAfUpwm6fTe8YI1fgW6iA5wolvvBmOiead0d8', 1497338175663, 1497338175663),
(189, 'Xiaomi Mi 4i', '5.0.2', '11 (11)', '6968d935', 'c6ZCvpU3Xs0:APA91bGFzI1AtzcyS9w80KEvKlyDo1MOLGhiaornZZoAahMWvjHN1BFdaqHWio2spgpBHL2-lUgcpgx013sRb_I2kAYJ0nLwX7bqX2HohSXLtWyqmOCsvBcp3Ump2LZmNGds4uXwJzCX', 1497338392441, 1497338392441),
(190, 'samsung SM-N910G', '6.0.1', '11 (11)', 'd1e165df', 'ePyCU7lDjaI:APA91bHEowFGRwDoIByPhCtm6Abxa8bQsQXwuAUomO97JovEDt0IuZAvLFf9RkjGX3J3G8kBuq-E0CaBlQlpK9_-CaAGf3cwD2-Z5wNBFprPxrKdMHz-J12tuChAFfsj7yXsBOhCqu7n', 1497340531477, 1497340531477),
(191, 'samsung SM-A510F', '6.0.1', '11 (11)', '33003962496b4371', 'dWPHw8rZG54:APA91bEySRddggKFbefs68lZ31VN2tHZtipkI1wttsUQfhMhtIJGZJGlnI-VYPtz5Y4nAZHQLrNGwSo_VMNFHrWsaFbCy57XFmYqA_IpmFuUv9MWdFC1vuZ08n1ElTjsXPT42yUrV3j8', 1497343166518, 1497343166518),
(192, 'samsung SM-G935F', '7.0', '11 (11)', 'ce10160a40fb911f04', 'cSOs5O5rPKo:APA91bG3bs-Zy_QkcCjaUxiRenFLI9Pch-ZTG3AZWQFbYrDko4HBjkGEYkBGzcRqL_fKjGuq4A5j4oa4aRUsRZ5OgKv3WKy4xVTEEpmxEcG2g5VwXTI-lVd_4DWWIuqXPvpjzdtvZaJ3', 1497343904349, 1497343904349),
(193, 'HTC One ME', '5.0.2', '11 (11)', 'FA56HB102379', 'ehKyOXJXzII:APA91bFl_Y29V2Z5j87LDvCnE3kKfkT5iLG1k-L0ryg0kO-xX_4MsTq4K_k3iNcwaU2e5t5c_NwuRsiLxgxMsaCnOiP7w2v4nqSm7_0CZJ3GFqbpHKV4KSx3q6l9ExT-j2yNJABA1FqR', 1497348636292, 1497348636292),
(194, 'samsung SM-G930F', '7.0', '11 (11)', '9886674e54414e3155', 'eusp6zxaQug:APA91bFbv1vrEeWi6hrAg3701N7rwrR0bygPNNo63YMXnXvaSwfUYSN97wW5rJ1wIdQbA6ci6s9Vqxf6BT51fZqZnLCQBY0O-oazYn4Wj13WALOwgoYVSnFcT07t2IrGdoVfDg8hJwBW', 1497362240166, 1497362240166),
(195, 'Xiaomi Redmi Note 2', '5.0.2', '11 (11)', 'I7NZ7HMBAAD6UOOR', 'eTLSRi7PB8I:APA91bFrDjASx-payT-OUQkwoNr8Vpdo7Z1D_KmvIO1FX3Jkid4Up4lMYvN0NQI6cm8HTLbQzAXjJZgxt1vOrmtGqIcZEtuPrlPS7dxeYSpEvcCmbXr-dOulZcLoR8nc7MCZM8n8a5e8', 1497404267881, 1497404267881),
(196, 'samsung SM-G920F', '7.0', '11 (11)', '03157df3d946552b', 'cTsQ7S2qg0A:APA91bE-lqJyjYALFdqXe0-mabmfiFDDCC3YbSlSQzHtlgiCwWBNBqBa1bExi1iPdG4JkMQb_epuACEHCbkgzH_W4yXyQf69mMEGDiGlKZyZ5Oe0h6Sv1-r0x7irxCyJZhH0I8cmc4K5', 1497407826061, 1497407826061),
(197, 'samsung SM-G930F', '7.0', '11 (11)', '9886334b53394a4b32', 'dzuiGeUBEQI:APA91bFdnvMuZ6YcfNXTlfFmLhfW5cDU2qIEEDAQfzQfEwcSh-_XyO00vNYrRcUMbc_TSTxNR-Pt-i9WOI99lAQaC48LhLv_V-RpwaP5yeDnT73SaKE-XlOlED4fGxAD-e7S--qafUhN', 1497408221688, 1497408221688),
(198, 'asus ASUS_Z010D', '5.0.2', '11 (11)', 'G4AXGF004217EFV', 'dbC_wLkv53g:APA91bG3iiP1lkdX8SmFHMbvI-OqlP8SMyC_uunioyEoNJSHImCFFANQEoFjmtvMoBVxkWoiv-adfKOh6i-L1Xl5vP1eBRQjxkyWUxJhghzWott-HVtyigTZYhAPUeNaChyJMqafTuT4', 1497431764333, 1497431764333),
(199, 'LENOVO Lenovo P1a42', '6.0.1', '11 (11)', '7dfecae0', 'feYzne0T-y8:APA91bF6jn55kvoDCfE9HU8P8FTSG8_SjwE7Npbw846YYOUZrUJj0T0_jFZiyxzLin1rg-G-MX_hBtyhraHQdBeXauC54_ZTa3zSzCIS5iKPoxSTYVCYts6q1itlDJtO3x3PSmU0PYSZ', 1497441053727, 1497441053727),
(200, 'asus ASUS_Z012DB', '7.0', '11 (11)', 'GCAZGF014182CBZ', 'euGOdxBAZOg:APA91bFpmcJNYrcHgI_sQBUbnXMCDIbaUGyz4IvMsefSCv6OaCGqp1oeQJ6A1ndeKa1tEzdn8-_EsFa--5P8M3A0-6LGn7YMRiz3-E2eY0tGBcEyftTOK3hE0coCTGdhOcI_JwyieTl5', 1497451230338, 1497451230338);

-- --------------------------------------------------------

--
-- Table structure for table `merchant`
--

CREATE TABLE `merchant` (
  `id` int(11) NOT NULL,
  `email` varchar(30) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `alamat` varchar(39) NOT NULL,
  `date_created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `news_info`
--

CREATE TABLE `news_info` (
  `id` bigint(20) NOT NULL,
  `title` varchar(100) NOT NULL,
  `brief_content` varchar(200) NOT NULL,
  `full_content` text NOT NULL,
  `image` varchar(110) NOT NULL,
  `draft` tinyint(1) NOT NULL,
  `status` varchar(50) NOT NULL,
  `created_at` bigint(20) NOT NULL,
  `last_update` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `news_info`
--

INSERT INTO `news_info` (`id`, `title`, `brief_content`, `full_content`, `image`, `draft`, `status`, `created_at`, `last_update`) VALUES
(11, 'Paket Umrah Ramadhan MTT', '10 Hari awal Ramadhan (29 Mei)\n15 Hari akhir ramadhan (12 Juni)', '<p>Majelis Taâ€™lim Telkomsel menyelenggarakan paket Ibadah Umroh dengan \nbeberapa paket yang menarik dan harga terjangkau. Pelayanan Prima, \nKemudahan dan Pembinaan Agama bagi jamaah umroh menjadi prioritas kami.</p>\n<p>Pemesanan bisa pilih menu travel disini, selanjutnya kami akan mengkontak anda.<br></p><p>Info lebih lebih lengkap klik :&nbsp;http://www.mtt.or.id/umroh-mtt-2/</p>\n<p>atau hubungi langsung : 081285688212</p><br>', 'Paket Umrah Ramadhan MTT.jpg', 0, 'FEATURED', 1495336887152, 1495336887152);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` bigint(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `merchant_id` int(11) NOT NULL DEFAULT '5',
  `category_id` varchar(50) NOT NULL,
  `image` varchar(110) NOT NULL,
  `price` decimal(12,2) NOT NULL,
  `stock` int(10) NOT NULL,
  `draft` tinyint(1) NOT NULL,
  `description` text NOT NULL,
  `status` varchar(50) NOT NULL,
  `created_at` bigint(20) NOT NULL,
  `last_update` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `name`, `merchant_id`, `category_id`, `image`, `price`, `stock`, `draft`, `description`, `status`, `created_at`, `last_update`) VALUES
(1, 'Nasi Pecel Madiun (MTT)', 5, '', 'Nasi pecel madiun.jpg', '19000.00', 100, 0, 'Nasi pecel madiun nikmat<br>', 'READY STOCK', 1495705459000, 1495705459000),
(2, 'Nasi Gado Gado (MTT)', 5, '', 'Nasi Gado Gado MTT.jpg', '20000.00', 100, 0, 'Nasi Gado Gado<br>', 'READY STOCK', 1495705459000, 1495876377748),
(3, 'Nasi Ayam Komplit (MTT)', 5, '', 'Nasi Ayam Komplit MTT.jpg', '26000.00', 100, 0, 'Nasi ayam plus sayur<br>', 'READY STOCK', 1495705459000, 1495705459000),
(4, 'Nasi Bakar (MTT)', 5, '11', 'Nasi Bakar MTT.jpg', '22000.00', 99, 0, 'Nasi Bakar isi Teri dan Jamur', 'READY STOCK', 1495705459000, 1495705459000),
(5, 'Nasi Sayur (MTT)', 5, '', 'Nasi Sayur MTT.jpg', '15000.00', 100, 0, 'nasi sayur<br>', 'READY STOCK', 1495705459000, 1495705459000),
(6, 'Indomie Telor (MTT)', 5, '', 'Indomie Telor MTT.jpg', '10000.00', 100, 0, 'indomie telor<br>', 'READY STOCK', 1495705459000, 1495705459000),
(7, 'Indomie Telor Plus sosis, nugget, keju (MTT)', 5, '', 'Indomie Telor Plus sosis nugget keju MTT.jpg', '15000.00', 100, 0, 'Indomie telor pus<br>', 'READY STOCK', 1495705459000, 1495705459000),
(8, 'Gorengan Bakwan,Tahu (MTT)', 5, '', 'Gorengan BakwanTahu MTT.jpg', '3000.00', 99, 0, 'Gorengan Bakwan, Tahu', 'READY STOCK', 1495705459000, 1495705459000),
(9, 'Habatussauda', 5, '', 'Habatussauda.jpg', '60000.00', 100, 0, 'Obat Herbal Favorit Habatussauda<br>', 'READY STOCK', 1495705459000, 1495705459000),
(10, 'Kopi Hijau Diet Kopi Kawi', 5, '', 'Kopi Hijau Diet Kopi Kawi.jpg', '65000.00', 99, 0, 'Diduga, asam klorogenat ini memiliki banyak manfaat&nbsp;terhadap kesehatan. <b>Kopi hijau</b>\n menjadi populer untuk menurunkan berat badan setelah ditampilkan pada \nacara Dr. Oz pada 2012. Dalam acara itu disebutkan bahwa biji kopi jenis\n ini dapat membakar lemak dengan cepat tanpa olahraga tambahan.', 'READY STOCK', 1495705459000, 1495705459000),
(11, 'Kurma Date Crown Khalas 1 kg', 5, '', 'Kurma Date Crown Khalas 1 kg.jpg', '74000.00', 100, 0, 'Kurma Date Crown Khalas 1 kg', 'READY STOCK', 1495705459000, 1495705459000),
(12, 'Kurma Date Crown Khenizi 1 kg', 5, '', 'Kurma Date Crown Khenizi 1 kg.jpg', '78000.00', 100, 0, 'Kurma Date Crown Khenizi 1 kg', 'READY STOCK', 1495705459000, 1495705459000),
(13, 'Kurma Dattes 1 Kg (Khalas)', 5, '', 'Kurma Dattes 1 Kg Khalas.jpg', '60000.00', 68, 0, 'Kurma Dattes 1 Kg (Khalas)<br><div>Awali buka puasa anda dengan kurma golden dates khalas</div>', 'READY STOCK', 1495705459000, 1495705459000),
(14, 'Kurma Golden Dates 1 Kg (Sayer)', 5, '', 'Kurma Golden Dates 1 Kg Sayer.jpg', '52000.00', 100, 0, 'Kurma Golden Dates 1 Kg (Sayer)', 'READY STOCK', 1495705459000, 1495705459000),
(15, 'Madu Lengkeng Al Amin', 5, '', 'Madu Lengkeng Al Amin.jpg', '125000.00', 100, 0, 'Madu Kesehatan <br>', 'READY STOCK', 1495705459000, 1495705459000),
(16, 'Madu Pahit Az Zikra', 5, '', 'Madu Pahit Az Zikra.jpg', '100000.00', 100, 0, 'Madu pahit penuh khasiat<br>', 'READY STOCK', 1495705459000, 1495705459000),
(17, 'Madu Super Liar Al Amin', 5, '', 'Madu Super Liar Al Amin.jpg', '105000.00', 100, 0, 'Madu Super Liar Al Amin<br>', 'READY STOCK', 1495705459000, 1495705459000),
(18, 'Minyak Habbatussauda', 5, '', 'Minyak Habbatussauda.jpg', '75000.00', 100, 0, 'â€œSesungguhnya di dalam Habbatussauda (jintan hitam) terdapat penyembuh bagi segala macam penyakit kecuali kematian.â€ (HR. Bukhori &amp; Muslim)', 'READY STOCK', 1495705459000, 1495705459000),
(19, 'Minyak Zaitun Extra Virgin', 5, '', 'Minyak Zaitun Extra Virgin.jpg', '70000.00', 100, 0, 'Minyak Zaitun Extra Virgin', 'READY STOCK', 1495705459000, 1495705459000),
(20, 'Siwak Al Hijazi', 5, '', 'Siwak Al Hijazi.png', '10000.00', 100, 0, 'Siwak Al Hijazi', 'READY STOCK', 1495705459000, 1495705459000),
(21, 'Kripik KEPO', 5, '', 'Kripik KEPO.jpg', '15000.00', 99, 0, 'Kripik Enak MTT<br>', 'READY STOCK', 1495705459000, 1495705459000),
(22, 'Celana Sirwal Eksekutif', 5, '', 'Celana Sirwal Eksekutif.jpg', '110000.00', 100, 0, '<p>Celana Sirwal Serba Guna, bisa dipakai dalam berbagai acara.</p>\n<p>Bahan nyaman, Tersedia aneka pilihan : Biru, Krem, Coklat</p>', 'READY STOCK', 1495705459000, 1495705459000),
(23, 'Kemeja Taqwa Bordir', 5, '', 'Kemeja Taqwa Bordir.jpg', '90000.00', 100, 0, 'Kemeja Taqwa Bordir , tersedia aneka pilihan warna : Biru, Orange, Coklat, Merah', 'READY STOCK', 1495705459000, 1495705459000),
(24, 'Kopiah', 5, '', 'Kopiah.jpg', '10000.00', 100, 0, 'Kopiah Haji , tersedia 3 warna : Hitam , Putih dan Abu-abu', 'READY STOCK', 1495705459000, 1495705459000),
(25, 'Tas Ransel Eiger MTT', 5, '', 'Tas Ransel Eiger MTT.png', '340000.00', 100, 0, 'Tas Ransel Eiger MTT', 'READY STOCK', 1495705459000, 1495705459000),
(26, 'Ruko di Apartement Bogor Valley', 5, '', 'Ruko di Apartement Bogor Valley.jpg', '375000000.00', 1, 0, 'Ruko di Apartement Bogor Valley, untuk lebih detail bisa kontak MTT<br>', 'READY STOCK', 1495705459000, 1495705459000),
(27, 'Rumah di Bekasi Selatan', 5, '', 'Rumah di Bekasi Selatan.jpg', '625000000.00', 1, 0, '<p>Lokasi : Taman Galaxi Indah 1 Bekasi Selatan</p>\n<p>Lokasi sangat-sangat strategis. Posisi depan Masjid Persis (Masjid \nal-Muhajirin), hanya 2 menit dari pusat pelayanan publik (Kantor Polsek \ndan Kantor Koramil, KUA, Puskesmas, SMPN 12 Bekasi, SMUN 3 Bekasi, \nKantor Kelurahan, Kantor Kecamatan, Masjid Jamie Muhammad Ramadhan, \nWisata Kuliner) , 5 menit dari pusat perbelanjaan Mall Grand Galaxi Park\n dan Pusat Niaga, kolam renang Tirta Mas , 10 menit dari tol penghubung \nJakarta pintu tol Cikunir dan pintu tol Bekasi Barat.</p>\n<p>Luas : lt. 90 m lb. 90 m,</p>\n<p>Kondisi : 3 kamar tidur , &nbsp;1 kamar mandi, listrik 1300watt, air tanah kencang, jln cor bisa lewat 2 mobil</p>\nHarga masih bisa nego\n<p>Minat Hubungi : 081285688212</p>', 'READY STOCK', 1495705459000, 1495705459000),
(28, 'Umroh Bersama MTT', 5, '', 'Umroh Bersama MTT.jpg', '26000000.00', 70, 0, '<p>Majelis Taâ€™lim Telkomsel menyelenggarakan paket Ibadah Umroh dengan \nbeberapa paket yang menarik dan harga terjangkau. Pelayanan Prima, \nKemudahan dan Pembinaan Agama bagi jamaah umroh menjadi prioritas kami.</p>\n<p>Info lebih lebih lengkap klik :&nbsp;http://www.mtt.or.id/umroh-mtt-2/</p>\n<p>atau Hubungi : 081285688212</p>', 'READY STOCK', 1495705459000, 1495705459000),
(29, 'Al Quran Al Haramain', 5, '', 'Al Quran Al Haramain.png', '80000.00', 100, 0, 'Al Quran Al Haramain', 'READY STOCK', 1495705459000, 1495705459000),
(30, 'Metode Tartila (Praktis Tahsin & Tilawah)', 5, '17', 'Metode Tartila Praktis Tahsin  Tilawah.png', '35000.00', 100, 0, 'Belajar tahsin &amp; tilawah dengan praktis metode tartila<br>', 'READY STOCK', 1495705459000, 1495705459000),
(47, 'Nasi Tengkleng', 0, '', 'Nasi Tengkleng.jpg', '35000.00', 10, 0, 'Nasi Tengkleng Kambing&nbsp;<div>Bumbunta mantap pedasnya</div>', 'READY STOCK', 1495705459000, 1495705459000),
(49, 'Kurma Date Crown (Khalas) 1 Kg', 20, '12', 'Cover_Kurma_Date_Crown_Khalas_1_Kg.jpg', '75000.00', 100, 0, 'Kurma Date Crown (Khalas) 1 Kg. Premium Emirates Dates.\nKhalas : Melts in Your Mouth . Grown all over UAE farm especially the Eastern Region, it is characterized by smooth fibers and soft delicate texture, unique rich flavor with balance sweetness,light golden colour,large size fruit with high demand on its rutab during ripening season.', 'READY STOCK', 1495705459000, 1495705459000),
(51, 'Bubur Kuningan', 21, '11', '20170522_100902-1.jpg', '17000.00', 0, 0, '', 'READY STOCK', 1495705459000, 1495705459000),
(52, 'Batagor', 21, '11', '20170313_134609-1.jpg', '16000.00', 0, 0, '', 'READY STOCK', 1495705459000, 1495705459000),
(53, 'Tafsir Sesat', 5, '17', 'IMG_20170522_214058_222.jpg', '100000.00', 30, 0, 'Karya Ustadz Fahmi Salim', 'READY STOCK', 1495705459000, 1495705459000),
(54, 'Baju Taqwa Riyan', 5, '15', 'IMG_20170522_142424_179.jpg', '150000.00', 6, 0, 'Bordir depan, Belakang dan Lengan. Bahan katun nyaman dipakai', 'READY STOCK', 1495705459000, 1495705459000),
(56, 'Bangga Menjadi Ibu', 25, '17', '20170523_061559-1.jpg', '67000.00', 3, 0, '<p>Kumpulan karya pena para ibu yang menjadi finalis kompetisi penulisan \"Bangga Menjadi Ibu\". Dengan membaca buku ini, anda akan meresapi kisah-kisah perjuangan menjadi seorang ibu, memahami proses pembelajaran yang para ibu alami, dan banyak kisah penuh cinta dan kasih sayang para ibu</p>', 'READY STOCK', 1495863900032, 1495863900032),
(58, 'Duren-Durian Medan', 27, '13', '', '120000.00', 0, 0, 'Pancake Duren Medan asli dikirim langsung dari Medan :)', 'SUSPEND', 1495705459000, 1495705459000),
(59, 'Sepatu Vibram Fivefingers for Men', 23, '15', 'IMG-20170523-WA0007.jpg', '0.00', 0, 0, 'Sepatu berbentuk jari kaki yg nyaman untuk aktifitas harian dan olahraga.\nHarga tergantung model, mulai 1,35 - 2,5 jt + ongkos JNE atau Gojek.\nModel dan warna berbeda untuk laki-laki dan perempuan.\n\nContact 0811412344 utk detil produk', 'READY STOCK', 1495705459000, 1495705459000),
(60, 'Sepatu Vibram Fivefingers for women', 23, '15', 'IMG-20170523-WA0006.jpg', '0.00', 0, 0, 'Sepatu berbentuk jari kaki yg nyaman untuk aktifitas harian dan olahraga.\nHarga tergantung model, mulai 1,35 - 2,5 jt + ongkos JNE atau Gojek.\nModel dan warna berbeda untuk laki-laki dan perempuan\n\nContact 0811412344 utk detil produk', 'READY STOCK', 1495705459000, 1495705459000),
(61, 'Kurma Coklat Almond', 29, '13', 'Kurcok_Almond_48rb.jpg', '48000.00', 3, 0, 'T : Kurma yg dipake jenis apa ?\nJ : Kita pake kurma Madinah klo madinah kosong kita pake kurma Tunisia. Pokoknya yg teksturnya lembut :)\n\nT : Bahan2 nya halal ga? Kok belum ada Logo halal ?\nJ : InsyaAllah NaaiNa menggunakan bahan2 premium yg berlabel halal, kita makan, anak2 makan dan kita pastikan semua halal. Logo halal Mui alhmdulillah sdh keluar :)\n\nSalam sukses with KurCok NaaiNa :)', 'READY STOCK', 1495705459000, 1495705459000),
(62, 'Kurma Coklat Durian', 29, '13', 'Kurcok_Durian_38rb.jpg', '38000.00', 2, 0, 'T : Kurma yg dipake jenis apa ?\nJ : Kita pake kurma Madinah klo madinah kosong kita pake kurma Tunisia. Pokoknya yg teksturnya lembut :)\n\nT : Bahan2 nya halal ga? Kok belum ada Logo halal ?\nJ : InsyaAllah NaaiNa menggunakan bahan2 premium yg berlabel halal, kita makan, anak2 makan dan kita pastikan semua halal. Logo halal Mui alhmdulillah sdh keluar :)\n\nSalam sukses with KurCok NaaiNa :)', 'READY STOCK', 1495705459000, 1495705459000),
(63, 'Kurma Coklat GreenTea', 29, '13', 'Kurcok_Greentea_38rb.jpg', '38000.00', 4, 0, 'T : Kurma yg dipake jenis apa ?\nJ : Kita pake kurma Madinah klo madinah kosong kita pake kurma Tunisia. Pokoknya yg teksturnya lembut :)\n\nT : Bahan2 nya halal ga? Kok belum ada Logo halal ?\nJ : InsyaAllah NaaiNa menggunakan bahan2 premium yg berlabel halal, kita makan, anak2 makan dan kita pastikan semua halal. Logo halal Mui alhmdulillah sdh keluar :)\n\nSalam sukses with KurCok NaaiNa :)', 'READY STOCK', 1495705459000, 1495705459000),
(64, 'Kurma Coklat Kacang', 29, '13', 'Kurcok_Kacang_38rb.jpg', '38000.00', 2, 0, 'T : Kurma yg dipake jenis apa ?\nJ : Kita pake kurma Madinah klo madinah kosong kita pake kurma Tunisia. Pokoknya yg teksturnya lembut :)\n\nT : Bahan2 nya halal ga? Kok belum ada Logo halal ?\nJ : InsyaAllah NaaiNa menggunakan bahan2 premium yg berlabel halal, kita makan, anak2 makan dan kita pastikan semua halal. Logo halal Mui alhmdulillah sdh keluar :)\n\nSalam sukses with KurCok NaaiNa :)', 'READY STOCK', 1495705459000, 1495705459000),
(65, 'Kurma Coklat Keju', 29, '', 'Kurcok_Keju_38rb.jpg', '38000.00', 4, 0, 'T : Kurma yg dipake jenis apa ?\nJ : Kita pake kurma Madinah klo madinah kosong kita pake kurma Tunisia. Pokoknya yg teksturnya lembut :)\n\nT : Bahan2 nya halal ga? Kok belum ada Logo halal ?\nJ : InsyaAllah NaaiNa menggunakan bahan2 premium yg berlabel halal, kita makan, anak2 makan dan kita pastikan semua halal. Logo halal Mui alhmdulillah sdh keluar :)\n\nSalam sukses with KurCok NaaiNa :)', 'READY STOCK', 1495705459000, 1495705459000),
(66, 'Kurma Coklat Kismis', 29, '13', 'Kurcok_Kismis_38rb.jpg', '38000.00', 2, 0, 'T : Kurma yg dipake jenis apa ?\nJ : Kita pake kurma Madinah klo madinah kosong kita pake kurma Tunisia. Pokoknya yg teksturnya lembut :)\n\nT : Bahan2 nya halal ga? Kok belum ada Logo halal ?\nJ : InsyaAllah NaaiNa menggunakan bahan2 premium yg berlabel halal, kita makan, anak2 makan dan kita pastikan semua halal. Logo halal Mui alhmdulillah sdh keluar :)\n\nSalam sukses with KurCok NaaiNa :)', 'READY STOCK', 1495705459000, 1495705459000),
(67, 'Kurma Coklat Kacang Mede', 29, '13', 'Kurcok_Mede_41rb.jpg', '41000.00', 3, 0, 'T : Kurma yg dipake jenis apa ?\nJ : Kita pake kurma Madinah klo madinah kosong kita pake kurma Tunisia. Pokoknya yg teksturnya lembut :)\n\nT : Bahan2 nya halal ga? Kok belum ada Logo halal ?\nJ : InsyaAllah NaaiNa menggunakan bahan2 premium yg berlabel halal, kita makan, anak2 makan dan kita pastikan semua halal. Logo halal Mui alhmdulillah sdh keluar :)\n\nSalam sukses with KurCok NaaiNa :)', 'READY STOCK', 1495705459000, 1495705459000),
(68, 'Kurma Coklat Misju (Kismis Keju)', 29, '', 'Kurcok_Misju_41rb.jpg', '41000.00', 3, 0, 'T : Kurma yg dipake jenis apa ?\nJ : Kita pake kurma Madinah klo madinah kosong kita pake kurma Tunisia. Pokoknya yg teksturnya lembut :)\n\nT : Bahan2 nya halal ga? Kok belum ada Logo halal ?\nJ : InsyaAllah NaaiNa menggunakan bahan2 premium yg berlabel halal, kita makan, anak2 makan dan kita pastikan semua halal. Logo halal Mui alhmdulillah sdh keluar :)\n\nSalam sukses with KurCok NaaiNa :)', 'READY STOCK', 1495705459000, 1495705459000),
(69, 'Kurma Coklat Nutella', 29, '', 'Kurcok_Nutella_43rb.jpg', '43000.00', 4, 0, 'T : Kurma yg dipake jenis apa ?\nJ : Kita pake kurma Madinah klo madinah kosong kita pake kurma Tunisia. Pokoknya yg teksturnya lembut :)\n\nT : Bahan2 nya halal ga? Kok belum ada Logo halal ?\nJ : InsyaAllah NaaiNa menggunakan bahan2 premium yg berlabel halal, kita makan, anak2 makan dan kita pastikan semua halal. Logo halal Mui alhmdulillah sdh keluar :)\n\nSalam sukses with KurCok NaaiNa :)', 'READY STOCK', 1495705459000, 1495705459000),
(70, 'Kurma Coklat TinCot (Buah Tin Apricot)', 29, '13', 'Kurcok_TinCot_48rb.jpg', '48000.00', 2, 0, 'T : Kurma yg dipake jenis apa ?\nJ : Kita pake kurma Madinah klo madinah kosong kita pake kurma Tunisia. Pokoknya yg teksturnya lembut :)\n\nT : Bahan2 nya halal ga? Kok belum ada Logo halal ?\nJ : InsyaAllah NaaiNa menggunakan bahan2 premium yg berlabel halal, kita makan, anak2 makan dan kita pastikan semua halal. Logo halal Mui alhmdulillah sdh keluar :)\n\nSalam sukses with KurCok NaaiNa :)', 'READY STOCK', 1495705459000, 1495705459000),
(71, 'Kurma Coklat Mix', 29, '13', 'Kurcok_Mix_43rb.jpg', '43000.00', 5, 0, 'T : Kurma yg dipake jenis apa ?\nJ : Kita pake kurma Madinah klo madinah kosong kita pake kurma Tunisia. Pokoknya yg teksturnya lembut :)\n\nT : Bahan2 nya halal ga? Kok belum ada Logo halal ?\nJ : InsyaAllah NaaiNa menggunakan bahan2 premium yg berlabel halal, kita makan, anak2 makan dan kita pastikan semua halal. Logo halal Mui alhmdulillah sdh keluar :)\n\nSalam sukses with KurCok NaaiNa :)', 'READY STOCK', 1495705459000, 1495705459000),
(72, 'Al-Qolam - Smart Hafiz', 36, '', 'Smart_Hafiz.jpg', '1850000.00', 5, 0, 'Assalamu\'alaikum Ayah Bunda ...\n\nSudah pernah dengar Smart Hafiz belum?\nItu lho, produk terbaru dari Al Qolam yang dipersembahkan bagi keluarga Muslim Indonesia... \n\nProduk ini merupakan salah satu, dari sekian banyak produk edukasi yang layak dimiliki.\n\nYuk kita intip, apa saja yaaa yang ada di dalam video player mungil portable ini... \n\n1. Sing A Song\na. Badanamu : ada 44 lagu Badanamu\nb. Songs : ada 3 lagu theme song Al Qolam ★ 3 lagu Doa ★ 5 lagu daerah ★ 6 lagu Animal Series ★ 20 lagu anak muslim\nc. Ngaji Yuk : Juz 30 oleh Qari Anak Farid ★ Juz 30 oleh Qariah Anak Millah ★ Maqamat Juz 30 oleh Qariah Dewasa\nd. Adzan : ada 7 maqam Adzan (Sika, Shaba, Rast, Nahawand, Jiharkah, Hijaz, & Bayyati) ★ Adzan Subuh ★ Iqomat\ne. Senandung Asmaul Husna\n\n2. Mengaji Yuk\na. Metode Al Qolam : ada 12 Bab Metode Al Qolam\nb. Asmaul Husna : ada 99 Asmaul Husna\nc. Da\'i Cilik : ada 6 materi Da\'i Cilik yang dibawakan oleh Aisyah dan Fuadi\n\n3. Cerita\na. Hari Besar Umat Islam : ada 7 Hari Besar Umat Islam\nb. Jejak Islami : ada 15 Kota yang memiliki jejak Islam di berbagai belahan dunia diceritakan di sini\nc. Sirah Nabawiyah : ada 60 judul cerita tentang kisah hidup Nabi Muhammad SAW\nd. Hafiz dan Hafizah Umroh : kisah Hafiz dan Hafizah beribadah umroh\ne. Kisah 25 Nabi : ada 25 judul cerita tentang 25 Nabi dan Rasul yang wajib kita imani, dalam bentuk story telling\nf. Khalifah Burung Bangau : kisah tentang Khalifah Burung Bangau\ng. Fable Story : ada 10 cerita tentang dongeng binatang (contoh: Kelinci dan Kura-Kura)\nh. Fairy Tale : ada 9 cerita dongeng anak internasional (contoh: Pinokio)\ni. Cerita Rakyat : ada 10 cerita rakyat Indonesia (contoh: Legenda Sangkuriang)\nj. Kosakata : ada 7 kelompok kosakata 3 bahasa (Indonesia - Inggris - Arab)\n\n4. Movies\na. Animasi Pre School : ada 9 video animasi pre school (contoh: Mengenal Bentuk dan Warna)\nb. Animasi Pendidikan : ada 6 video animasi pendidikan (contoh: Ku Tahu Nama Satwa)\nc. Seri Ibadah : ada 7 video seri ibadah (contoh: Belajar Bersuci)\n\n5. Akhlak Terpuji\nAda 9 video cerita tentang akhlak-akhlak yang terpuji (contoh: Belajar Bertanggung Jawab)\n\n6. Others\na. Produk Al Qolam : ada 11 video tentang produk Al Qolam yang bermanfaat untuk keluarga\nb. Manual : ada 2 video petunjuk manual tentang cara menggunakan smart hafiz\nc. Recording : fitur merekam \nd. Hasil Recording : untuk menyimpan dan mendengarkan hasil rekaman\ne. Music Download : folder untuk menyimpan dan menampilkan musik favorit yang ingin kita dengar melalui Smart Hafiz\nf. Movie Download : folder untuk menyimpan dan menampilkan film favorit yang ingin kita lihat melalui smart hafiz\ng. Pictures : folder untuk menyimpan dan menampilkan foto atau gambar favorit yang ingin kita lihat melalui smart hafiz\nh. My File : untuk menampilkan external memory ataupun memory card yang aktif terpasang \ni. Setting : untuk mengatur tampilan smart hafiz\n\nBanyaaaak ya Ayah-Bunda ... \nIn syaa Allah sangat lengkap dan cocok sekali untuk menemani Ananda bermain sambil belajar di mana saja, kapan saja.. Karena smart hafiz menggunakan baterai yang dapat dicharge ????\n\nYuk, hadirkan smart hafiz di tengah-tengah kehangatan keluarga ...', 'READY STOCK', 1495705459000, 1495705459000),
(73, 'Buku Mendidik Anak Laki-laki', 35, '17', 'aqwam1.jpg', '36550.00', 3, 0, 'Judul : Mendidik Anak Laki-laki\nPenulis : Dr.Khalid Asy-Syantut\nPenerbit : Aqwam\nSoftcover', 'READY STOCK', 1495705459000, 1495705459000),
(75, 'Al-Fatih vs Vlad Dracula #1 (Kegelapan)', 35, '17', 'alfatih.jpg', '65000.00', 3, 0, 'Al-Fatih vs Vlad Dracula #1 (Kegelapan)\n\nPenerbit : Salsabila\nSoft Cover\nBerat : 200 gram', 'READY STOCK', 1495705459000, 1495705459000),
(76, 'Daihatsu Xenia R A/T Deluxe 2012/2011 istimewa km baru 36rb-an', 39, '19', 'IMG_20160619_111811_hdr.jpg', '125000000.00', 1, 0, 'Xenia R A/T Deluxe 2012/2011 km36rb ASLI terawat apik istimewa banget  \nDaihatsu Xenia 1.3 R Deluxe Automatic type tertinggi dan jarang ada\nWarna Hitam Mengkilat\n\nKhusus buat yang ngerti mobil dan cari mobil bagus dan terawat bukan mobil bekas rental atau taxi online\n\nKelebihan mobil saya ini:\n1. Jarang pakai (KM 36rb-an), bisa dibuktikan keaslian-nya dari service record-nya di buku service dan di bengkel resmi\n2. Ban-ban masih tebal asli sejak baru belum pernah diganti, karena baru 36rban kilometernya\n3. Tangan kedua dari baru atas nama sendiri bukan PT, bisa dicek dari Faktur-nya\n4. Pemakaian tahun 2012, rakitan 2011 bulan December tanggal 30 (1 hari sebelum tahun 2012)\n5. Kunci serep, manual book dan service book lengkap\n6. Sudah ada monitor bawaan asli (Daihatsu genuine accessories) bisa CD, VCD, DVD\n7. Bahan bakar selalu pertamax (ada kumpulan struk pembelian pertamax-nya)\n8. Pajak masih panjang... bulan 01-2018\n9. Dokumen lengkap, STNK, BPKB, Faktur Asli\n\nSpesifikasi Daihatsu Xenia 1.3 R Deluxe Automatic (selengkapnya bisa di cari di google):\n1. Mesin 1300 CC 4 silinder, irit dan tangguh\n2. EM/Electric Mirror, pengaturan spion elektronis\n3. AC double blower\n4. Rear spoiler\n5. Mirror turning light/lampu sen di spion kanan dan kiri\n\nDijual sesuai harga pasaran CASH aja gak pakai mahal...\n\nMobil bisa dilihat di basement TSO dengan janji\n\nAtau setiap hari di Kampus UIN Syarif Hidayatullah, Fakultas Kedokteran, Cireundeu, Ciputat, Kertamukti, Jl.Pisangan Raya\n\nSilahkan janjian dulu via WhatsApp dan telpon 0811-92-33-66', 'READY STOCK', 1495705459000, 1495705459000),
(77, 'Ford Everest diesel TDCI A/T 2010', 39, '19', '20160923_122904.jpg', '189000000.00', 1, 0, 'Jual Ford Everest 2010\n\nKelebihan mobil saya sbb:\n+ warna putih mulus (warna favourite dan paling banyak yang nyari utk SUV)\n+ pajak masih panjang sampai bulan Sept tahun 2017\n+ STNK, BPKB, Faktur asli semua\n+ ex Dokter\n+ Ban besar 265/70/R16 masih tebal semua, sudah di balance dan spooring\n+ Oli mesin baru diganti, gak perlu ganti oli lagi sampai 5000km ke depan\n+ Km sedikit baru 141rb-an artinya pemakaian normal hanya 60km per hari (55km x 365 hari x 7 tahun = 140.524km)\n+ buku service, buku manual lengkap\n+ kunci serep lengkap\n+ toolkit dan dongkrak asli lengkap\n+ Head unit asli Ford, ada iPod/iPhone/iPad connector, kamera mundur, bisa Radio, CD, VCD\n+ sudah ada 2 unit monitor asli Ford di Headrest, canggih, pas mundur head unit akan otomatis menampilkan kondisi sekitar, tapi monitor yg di headrest tetap memutar film dari VCD\n+ insya Allah mobil dalam kondisi siap pakai dalam dan luar kota\n\nKelebihan Ford Everest 2010\n1. Lebih bertenaga dibanding Fortuner 2010\n2. Lebih lega dibanding Pajero 2010\n3. Lebih nyaman dibanding Innova 2010\n4. Lebih gagah dibanding Captiva 2010\n5. Lebih masuk akal harganya dibanding mobil sejenis (Fortuner 2010 dan Pajero 2010 Rp250jtan, Everest 2010 dibawah Rp200jt)\n\nSpecifikasi singkat Ford Everest 2010:\n1. Mesin DURATORQ 2.5L Diesel, 4-Cyl, DOHC, Direct Injection Commonrail Turbo with Intercooler, sangat bertenaga namun tetap irit dan tidak masalah menggunakan bahan bakar solar apa saja\n2. System rem sudah menggunakan LSPV (Load Sensing Proportioning Valve) menghasilkan rem yg tetap pakem baik pada saat muatan sedikit maupun penuh\n\nPerbandingan SUV Keluarga (otomotifnetdotcom):\n\nKemajuan teknologi, terutama dapur pacu diesel bisa dirasakan sempurna pada semua SUV (Everest, Fortuner, Pajero, Captiva). Diesel yg dulu dikenal loyo, kini berlimpah tenaga dan torsi. Apalagi semua sudah menggunakan turbo dgn variable geometry, kecuali Grand New Fortuner.\n\nNamun keiritan bahan bakar ternyata dimenangkan oleh Ford Everest. SUV ini sekaligus memberikan kelegaan kabin dan kenyamanan karena memang dimensinya paling besar dan bantingan suspensi empuk. Selain itu banderol Ford Everest adalah yg paling terjangkau. Jadi buat Anda lelaki sejati yg cinta keluarga dan mengutamakan value for money maka Ford Everest adalah pilihan yg paling pas.\n\nMobil bisa dilihat dengan janji via telp atau WhatsApp 0811-923-366\n\nSenin-Jumat jam 9-16 di\nTelkomsel Smart Office, Jl.Gatsu 52 Jaksel\n\nSabtu-Ahad di Kampus UIN Syarif Hidayatullah, Ciputat, Fakultas Kedokteran', 'READY STOCK', 1495705459000, 1495705459000),
(78, 'Motor listrik Mr.Jackie', 39, '19', '20161002_100604.jpg', '5500000.00', 1, 0, 'Motor listrik merek Mr.Jackie\nCocok buat ke Masjid dan jalan-jalan di dalam komplek\nDaya tahan batere sekitar 2 jam non stop', 'READY STOCK', 1495705459000, 1495705459000),
(79, 'Kurma Ajwa Al-Madina Al-Mubaraka ( 500gr)', 25, '12', '10407786_af6c1712-1a7d-4285-a8ca-d19b41284434-1.jpg', '200000.00', 5, 0, '<p>Kurma ajwa biasa dikenal sebagai Kurma Nabi. Produk handcarry langsung dari Saudi Arabia dengan kualitas premium dan ukuran buah sedang.</p>', 'OUT OF STOCK', 1495863900032, 1495863900032),
(80, 'Alas Sajadah Microsatin', 41, '14', 'pjimage.jpg', '50000.00', 0, 0, '<p>Alas Sajadah Microsatin :)</p>\n<p>&nbsp;</p>\n<p>Alas Sajadah yang cocok digunakan pada saat sholat Ied nanti. Terbuat dari bahan yang sangat lentur, warna karakter bervariasi tidak mudah luntur, dapat dicuci atau dilap dengan kanebo dan tidak mudah mengelupas walaupun sering di lipat.</p>\n<p>&nbsp;</p>\n<p>Bahan : Microsatin</p>\n<p>Ukuran : 120 x 60 cm</p>\n<p>&nbsp;</p>\n<p>Kelebihan dari Alas Sajadah ini :</p>\n<p>&nbsp;</p>\n<p>- Praktis : mudah dibawa, dengan lipatan kecil sedemikian rupa tentunya menjadikan mudah dibawa berpergian.</p>\n<p>- Hemat tempat : mudah disimpan karena tidak memerlukan tempat/ruang yang besar untuk penyimpanannya.</p>\n<p>- Hemat biaya : jika dibawa ke tempat sholat ied di lapangan/jalan atau wisata/piknik tidak perlu lagi sewa tikar.</p>\n<p>- Mudah dibersihkan : alas ini berbahan micro yang bersifat tidak tembus air (waterproof) sehingga mudah dilap/dibersihkan.</p>\n<p>- Beragam motif &amp; warna yang dapat dipilih sesuai dengan kesukaan.</p>\n<p>&nbsp;</p>\n<p>Stok Terbatas...jadi buruan pesan sebelum kehabisan :)</p>\n<p>&nbsp;</p>\n<p>Terima kasih :)</p>\n<p>&nbsp;</p>', 'READY STOCK', 1496272736550, 1496272736550),
(81, 'Sarung Wadimor Type Horison', 43, '15', 'IMG-20170522-WA0017_35ndjruk46o0o.jpg', '70000.00', 3, 0, '<p>Sarung Wadimor Asli Buatan Pabrik.. Barang Bagus Cocok Untuk Sholat, Santai dirumah juga untuk gaya..</p>\n<p>silahkan diborong.. yang borong saya do\'ain bersama-sama saya masuk Syurga.. Aamiin</p>', 'READY STOCK', 1495871643706, 1495871643706),
(82, 'Sarung Wadimor Type 3Dara', 43, '15', 'IMG-20170522-WA0016.jpg', '70000.00', 3, 0, '<p>Variasi Warna.. Pesan 1 kodi lebih murah...</p>\n<p>monggo diborong..</p>\n<p>yang borong saya do\'ain bareng2 masuk syurga.. Aamiin.</p>\n<p>&nbsp;</p>\n<p>.</p>', 'READY STOCK', 1495872082406, 1495872082406),
(83, 'Sarung Wadimor Type Horison2', 43, '15', 'IMG-20170522-WA0014.jpeg', '70000.00', 3, 0, '<p>Variasi Warna.. Pesan 1 kodi lebih murah...</p>\n<p>monggo diborong..</p>\n<p>yang borong saya do\'ain bareng2 masuk syurga.. Aamiin.</p>', 'READY STOCK', 1495872082406, 1495872082406),
(84, 'Sarung Wadimor Type Darussalam', 43, '15', 'IMG-20170522-WA0002.jpeg', '70000.00', 3, 0, '<p>Variasi Warna.. Pesan 1 kodi lebih murah...</p>\n<p>monggo diborong..</p>\n<p>yang borong saya do\'ain bareng2 masuk syurga.. Aamiin.</p>', 'READY STOCK', 1495872082406, 1495872082406),
(85, 'Baju Koko Al Mi\'a', 43, '15', 'quickgrid_201751721152527.jpg', '275000.00', 6, 0, '<p>Koko Al - Mi\'a size S - XXL hanya 275.000 (harga bandrol 369.000).. monggo diborong pak????</p>\n<p>Yang Borong Saya Do\'ain bareng2 saya disegerakan Allah berangkat Umroh.. Aamiin</p>\n<p>&nbsp;</p>', 'READY STOCK', 1495872082406, 1495872082406),
(86, 'Sarung Murah', 43, '15', 'IMG-20170526-WA0000.jpg', '35000.00', 12, 0, '<p>Sarung Murah Berkualitas Cocok utk berbagi dibulan Nan Fitri..</p>\n<p>&nbsp;</p>', 'READY STOCK', 1495872082406, 1495872082406),
(87, 'Koko Al Mi\'a Putih', 43, '15', '20170517_205625.jpg', '275000.00', 3, 0, '<p>Tersedia uk. S - XXL</p>', 'READY STOCK', 1495886373813, 1495886373813),
(88, 'Sarung Anak Cap Gajah Suci', 43, '15', 'IMG-20170527-WA0009.jpg', '40000.00', 12, 0, '<p>Cocok utk anak TK - SD</p>\n<p>&nbsp;</p>', 'READY STOCK', 1495886373813, 1495886373813),
(89, 'Emping Melinjo Khas Jogja', 43, '13', 'IMG-20170527-WA0008.jpg', '22500.00', 20, 0, '<p>Emping mlinjo khas jogja, sdh berbumbu..tinggal digoreng, makriuuuk bingit..only 22500/ 250gr. Teman santap sahur n berbuka, bisa dimix dg sup2an, gulai, opor, lodeh, dicolek sambel kecap, dll. Pesen yuks bro..sist..????????</p>', 'READY STOCK', 1495886373813, 1495886373813),
(90, 'Belajar Bareng - Ilmu Shorof', 46, '20', 'IMG_20170528_043135_35niezytkds04.jpg', '0.00', 0, 0, '<p>Assalamu \'alaykum Wr. Wb.</p>\n<p>&nbsp;</p>\n<p>al-Qur\'an adalah sumber Hukum dan Petunjuk Hidup bagi Umat Islam, namun demikian banyak sekali umat Islam di Indonesia lebih memahami Bahasa Inggris dibandingkan Bahasa al-Qur\'an.</p>\n<p>Akibatnya, ilmu atau pemahaman yang disebarkan dengan bahasa Inggris lebih banyak diserap oleh umat Islam dibandingkan ayat-ayat al-Qur\'an.</p>\n<p>Mari kita mulai berkenalan dgn al-Qur\'an, setidaknya memahami Bentuk-bentuk kata yang ada di al-Qur\'an yang dikenal dengan ilmu Shorof.</p>\n<p>&nbsp;</p>\n<p>Wassalamu\'alaykum Wr. Wb.</p>', 'READY STOCK', 1495921198334, 1495921198334),
(91, 'Sarung Wadimor Donggala', 43, '15', 'IMG-20170528-WA0001.jpeg', '70000.00', 10, 0, '', 'READY STOCK', 1495952822437, 1495952822437),
(92, 'Kacang Kudus', 24, '13', 'IMG_20170528_171416_bd6kuipo0i5f.jpg', '17500.00', 20, 0, '<p>Kacang Kudus aseli didatangkan dari Kota Kudus.</p>\n<p>Dengan rasa bawang gurih sangat cocok untuk camilan dan sajian bagi Anda...</p>\n<p>Harga Rp.17500 @250gr</p>', 'READY STOCK', 1496037005830, 1496037005830),
(93, 'Madu Malika 500gr', 20, '12', 'Madu_Malika_500gr.JPG', '75000.00', 10, 0, '<p>Madu Malika : Madu Bunga Jintan Hitam (Black Seed Flower Honey) 500gr</p>', 'READY STOCK', 1496132393932, 1496132393932),
(94, 'Tas Decoupage Goni Holland', 37, '21', 'IMG-20170524-WA0013.jpg', '375000.00', 1, 0, '', 'READY STOCK', 1496804531873, 1496804531873),
(95, 'Tas Decoupage Goni Kampung Halaman', 37, '21', 'IMG-20170524-WA0014_bd86tl3gaky1.jpg', '375000.00', 1, 0, '<p>Tas Unik hasil kerajinan tangan</p>', 'READY STOCK', 1496804531873, 1496804531873),
(96, 'Tikar Lipat Kekinian', 41, '14', 'LongChamp1.jpg', '125000.00', 0, 0, '<p>Suka piknik? Suka main ke pantai? Malas bawa tikar yang besar? Atau capek keluar uang untuk terus-terusan sewa tikar di tempat wisata?</p>\n<p>Gak usah kuatir lagi. Sekarang ada tikar lipat yang praktis, hemat tempat, anti air dan nyaman dibawa dengan motif trendy.</p>\n<p>&nbsp;</p>\n<p>Bahan: Microsatin atau Long Champ</p>\n<p>Ukuran: 200 cm x 150 cm</p>\n<p>Ukuran setelah dilipat: 25 x 15 cm</p>\n<p>Motif Ready bisa dilihat di : http://bit.ly/KatalogTikarLipat</p>\n<p>&nbsp;</p>\n<p>Pesan sekarang, stok terbatas!</p>\n<p>SMS / Whatsapp: 0813-9538-9062</p>\n<p>Fanspage FB : @TikarLipatKekinian</p>', 'READY STOCK', 1496673764418, 1496673764418),
(97, 'Tas bantal decoupage', 37, '21', 'IMG-20170608-WA0013.jpg', '200000.00', 1, 0, '', 'READY STOCK', 1496916755265, 1496916755265),
(98, 'Tas jinjing decoupage 20 x 27 (ukuran S)', 37, '21', 'IMG-20170608-WA0012.jpg', '0.00', 1, 0, '', 'READY STOCK', 1496916755265, 1496916755265),
(99, '', 66, '', '', '0.00', 0, 0, '', 'READY STOCK', 1497334659848, 1497334659848),
(100, 'Egg Chicken Roll', 69, '13', '1-eggroll.jpg', '32000.00', 100, 1, '<p>Sekarang gak perlu bingung atau repot siapkan menu bekal sekolah buat si kecil atau sarapan pagi keluarga, Egg Chicken dari Kraukk Frozen Food ini dibuat khusus dari daging ayam segar yang dicincang, dibumbui, dan dibungkus dengan kulit berbahan telur serta dalam balutan tepung roti.</p>\n<p>&nbsp;</p>\n<p>Rasanya yang lezat dan renyah hingga akhir, merupakan panganan yang cukup digemari di kalangan anak-anak dan masyarakat di semua lapisan, terutama bagi Anda penikmat hidangan masakan Jepang ala bento.</p>\n<p>&nbsp;</p>\n<p>Sensasi daging ayam cincang segar berkualitas yang diolah bersama bumbu rempah, dan dibalut dengan kulit telur sehingga menghasilkan lauk masakan jepang yang disuka seluruh kalangan, terutama penggemar lauk ala bento.</p>\n<p>&nbsp;</p>\n<p>Rasanya yang lezat tidak kalah dari menu masakan jepang di restoran bento ternama di Indonesia. Sudah gak sabar menyantap masakan jepang di rumah anda ?&nbsp;</p>\n<p>&nbsp;</p>\n<p>Yuuk pesan sekarang juga !!</p>\n<p>&nbsp;</p>\n<p>Berat Per Pack : 475 Gr</p>\n<p>Isi : 18 Pcs</p>\n<p>&nbsp;</p>\n<p>Bukan HANYA menunya yang siap goreng dan praktis saja, Egg Chicken Roll ini juga punya kelebihan sebagai berikut:</p>\n<p>- Tanpa Bahan Pengawet</p>\n<p>- Kualitas sama dengan Resto Jepang ala Bento ternama</p>\n<p>- Baik untuk pertumbuhan (mengandung sayuran atau Seafood)</p>\n<p>- Bahan baku Pilihan Kualitas No#1</p>\n<p>&nbsp;</p>\n<p>Dengan adanya menu dari KRAUKK, kini menghidangkan SARAPAN atau BEKAL makanan SEHAT &amp; NIKMAT ala Restoran untuk putra/i kita cukup dari rumah saja. Tinggal keluarkan varian frozen food KRAUKK, lalu goreng dalam 5 menit, menu untuk BEKAL dan sarapanpun siap.</p>\n<p>&nbsp;</p>\n<p>Berat Per Pack : 475 Gr</p>\n<p>Isi : 18</p>', 'READY STOCK', 1497335158562, 1497335158562),
(101, 'Shrimp Roll', 69, '13', '1-shrimp-roll.jpg', '29000.00', 100, 1, '<p>Masakan Jepang ala Kraukk ini merupakan cincangan udang segar berkualitas dengan tepung tapioka yang telah diracik bersama bumbu rempah dan dibalut tepung roti yang membuatnya menjadi renyah setelah digoreng dan nikmat hingga gigitan terakhir.</p>\n<p>&nbsp;</p>\n<p>Rasanya yang lezat tidak kalah dari menu masakan jepang di restoran bento ternama di Indonesia. Sudah gak sabar menyantap masakan jepang di rumah anda ?&nbsp;</p>\n<p>&nbsp;</p>\n<p>Yuuk pesan sekarang juga !!</p>\n<p>&nbsp;</p>\n<p>Berat Per Pack : 350 Gr</p>\n<p>Isi : 12 Pcs</p>\n<p>&nbsp;</p>\n<p>Kraukk menyediakan Menu Bekal Sekolah Frozen Food yang praktis dan lezat tersaji dalam hitungan menit, dengan varian yang lengkap berupa olahan dari udang, ikan, cumi, kepiting dan bahkan makanan olahan ayam juga dapat Anda pesan langsung .</p>\n<p>&nbsp;</p>\n<p>Bukan HANYA menunya yang siap goreng dan praktis saja, KRAUKK juga punya kelebihan sebagai berikut:</p>\n<p>- Tanpa Bahan Pengawet</p>\n<p>- Kualitas Hotel Berbintang</p>\n<p>- Free MSG (Kecuali Beberapa Produk)</p>\n<p>- Baik untuk pertumbuhan (mengandung sayuran atau Seafood)</p>\n<p>- Bahan baku Pilihan Kualitas No#1</p>\n<p>- Telah bersertifikat halal MUI</p>\n<p>- Telah memiliki izin edar dari BPOM / Depkes</p>\n<p>&nbsp;</p>\n<p>SARAPAN PAGI KINI JADI LEBIH MUDAH</p>\n<p>Dengan adanya KRAUKK, kini menghidangkan SARAPAN atau BEKAL makanan SEHAT &amp; NIKMAT ala Restoran untuk putra/i kita cukup dari rumah saja. Tingal keluarkan varian frozen food KRAUKK, lalu goreng dalam 5 menit, menu untuk BEKAL dan sarapanpun siap.</p>\n<p>Dengan rasa yang DISUKA, dan kandungan sehatnya yang tanpa perasa MSG juga TANPA PENGAWET, membuat anak-anak dan seluruh keluarga tetap sehat, bersemangat ke sekolah dan bekerja.</p>\n<p>&nbsp;</p>\n<p>Berat Per Pack : 350 Gr</p>\n<p>Isi : 12 Pcs</p>', 'READY STOCK', 1497335158562, 1497335158562),
(102, 'Udang Tempura', 69, '13', '1-Udang-Tempura.jpg', '24000.00', 100, 1, '<p>Udang Tempura merupakan hidangan berbahan dasar Udang yang dicelup kedalam adonan berupa tepung terigu dan kuning telur yang diencerkan dengan air bersuhu dingin lalu digoreng dengan minyak gorengyang banyak hingga berwarna kuning muda.</p>\n<p>&nbsp;</p>\n<p>Udang Tempura ini adalah Masakan Jepang populer dan banyak disukai. Balutan adonan tepungnya terasa renyah di bagian luar serasi dengan celupan saus sambal. Nasi hangat membuat sajian ini makin komplit dan mengenyangkan. Cocok dijadikan menu hidangan Anda dan keluarga.</p>\n<p>&nbsp;</p>\n<p>Meskipun Udang Tempura populer sebagai Masakan Jepang, namun sajian gorengan ini praktis dibuat dan rasanya yang renyah dan enak membuatnya banyak disukai termasuk oleh orang Indonesia. Udang Tempura yang diproduksi oleh Kraukk Indonesia ini dibuat dengan tepung khusus berkualitas ekspor yang lazim digunakan oleh para produsen pizza sehingga rasanya bisa renyah dan gurih di bagian luarnya dan lembut pada bagian dalam.</p>\n<p>&nbsp;</p>\n<p>Rasanya yang lezat tidak kalah dari menu masakan jepang di restoran bento ternama di Indonesia. Sudah gak sabar menyantap masakan jepang di rumah anda ?&nbsp;</p>\n<p>&nbsp;</p>\n<p>Yuuk pesan sekarang juga !!</p>\n<p>&nbsp;</p>\n<p>Berat Per Pack : 250 Gr</p>\n<p>Isi : 8 Pcs</p>\n<p>&nbsp;</p>\n<p>Kraukk menyediakan Menu Bekal Sekolah Frozen Food yang praktis dan lezat tersaji dalam hitungan menit, dengan varian yang lengkap berupa olahan dari udang, ikan, cumi, kepiting dan bahkan makanan olahan ayam juga dapat Anda pesan langsung .</p>\n<p>&nbsp;</p>\n<p>Bukan HANYA menunya yang siap goreng dan praktis saja, KRAUKK juga punya kelebihan sebagai berikut:</p>\n<p>- Tanpa Bahan Pengawet</p>\n<p>- Kualitas Hotel Berbintang</p>\n<p>- Free MSG (Kecuali Beberapa Produk)</p>\n<p>- Baik untuk pertumbuhan (mengandung sayuran atau Seafood)</p>\n<p>- Bahan baku Pilihan Kualitas No#1</p>\n<p>- Telah bersertifikat halal MUI</p>\n<p>- Telah memiliki izin edar dari BPOM / Depkes</p>\n<p>&nbsp;</p>\n<p>Berat Per Pack : 250 Gr</p>\n<p>Isi : 8 Pcs</p>', 'READY STOCK', 1497335158562, 1497335158562),
(103, 'Moneybag', 69, '13', '1-moneybag.jpg', '19500.00', 100, 1, '<p>Makanan sehat yang berbentuk kantung untuk menyimpan uang ini lebih dikenal dengan sebutan Money Bag. Frozen food berbahan baku ikan, udang, dansayuran yang dihasilkan oleh Kraukk Indonesia ini bersifat sehat dan bebas dari bahan kimia apapun, dibungkus dalam kulit lumpia (kulit pangsit) laludibentuk menyerupai kantung yang diikat dengan tali dari daun kucai yang aman dikonsumsi.</p>\n<p>&nbsp;</p>\n<p>Moneybag termasuk ke dalam kategori dim sum goreng. Kulitnya yang crispy dengan cacahan udang, ikan, dan sayuran yang lekat dengan adonan tepung di dalamnya sangat cocok dijadikan salah satu menu pelengkap hidangan Anda dan keluarga. Menjadi salah satu produk terlaris Kraukk karena bentuknya yang unik dengan rasa yang lezat.</p>\n<p>&nbsp;</p>\n<p>Makanan sehat dalam bentuk moneybag dari KRAUKK ini dibuat tanpa menggunakan bahan pengawet, pewarna, ataupun MSG, tersedia dalam bentuk makanan beku atau frozen food .</p>\n<p>&nbsp;</p>\n<p>Berat Per Pack : 250 Gr</p>\n<p>Isi : 15 Pcs</p>\n<p>&nbsp;</p>\n<p>Kraukk menyediakan Menu Bekal Sekolah Frozen Food yang praktis dan lezat tersaji dalam hitungan menit, dengan varian yang lengkap berupa olahan dari udang, ikan, cumi, kepiting dan bahkan makanan olahan ayam juga dapat Anda pesan langsung .</p>\n<p>&nbsp;</p>\n<p>Bukan HANYA menunya yang siap goreng dan praktis saja, KRAUKK juga punya kelebihan sebagai berikut:</p>\n<p>- Tanpa Bahan Pengawet</p>\n<p>- Kualitas Hotel Berbintang</p>\n<p>- Free MSG (Kecuali Beberapa Produk)</p>\n<p>- Baik untuk pertumbuhan (mengandung sayuran atau Seafood)</p>\n<p>- Bahan baku Pilihan Kualitas No#1</p>\n<p>- Telah bersertifikat halal MUI</p>\n<p>- Telah memiliki izin edar dari BPOM / Depkes</p>\n<p>&nbsp;</p>\n<p>Berat Per Pack : 250 Gr</p>\n<p>Isi : 15 Pcs</p>', 'READY STOCK', 1497335723740, 1497335723740),
(104, '2013 Chevrolet Spin LTZ', 67, '19', '20170609_063227_35rdmxi46uckg.jpg', '128000000.00', 1, 0, '<p>Bismillah..</p>\n<p>Assalamualaikum, Dijual Chevrolet Spin LTZ 1500cc matic tahun 2013</p>\n<p>- Pemakaian pribadi</p>\n<p>- Mobil sering untuk istri pergi pengajian/keliling komplek</p>\n<p>- Tidak bau rokok</p>\n<p>- Service record selalu di bengkel resmi (barusan ganti ban depan dan filter ac)</p>\n<p>- Surat - surat lengkap</p>\n<p>- Tarikan mesin masih enak</p>\n<p>- Ban serep blm pernah dipake</p>\n<p>Fitur tambahan :</p>\n<p>- Head unit sudah diganti LCD touch screen (Navigasi,DVD, dsb)</p>\n<p>- Camera mundur</p>\n<p>- Alat informasi jarak toleransi mundur</p>\n<p>- Sarung jok</p>\n<p>&nbsp;-Harga masih nego (tapi jangan afgan)</p>\n<p>&nbsp;</p>\n<p>- Informasi lebih lanjut 08118705533</p>\n<p>Trima kasih :) Wassalamualaikum.</p>', 'READY STOCK', 1497335058738, 1497335058738),
(105, 'More Tea - Rasa Original', 73, '13', '', '0.00', 0, 0, '<p>More Tea - Homemade Thai Tea</p>\n<p>Rasa Original</p>', 'OUT OF STOCK', 1497335542102, 1497335542102),
(106, 'More Tea - Rasa Green Tea', 73, '13', '', '0.00', 0, 0, '<p>More Tea - Homemade Thai Tea</p>\n<p>Rasa Green Tea</p>', 'OUT OF STOCK', 1497335542102, 1497335542102),
(107, 'Rosida Jas Hujan - Cowok', 76, '14', '', '170000.00', 3, 0, '<p>Quality Export, Berbahan tebal, tersedia warna Merah Hati, Biru Dan Hitam dgn Ukuran M-L-XL</p>', 'READY STOCK', 1497354943712, 1497453334385),
(108, 'Bakso Frozen Halus', 76, '13,14', '', '60000.00', 5, 0, '<p>Pakkar Bakso diproduksi dgn higienis, tanpa bahan pengawat Dan tanpa MSG. Tahan sampai 5 bulan dalam freezer Dan 15 jam di luar kulkas.</p>', 'READY STOCK', 1497354943712, 1497453313593),
(109, 'Camry V 2010', 79, '19', 'IMG-20170609-WA0005.jpg', '220000000.00', 1, 0, '<p>Assalamualaikum Wr. Wb</p>\n<p>Dijual santai, camry v 2010, barang simpanan</p>\n<p>kondisi sangat baik km rendah 38.000 on going</p>\n<p>ban serep belum turun, sangat jarang jarak jauh</p>\n<p>jok leather electric semua original dan berjalan sangat baik&nbsp;</p>\n<p>buat Sahabat yg mencari mobil sangat layak pakai monggo dilihat , tapi janjian dulu ya soalnya mobil kerudungan terus di rumah ????.</p>\n<p>japri dulu call / &nbsp;wa ke 08121139007 hatur nuhun</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>', 'READY STOCK', 1497409045997, 1497453356890),
(110, 'Fossil FIONA black', 81, '15', 'IMG-20170614-WA0155_35rp6jp2x5c0c.jpg', '3200000.00', 1, 0, '<p>Fossil authentic ..</p>\n<p>Fiona black</p>\n<p>full leather</p>\n<p>Size 30 cm</p>\n<p>Utk lbh detail silahkan &nbsp;wa 08117700122 ( no call )</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>', 'READY STOCK', 1497454387957, 1497454387957);

-- --------------------------------------------------------

--
-- Table structure for table `product_category`
--

CREATE TABLE `product_category` (
  `product_id` bigint(20) NOT NULL,
  `category_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_category`
--

INSERT INTO `product_category` (`product_id`, `category_id`) VALUES
(1, 11),
(6, 11),
(7, 11),
(9, 12),
(10, 12),
(10, 13),
(11, 12),
(12, 12),
(14, 12),
(15, 12),
(17, 12),
(18, 12),
(20, 12),
(21, 13),
(22, 15),
(23, 15),
(24, 15),
(25, 14),
(26, 16),
(27, 16),
(28, 18),
(29, 17),
(30, 17),
(5, 11),
(16, 12),
(19, 12),
(3, 11),
(8, 11),
(47, 11),
(4, 11),
(49, 12),
(51, 11),
(52, 11),
(53, 17),
(54, 15),
(56, 17),
(58, 13),
(59, 15),
(60, 15),
(61, 13),
(62, 13),
(63, 13),
(64, 13),
(66, 13),
(67, 13),
(70, 13),
(71, 13),
(73, 17),
(75, 17),
(76, 19),
(77, 19),
(78, 19),
(79, 12),
(13, 12),
(80, 14),
(81, 15),
(82, 15),
(83, 15),
(84, 15),
(85, 15),
(86, 15),
(2, 11),
(87, 15),
(88, 15),
(89, 13),
(90, 20),
(91, 15),
(92, 13),
(93, 12),
(94, 21),
(95, 21),
(96, 14),
(97, 21),
(98, 21),
(100, 13),
(101, 13),
(102, 13),
(103, 13),
(104, 19),
(105, 13),
(106, 13),
(110, 15),
(108, 13),
(108, 14),
(107, 14),
(109, 19);

-- --------------------------------------------------------

--
-- Table structure for table `product_image`
--

CREATE TABLE `product_image` (
  `product_id` bigint(20) NOT NULL,
  `name` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_image`
--

INSERT INTO `product_image` (`product_id`, `name`) VALUES
(3, 'Nasi Ayam Komplit (MTT)_01495330493177.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `product_order`
--

CREATE TABLE `product_order` (
  `id` bigint(20) NOT NULL,
  `code` varchar(20) NOT NULL,
  `buyer` varchar(50) NOT NULL,
  `address` varchar(300) NOT NULL,
  `email` varchar(50) NOT NULL,
  `shipping` varchar(20) NOT NULL,
  `date_ship` bigint(20) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `comment` text NOT NULL,
  `status` varchar(50) NOT NULL,
  `total_fees` decimal(12,2) NOT NULL,
  `tax` decimal(12,2) NOT NULL,
  `created_at` bigint(20) NOT NULL,
  `last_update` bigint(20) NOT NULL,
  `date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_order`
--

INSERT INTO `product_order` (`id`, `code`, `buyer`, `address`, `email`, `shipping`, `date_ship`, `phone`, `comment`, `status`, `total_fees`, `tax`, `created_at`, `last_update`, `date_time`) VALUES
(1, 'LO76366JW', 'Qomar', 'Lantai 11', 'qomarullah.mail@gmail.com', 'COD (Cash On Deliver', 1495299802585, '08118003585', 'test', 'WAITING', '26000.00', '0.00', 1495299808368, 1495299808368, '0000-00-00 00:00:00'),
(2, 'JX45983AP', 'Qomar', 'Lantai 11', 'qomarullah.mail@gmail.com', 'COD (Cash On Deliver', 1495336022805, '08118003585', 'test', 'WAITING', '128000.00', '0.00', 1495336030207, 1495336030207, '0000-00-00 00:00:00'),
(3, 'IG23519VG', 'Qomar', 'Lantai 11', 'qomarullah.mail@gmail.com', 'Ambil di Sekre MTT l', 1495338610695, '08118003585', 'cek', 'WAITING', '26080000.00', '0.00', 1495338620373, 1495338620373, '0000-00-00 00:00:00'),
(4, 'JS84974GQ', 'Fitri', 'kirim lt 16', 'test@gmail.com', 'TOD (T-Cash On Deliv', 1495340279951, '0811111111', 'test note', 'WAITING', '625437000.00', '0.00', 1495340289400, 1495340289400, '0000-00-00 00:00:00'),
(5, 'QD70575MP', 'Fitri', 'kirim lt 16', 'test@gmail.com', 'COD (Cash On Deliver', 1495340486070, '0811111111', 'kasih rawit', 'WAITING', '10000.00', '0.00', 1495340502454, 1495340502454, '0000-00-00 00:00:00'),
(6, 'DZ18160ON', 'Fitri', 'kirim lt 16', 'test@gmail.com', 'TOD (T-Cash On Deliv', 1495340981217, '0811111111', 'test', 'WAITING', '1000.00', '0.00', 1495340988873, 1495340988873, '0000-00-00 00:00:00'),
(7, 'MY58617DE', 'y', 'g', 'mttpreneurship@gmail.com', 'TOD (T-Cash On Deliv', 1495521267448, '081285688212', 'snsndnd', 'WAITING', '44000.00', '0.00', 1495348477699, 1495348477699, '0000-00-00 00:00:00'),
(8, 'CJ41423TV', 'y', 'g', 'mttpreneurship@gmail.com', 'TOD (T-Cash On Deliv', 1495521337051, '081285688212', 'bjj', 'WAITING', '44000.00', '0.00', 1495348544981, 1495348544981, '0000-00-00 00:00:00'),
(12, 'AT80640MG', 'y', 'g', 'mttpreneurship@gmail.com', 'TOD (T-Cash On Deliv', 1495437091068, '081285688212', 'tt', 'WAITING', '15000.00', '0.00', 1495350695348, 1495350695348, '0000-00-00 00:00:00'),
(13, 'AV21873AE', 'qomar', 'Lantai 16 TSO, breakout', 'qomarullah.mail@gmail.com', 'TOD (T-Cash On Deliv', 1495411914450, '08118003585', 'minta kirim siangan jam 13.00', 'WAITING', '35000.00', '0.00', 1495411934386, 1495411934386, '0000-00-00 00:00:00'),
(14, 'LZ07569TU', 'kardi', 'tso lt.16', 'kardi@telkomsel.co.id', 'COD (Cash On Deliver', 1495418380612, '0811100677', 'jam 10:00 ya...', 'WAITING', '15000.00', '0.00', 1495418412221, 1495418412221, '0000-00-00 00:00:00'),
(15, 'QD33851LC', 'Yuwono Eko', 'TSO lantai 16', 'yuwono_eko@telkomsel.co.id', 'COD (Cash On Deliver', 1495424324292, '0811310021', 'jam 11:00', 'PROCESSED', '25000.00', '0.00', 1495424352950, 1495424352950, '0000-00-00 00:00:00'),
(16, 'NF60129NC', 'vanul', 'tso lt.9', 'mttpreneurship@gmail.com', 'TOD (T-Cash On Deliv', 1495554057001, '081285688212', 'tes', 'WAITING', '150000.00', '0.00', 1495467669556, 1495467669556, '0000-00-00 00:00:00'),
(17, 'XM46774FK', 'Novi Faldian', 'lantai 8', 'faldian2002@gmail.com', 'COD (Cash On Deliver', 1495578420560, '0811830500', 'jam 10 pagi', 'WAITING', '10000.00', '0.00', 1495492045373, 1495492045373, '0000-00-00 00:00:00'),
(18, 'JO87907DT', 'furqon ahkamy', 'TSO Lt. 12', 'furqon.ahkamy@gmail.com', 'COD (Cash On Deliver', 1495578922484, '0811646446', 'Mohon info stock rasa yg ready, syukron', 'PROCESSED', '15000.00', '0.00', 1495492562129, 1495492562129, '0000-00-00 00:00:00'),
(19, 'GO90946AJ', 'qomar', 'lt 16', 'qomarullah.mail@gmail.com', 'TOD (T-Cash On Deliv', 1495496623808, '08118003585', 'lagi test, mohon ignore', 'WAITING', '15000.00', '0.00', 1495496642360, 1495496642360, '0000-00-00 00:00:00'),
(20, 'NY58332MP', 'qomar', 'lt 16', 'qomarullah.mail@gmail.com', 'TOD (T-Cash On Deliv', 1495496623808, '08118003585', 'lagi test, mohon ignore', 'WAITING', '15000.00', '0.00', 1495496653121, 1495496653121, '0000-00-00 00:00:00'),
(21, 'NR93340CH', 'qomar', 'lt 16', 'qomarullah.mail@gmail.com', 'TOD (T-Cash On Deliv', 1495496623808, '08118003585', 'lagi test, mohon ignore', 'WAITING', '15000.00', '0.00', 1495496804028, 1495496804028, '0000-00-00 00:00:00'),
(22, 'FZ71137MD', 'qomar', 'lt 16', 'qomarullah.mail@gmail.com', 'TOD (T-Cash On Deliv', 1495499941837, '08118003585', 'testing', 'WAITING', '16000.00', '0.00', 1495499950733, 1495499950733, '0000-00-00 00:00:00'),
(23, 'NJ79465CW', 'qomar', 'lt 16', 'qomarullah.mail@gmail.com', 'TOD (T-Cash On Deliv', 1495501020416, '08118003585', 'test 2 kali telegram', 'WAITING', '16000.00', '0.00', 1495501044204, 1495501044204, '0000-00-00 00:00:00'),
(24, 'WH65998UN', 'kardi', 'tso lt.16', 'kardi@telkomsel.co.id', 'TOD (T-Cash On Deliv', 1495593015840, '0811100677', 'kirim ke ruang Maroko lt.16. Tolong dipotong-potong 6 mendoan & 6 bakwan', 'WAITING', '36000.00', '0.00', 1495593261763, 1495593261763, '0000-00-00 00:00:00'),
(25, 'HL43131VA', 'KY', '23', 'yanto_santoso@telkomsel.co.id', 'TOD (T-Cash On Deliv', 1495599976183, '0811293333', 'jam 3 sore aja ya...', 'WAITING', '30000.00', '0.00', 1495600025762, 1495600025762, '0000-00-00 00:00:00'),
(26, 'AZ48548YI', 'jowvy', 'lt.23', 'jowvy_kumala@telkomsel.co.id', 'TOD (T-Cash On Deliv', 1495600173104, '0811412344', 'jam 16.00 wib', 'PROCESSED', '65000.00', '0.00', 1495600207335, 1495600207335, '0000-00-00 00:00:00'),
(27, 'UJ69348JF', 'qomar', 'lt 16', 'qomarullah.mail@gmail.com', 'TOD (T-Cash On Deliv', 1495706200711, '08118003585', 'test', 'WAITING', '26000.00', '0.00', 1495706205404, 1495706205404, '0000-00-00 00:00:00'),
(28, 'PO26079ID', 'qomar', 'lt 16', 'qomarullah.mail@gmail.com', 'TOD (T-Cash On Deliv', 1495706200711, '08118003585', 'test', 'WAITING', '26000.00', '0.00', 1495706310816, 1495706310816, '0000-00-00 00:00:00'),
(29, 'HU80053JS', 'qomar', 'lt 16', 'qomarullah.mail@gmail.com', 'COD (Cash On Deliver', 1495706430264, '08118003585', 'test lagi', 'WAITING', '26000.00', '0.00', 1495706438353, 1495706438353, '0000-00-00 00:00:00'),
(30, 'KI85471HI', 'qomar', 'lt 16', 'qomarullah.mail@gmail.com', 'COD (Cash On Deliver', 1495706687774, '08118003585', 'test', 'WAITING', '16000.00', '0.00', 1495706693334, 1495706693334, '0000-00-00 00:00:00'),
(31, 'IO10105CZ', 'qomar', 'lt 16', 'qomarullah.mail@gmail.com', 'COD (Cash On Deliver', 1495707663266, '08118003585', 'test', 'WAITING', '16000.00', '0.00', 1495707670121, 1495707670121, '0000-00-00 00:00:00'),
(32, 'HK02011JP', 'qomar', 'lt 16', 'qomarullah.mail@gmail.com', 'COD (Cash On Deliver', 1495707663266, '08118003585', 'test', 'WAITING', '16000.00', '0.00', 1495707704793, 1495707704793, '0000-00-00 00:00:00'),
(33, 'XS70693EJ', 'qomar', 'lt 16', 'qomarullah.mail@gmail.com', 'COD (Cash On Deliver', 1495707748685, '08118003585', 'test', 'WAITING', '16000.00', '0.00', 1495707754927, 1495707754927, '0000-00-00 00:00:00'),
(34, 'LE10263EK', 'qomar', 'lt 16', 'qomarullah.mail@gmail.com', 'COD (Cash On Deliver', 1495707748685, '08118003585', 'test', 'WAITING', '16000.00', '0.00', 1495707760907, 1495707760907, '0000-00-00 00:00:00'),
(35, 'IQ40472YJ', 'qomar', 'lt 16', 'qomarullah.mail@gmail.com', 'COD (Cash On Deliver', 1495707748685, '08118003585', 'test', 'WAITING', '16000.00', '0.00', 1495707939077, 1495707939077, '0000-00-00 00:00:00'),
(36, 'YK93067MO', 'qomar', 'lt 16', 'qomarullah.mail@gmail.com', 'TOD (T-Cash On Deliv', 1495715911898, '08118003585', 'cuma test', 'WAITING', '35000.00', '0.00', 1495715924028, 1495715924028, '0000-00-00 00:00:00'),
(37, 'QK63922ZB', 'qomar', 'lt 16', 'qomarullah.mail@gmail.com', 'COD (Cash On Deliver', 1495753824154, '08118003585', 'test', 'WAITING', '17000.00', '0.00', 1495753829964, 1495753829964, '0000-00-00 00:00:00'),
(38, 'FJ83266MO', 'qomar', 'lt 16', 'qomarullah.mail@gmail.com', 'COD (Cash On Deliver', 1495753928110, '08118003585', 'test', 'WAITING', '16000.00', '0.00', 1495753933959, 1495753933959, '0000-00-00 00:00:00'),
(39, 'ER58186TH', 'qomar', 'lt 16', 'qomarullah.mail@gmail.com', 'COD (Cash On Deliver', 1495753928110, '08118003585', 'test', 'WAITING', '16000.00', '0.00', 1495754616476, 1495754616476, '0000-00-00 00:00:00'),
(40, 'RQ62155KL', 'qomar', 'lt 16', 'qomarullah.mail@gmail.com', 'COD (Cash On Deliver', 1495754774414, '08118003585', 'test', 'WAITING', '16000.00', '0.00', 1495754781735, 1495754781735, '0000-00-00 00:00:00'),
(41, 'PJ03384EU', 'vanul', 'tso lt.9', 'mttpreneurship@gmail.com', 'Ambil di Sekre MTT l', 1496037814433, '081285688212', 'tes. bisa pilih tgl kirim', 'WAITING', '16000.00', '0.00', 1495865033812, 1495865033812, '0000-00-00 00:00:00'),
(42, 'MZ14528TG', 'qomar', 'lt 16', 'qomarullah.mail@gmail.com', 'COD (Cash On Deliver', 1495945697464, '08118003585', 'testing', 'WAITING', '16000.00', '0.00', 1495945705268, 1495945705268, '0000-00-00 00:00:00'),
(43, 'MM21629AO', 'Agung Handoko', 'TSO Lantai 12', 'agung_handoko@telkomsel.co.id', 'Tunai Saat Penerimaa', 1496221354935, '0811864686', 'sekarang', 'WAITING', '16000.00', '0.00', 1496221367783, 1496221367783, '2017-05-31 09:02:48'),
(44, 'MQ59439UL', 'qomar', '16', 'qomarullah.mail@gmail.com', 'T-Cash Saat Penerima', 1496223695618, '08118003585', 'test', 'WAITING', '16000.00', '0.00', 1496223702570, 1496223702570, '2017-05-31 09:41:42'),
(45, 'XZ23293WV', 'qomar', '16', 'qomarullah.mail@gmail.com', 'T-Cash Saat Penerima', 1496223790579, '08118003585', 'testing nyampe telegram', 'WAITING', '16000.00', '0.00', 1496223801096, 1496223801096, '2017-05-31 09:43:20'),
(46, 'VQ75396WC', 'qomar', '16', 'qomarullah.mail@gmail.com', 'Tunai Saat Penerimaa', 1496223895540, '08118003585', 'test', 'WAITING', '16000.00', '0.00', 1496223905846, 1496223905846, '2017-05-31 09:45:05'),
(47, 'KB15316KC', 'qomar', '16', 'qomarullah.mail@gmail.com', 'Tunai Saat Penerimaa', 1496245282217, '08118003585', 'test', 'WAITING', '16000.00', '0.00', 1496245292572, 1496245292572, '2017-05-31 15:41:32'),
(48, 'CM62286KT', 'qomar', '16', 'qomarullah.mail@gmail.com', 'Tunai Saat Penerimaa', 1496245282217, '08118003585', 'test', 'WAITING', '16000.00', '0.00', 1496245309555, 1496245309555, '2017-05-31 15:41:49'),
(49, 'PI23496ZR', 'qomar', '16', 'qomarullah.mail@gmail.com', 'Tunai Saat Penerimaa', 1496245380086, '08118003585', 'test', 'WAITING', '16000.00', '0.00', 1496245386157, 1496245386157, '2017-05-31 15:43:06'),
(50, 'DN31857GO', 'Hari Suryono', 'TSO LT 12', 'hari_suryono@telkomsel.co.id', 'T-Cash Saat Penerima', 1497334654356, '0811159429', 'ini ada tempat laptopnya gak ya? ada pilihan warnanya?', 'WAITING', '340000.00', '0.00', 1497334703128, 1497334703128, '2017-06-13 06:18:23'),
(51, 'FV61822MP', 'qomar', '16', 'qomarullah.mail@gmail.com', 'Transfer T-Cash 0812', 1497344987059, '08118003585', 'test', 'WAITING', '16000.00', '0.00', 1497344999481, 1497344999481, '2017-06-13 09:09:57');

-- --------------------------------------------------------

--
-- Table structure for table `product_order_detail`
--

CREATE TABLE `product_order_detail` (
  `id` bigint(20) NOT NULL,
  `order_id` bigint(20) NOT NULL,
  `product_id` bigint(20) NOT NULL,
  `product_name` varchar(100) NOT NULL,
  `merchant_id` int(11) NOT NULL,
  `merchant_name` varchar(30) NOT NULL,
  `merchant_phone` varchar(20) NOT NULL,
  `merchant_email` varchar(40) NOT NULL,
  `amount` int(11) NOT NULL,
  `price_item` decimal(12,2) NOT NULL,
  `created_at` bigint(20) NOT NULL,
  `last_update` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product_order_detail`
--

INSERT INTO `product_order_detail` (`id`, `order_id`, `product_id`, `product_name`, `merchant_id`, `merchant_name`, `merchant_phone`, `merchant_email`, `amount`, `price_item`, `created_at`, `last_update`) VALUES
(1, 1, 6, 'Indomie Telor (MTT)', 0, '', '', '', 1, '10000.00', 1495299808369, 1495299808369),
(2, 1, 7, 'Indomie Telor Plus sosis, nugget, keju (MTT)', 0, '', '', '', 1, '15000.00', 1495299808369, 1495299808369),
(3, 1, 8, 'Gorengan Bakwan,Tahu (MTT)', 0, '', '', '', 1, '1000.00', 1495299808369, 1495299808369),
(4, 2, 1, 'Nasi Pecel Madiun (MTT)', 0, '', '', '', 1, '19000.00', 1495336030208, 1495336030208),
(5, 2, 2, 'Nasi Gado Gado (MTT)', 0, '', '', '', 1, '20000.00', 1495336030208, 1495336030208),
(6, 2, 3, 'Nasi Ayam Komplit (MTT)', 0, '', '', '', 1, '26000.00', 1495336030208, 1495336030208),
(7, 2, 7, 'Indomie Telor Plus sosis, nugget, keju (MTT)', 0, '', '', '', 1, '15000.00', 1495336030208, 1495336030208),
(8, 2, 8, 'Gorengan Bakwan,Tahu (MTT)', 0, '', '', '', 1, '1000.00', 1495336030208, 1495336030208),
(9, 2, 5, 'Nasi Sayur (MTT)', 0, '', '', '', 1, '15000.00', 1495336030208, 1495336030208),
(10, 2, 6, 'Indomie Telor (MTT)', 0, '', '', '', 1, '10000.00', 1495336030208, 1495336030208),
(11, 2, 4, 'Nasi Bakar (MTT)', 0, '', '', '', 1, '22000.00', 1495336030208, 1495336030208),
(12, 3, 28, 'Umroh Bersama MTT', 0, '', '', '', 1, '26000000.00', 1495338620374, 1495338620374),
(13, 3, 10, 'Kopi Hijau Diet Kopi Kawi', 0, '', '', '', 1, '65000.00', 1495338620374, 1495338620374),
(14, 3, 21, 'Kripik KEPO', 0, '', '', '', 1, '15000.00', 1495338620374, 1495338620374),
(15, 4, 30, 'Metode Tartila (Praktis Tahsin & Tilawah)', 0, '', '', '', 1, '35000.00', 1495340289401, 1495340289401),
(16, 4, 27, 'Rumah di Bekasi Selatan', 0, '', '', '', 1, '625000000.00', 1495340289401, 1495340289401),
(17, 4, 24, 'Kopiah', 0, '', '', '', 1, '10000.00', 1495340289401, 1495340289401),
(18, 4, 25, 'Tas Ransel Eiger MTT', 0, '', '', '', 1, '340000.00', 1495340289401, 1495340289401),
(19, 4, 7, 'Indomie Telor Plus sosis, nugget, keju (MTT)', 0, '', '', '', 1, '15000.00', 1495340289401, 1495340289401),
(20, 4, 3, 'Nasi Ayam Komplit (MTT)', 0, '', '', '', 1, '26000.00', 1495340289401, 1495340289401),
(21, 4, 6, 'Indomie Telor (MTT)', 0, '', '', '', 1, '10000.00', 1495340289401, 1495340289401),
(22, 4, 8, 'Gorengan Bakwan,Tahu (MTT)', 0, '', '', '', 1, '1000.00', 1495340289401, 1495340289401),
(23, 5, 6, 'Indomie Telor (MTT)', 0, '', '', '', 1, '10000.00', 1495340502454, 1495340502454),
(24, 6, 8, 'Gorengan Bakwan,Tahu (MTT)', 0, '', '', '', 1, '1000.00', 1495340988875, 1495340988875),
(25, 7, 4, 'Nasi Bakar (MTT)', 0, '', '', '', 2, '22000.00', 1495348477701, 1495348477701),
(26, 8, 4, 'Nasi Bakar (MTT)', 0, '', '', '', 2, '22000.00', 1495348544982, 1495348544982),
(27, 12, 8, 'Gorengan Bakwan,Tahu (MTT)', 0, '', '', '', 5, '3000.00', 1495350695350, 1495350695350),
(28, 13, 47, 'Nasi Tengkleng', 0, '', '', '', 1, '35000.00', 1495411934388, 1495411934388),
(29, 14, 8, 'Gorengan Bakwan,Tahu (MTT)', 0, '', '', '', 5, '3000.00', 1495418412221, 1495418412221),
(30, 15, 8, 'Gorengan Bakwan,Tahu (MTT)', 0, '', '', '', 1, '3000.00', 1495424352951, 1495424352951),
(31, 15, 4, 'Nasi Bakar (MTT)', 0, '', '', '', 1, '22000.00', 1495424352951, 1495424352951),
(32, 16, 54, 'Baju Taqwa Riyan', 0, '', '', '', 1, '150000.00', 1495467669556, 1495467669556),
(33, 17, 24, 'Kopiah', 0, '', '', '', 1, '10000.00', 1495492045373, 1495492045373),
(34, 18, 21, 'Kripik KEPO', 0, '', '', '', 1, '15000.00', 1495492562129, 1495492562129),
(35, 21, 21, 'Kripik KEPO', 0, '', '', '', 1, '15000.00', 1495496804028, 1495496804028),
(36, 22, 52, 'Batagor', 0, '', '', '', 1, '16000.00', 1495499950734, 1495499950734),
(37, 23, 52, 'Batagor', 0, '', '', '', 1, '16000.00', 1495501044206, 1495501044206),
(38, 24, 8, 'Gorengan Bakwan,Tahu (MTT)', 0, '', '', '', 12, '3000.00', 1495593261763, 1495593261763),
(39, 25, 8, 'Gorengan Bakwan,Tahu (MTT)', 0, '', '', '', 10, '3000.00', 1495600025762, 1495600025762),
(40, 26, 10, 'Kopi Hijau Diet Kopi Kawi', 0, '', '', '', 1, '65000.00', 1495600207335, 1495600207335),
(41, 28, 3, 'Nasi Ayam Komplit (MTT)', 0, '', '', '', 1, '26000.00', 1495706310816, 1495706310816),
(42, 29, 3, 'Nasi Ayam Komplit (MTT)', 5, '', '', '', 1, '26000.00', 1495706438355, 1495706438355),
(43, 30, 52, 'Batagor', 21, '', '', '', 1, '16000.00', 1495706693336, 1495706693336),
(44, 35, 52, 'Batagor', 21, 'BuburKuningan', '081296363064', 'rudianabinkasmad@gmail.com', 1, '16000.00', 1495707939077, 1495707939077),
(45, 36, 47, 'Nasi Tengkleng', 0, '', '', '', 1, '35000.00', 1495715924030, 1495715924030),
(46, 37, 51, 'Bubur Kuningan', 21, 'BuburKuningan', '081296363064', 'rudianabinkasmad@gmail.com', 1, '17000.00', 1495753829966, 1495753829966),
(47, 38, 52, 'Batagor', 21, 'BuburKuningan', '081296363064', 'rudianabinkasmad@gmail.com', 1, '16000.00', 1495753933960, 1495753933960),
(48, 39, 52, 'Batagor', 21, 'BuburKuningan', '081296363064', 'rudianabinkasmad@gmail.com', 1, '16000.00', 1495754616476, 1495754616476),
(49, 40, 52, 'Batagor', 21, 'BuburKuningan', '081296363064', 'rudianabinkasmad@gmail.com', 1, '16000.00', 1495754781735, 1495754781735),
(50, 41, 52, 'Batagor', 21, 'BuburKuningan', '081296363064', 'rudianabinkasmad@gmail.com', 1, '16000.00', 1495865033812, 1495865033812),
(51, 42, 52, 'Batagor', 21, 'BuburKuningan', '081296363064', 'rudianabinkasmad@gmail.com', 1, '16000.00', 1495945705269, 1495945705269),
(52, 43, 52, 'Batagor', 21, 'BuburKuningan', '081296363064', 'rudianabinkasmad@gmail.com', 1, '16000.00', 1496221367784, 1496221367784),
(53, 44, 52, 'Batagor', 21, 'BuburKuningan', '081296363064', 'rudianabinkasmad@gmail.com', 1, '16000.00', 1496223702571, 1496223702571),
(54, 45, 52, 'Batagor', 21, 'BuburKuningan', '081296363064', 'rudianabinkasmad@gmail.com', 1, '16000.00', 1496223801096, 1496223801096),
(55, 46, 52, 'Batagor', 21, 'BuburKuningan', '081296363064', 'rudianabinkasmad@gmail.com', 1, '16000.00', 1496223905846, 1496223905846),
(56, 47, 52, 'Batagor', 21, 'BuburKuningan', '081296363064', 'rudianabinkasmad@gmail.com', 1, '16000.00', 1496245292573, 1496245292573),
(57, 48, 52, 'Batagor', 21, 'BuburKuningan', '081296363064', 'rudianabinkasmad@gmail.com', 1, '16000.00', 1496245309555, 1496245309555),
(58, 49, 52, 'Batagor', 21, 'BuburKuningan', '081296363064', 'rudianabinkasmad@gmail.com', 1, '16000.00', 1496245386157, 1496245386157),
(59, 50, 25, 'Tas Ransel Eiger MTT', 5, 'admin', '08118003585', 'qomarullah.mail@gmail.comx', 1, '340000.00', 1497334703128, 1497334703128),
(60, 51, 52, 'Batagor', 21, 'BuburKuningan', '081296363064', 'rudianabinkasmad@gmail.com', 1, '16000.00', 1497344999482, 1497344999482);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` bigint(11) NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `username` varchar(20) CHARACTER SET utf8 NOT NULL,
  `email` varchar(50) CHARACTER SET utf8 NOT NULL,
  `password` text CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `username`, `email`, `password`) VALUES
(1, 'Admin Jual Beli', 'admin', 'qomarullah.mail@gmail.com', '0192023a7bbd73250516f069df18b500');

-- --------------------------------------------------------

--
-- Table structure for table `user_merchant`
--

CREATE TABLE `user_merchant` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_merchant`
--

INSERT INTO `user_merchant` (`id`, `username`, `phone`, `email`, `password`) VALUES
(2, 'test', 'qomar@gmail.com', 'user_markeet@gmail.com', 'ee11cbb19052e40b07aac0ca060c23ee'),
(5, 'admin', '08118003585', 'qomarullah.mail@gmail.comx', '0192023a7bbd73250516f069df18b500'),
(6, 'mttpreneurship', '081285688212', 'mttpreneurship@gmail.com', '46c5451690d67cae6a4c5c4384f8486f'),
(16, 'ok', '0811800358883', 'qomarullahjsjjs@jsjjs', 'e3a36dae0fe547beb0c3e470ec5f69a6'),
(18, 'user', '6285322881993', 'qomarullah.mail@gmail.com', '29e3bb7ccc535f829fcbd5604b596089'),
(19, 'lukiisw', '0811917674', 'luki.iswara2010@gmail.com', 'd2be10e091562cad850e9701ab9dc4e0'),
(20, 'mapri', '081399848387', 'mapri.new@gmail.com', 'd3ec549861b667962a6f96cf66cb9990'),
(21, 'BuburKuningan', '081296363064', 'rudianabinkasmad@gmail.com', 'c94fd0ca8f2ce10778f44fe96d5fa604'),
(22, 'betha', '08111987781', 'betha_r_yulianto@telkomsel.co.id', 'efa533f214ddcdb52d1c6d96c7c513c4'),
(23, 'JowvyKum', '0811412344', 'jowvy_kumala@telkomsel.co.id', '612f2291963cf47efc97b5dc7ed266b6'),
(24, 'furqon', '0811646446', 'furqon.ahkamy@gmail.com', '9937fa0084be826cde4ab55f125e2706'),
(25, 'AMstore', '0811700360', 'taofik.haryanto@gmail.com', 'ca9637601eadbb7971821668113b7586'),
(26, 'mfmahdi', '0811806256', 'mfmahdi@gmail.com', '2c43ef84af60fa45e64240f0ad119038'),
(27, 'iDew', '08119629991', 'dewi_at_putri@telkomsel.co.ud', '36e498f319d7ca06e8969ba1fee3c142'),
(28, 'abe', '0811530084', 'javac_abe@yahoo.com', 'b45746b95e3ca1a2486ad63222c37c4b'),
(29, 'adhtie', '082188889472', 'adhtiekha@gmail.com', '207cf643f255fe8e7a556f46e295b432'),
(30, 'Megabintar', '085322536906', 'mega.putriana@gmail.com', 'b4a19a9fc334ee64ca0614e91fe5f21e'),
(35, 'miaQBooth', '0811983973', 'miaamalia@gmail.com', '586b66de2f1ba8b0f7f21331a6046c71'),
(36, 'Deby Sandra Putri', '0811149712', 'debysandraputri@gmail.com', '0b779014cd08a5f875a0678a3c702a47'),
(37, 'zaraqu', '0811830500', 'faldian2000@gmail.com', 'b24a94e6a7626d4451084846fd21d9e9'),
(38, 'abahalek', '0811871434', 'alekpn@gmail.com', 'fc788d57dc97a2e1e55ebd6c2dfd8471'),
(39, 'Fajarfe', '0811923366', 'fajar_fe_haryanto@telkomsel.co.id', '16d222a5fac9d8d7a3ee790f9ee46af5'),
(40, 'Riswan', '0811970399 ', 'riswan@telkomsel.co.id', '1892a43973bfb5fac054a197911e0023'),
(41, 'Sholehhudin', '08111987745', 'sholehhudin@gmail.com', 'dd2a913c637472f7f2cdf9730aed3ac4'),
(43, 'wandisaputra', '081283202012', 'wandi.saputra1985@gmail.com', '080c7c2faa9cccf1a8cb4f8b9c90cd58'),
(45, 'als', '0811978464', 'jerman90@gmail.com', '0d645d0dfa437e95e94228b047fa3e9d'),
(46, 'wawanset', '0811187372', 'setiawan.wawan@gmail.com', 'ccfcbcb802b5436158b775207cd6a849'),
(50, '', '+62811300890', 'arikademaya@gmail.com', '87fd14420279e90619cd0f609d83d3db'),
(54, 'arikamaya', '0811300890', 'arika_d_maya@telkomsel.co.id', '67baca674578a741c37c78e30e566c37'),
(55, 'Cheeqa', '0811414411', 'cheeqa@gmail.com', '1a137c4c35b41267c58fba8307619727'),
(58, 'acep1013', '08111177755', 'acepsugianto8@gmail.com', '12c6fbf08ff901cc0c783ea25c5cf9f0'),
(59, 'soekarnosan', '08119953080', 'soekarnosan@gmail.com', 'ab950d123f5e75b5be8b8780a4167b06'),
(60, 'dianck', '0811888204', 'dianck2002@gmail.com', '0786a6c27e40d5f6c0f8af2fbe6939ce'),
(65, 'Verdania Puri', '0811101738', 'verdania83@gmail.com', '63d3a794e88997f1bf884668c7a42965'),
(66, 'chacha', '082113655655', 'rizcha.87@gmail.com', 'c33367701511b4f6020ec61ded352059'),
(67, 'imamroh', '08118705533', 'imam_rohendi@telkomsel.co.id', '5c25d20e275385468f5b70599856ab2b'),
(68, 'diakbar', '08118135416', 'adhitya.akbar@gmail.com', 'a6e4b235a89d8ca4721c3424e164d7cb'),
(69, 'kraukk', '085695856155', 'kraukk.com@gmail.com', '96e79218965eb72c92a549dd5a330112'),
(70, 'aomrochman', '08119510106', 'saefullohrochman@gmail.com', '4a6a329c106ba24c46356d8fe2a3fa79'),
(71, 'ariffir', '081311660223', 'firmansyah2308@gmail.com', 'e10adc3949ba59abbe56e057f20f883e'),
(72, 'aquila', '0811987712', 'avindoym@gmail.com', '4f96de345f5dda75503f5355d6f7e4b9'),
(73, 'moretea', '0811916446', 'imanuel@telkomsel.co.id', '26a618681be56824fa07355355dcaf69'),
(74, 'Nanaari', '08111987786', 'Nana_d_arianti@telkomsel.co.id', 'b8bcec03495cf39f3b0af1a4a95ca5e5'),
(75, 'BaksoExmud', '0811100033', 'purwadi.hari@yahoo.com', 'd316f1b34717f63bf6a5938f2f91a95d'),
(76, 'pakkarasa', '0811100677', 'kardijarot21@gmail.com', '84178a2f7bfeb1ec97a7178228e7a794'),
(77, 'Juicefriend', '0811101696', 'juicefriend.com@gmail.com', '92e98df77fc911bfd81d2d776c9694c8'),
(79, 'dennyabe', '08119309007', 'dennyabe@yahoo.com', '70b3c7860de673c6e66882d0e1a3c6f5'),
(81, 'Bin jannah', '08111802753', 'ridarnasari0972@gmail.com', 'a8c66843d2688c364f36967fb5faf79f');

-- --------------------------------------------------------

--
-- Table structure for table `user_merchant_google`
--

CREATE TABLE `user_merchant_google` (
  `id` int(11) NOT NULL,
  `oauth_provider` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `oauth_uid` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `locale` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `picture` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_merchant_google`
--

INSERT INTO `user_merchant_google` (`id`, `oauth_provider`, `oauth_uid`, `first_name`, `last_name`, `email`, `gender`, `locale`, `picture`, `link`, `created`, `modified`) VALUES
(1, 'google', '104261500942024268827', 'qomarullah', 'qomarullah', 'qomarullah.mail@gmail.com', 'male', 'in', 'https://lh6.googleusercontent.com/-3VwzkxjTI5s/AAAAAAAAAAI/AAAAAAAAAL0/Wp-Jy2gfGcg/photo.jpg', 'https://plus.google.com/104261500942024268827', '2017-05-19 15:33:57', '2017-05-19 23:11:28');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `app_version`
--
ALTER TABLE `app_version`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique` (`name`);

--
-- Indexes for table `config`
--
ALTER TABLE `config`
  ADD PRIMARY KEY (`code`);

--
-- Indexes for table `currency`
--
ALTER TABLE `currency`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fcm`
--
ALTER TABLE `fcm`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `merchant`
--
ALTER TABLE `merchant`
  ADD KEY `id` (`id`);

--
-- Indexes for table `news_info`
--
ALTER TABLE `news_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `product_unique_name` (`name`);

--
-- Indexes for table `product_category`
--
ALTER TABLE `product_category`
  ADD KEY `fk_product_category_1` (`product_id`),
  ADD KEY `fk_product_category_2` (`category_id`);

--
-- Indexes for table `product_image`
--
ALTER TABLE `product_image`
  ADD KEY `fk_table_images` (`product_id`);

--
-- Indexes for table `product_order`
--
ALTER TABLE `product_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_order_detail`
--
ALTER TABLE `product_order_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_table_orders_item` (`order_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_username` (`username`),
  ADD UNIQUE KEY `unique_email` (`email`);

--
-- Indexes for table `user_merchant`
--
ALTER TABLE `user_merchant`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `user_merchant_google`
--
ALTER TABLE `user_merchant_google`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `app_version`
--
ALTER TABLE `app_version`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `currency`
--
ALTER TABLE `currency`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=165;
--
-- AUTO_INCREMENT for table `fcm`
--
ALTER TABLE `fcm`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=201;
--
-- AUTO_INCREMENT for table `merchant`
--
ALTER TABLE `merchant`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `news_info`
--
ALTER TABLE `news_info`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=111;
--
-- AUTO_INCREMENT for table `product_order`
--
ALTER TABLE `product_order`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;
--
-- AUTO_INCREMENT for table `product_order_detail`
--
ALTER TABLE `product_order_detail`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `user_merchant`
--
ALTER TABLE `user_merchant`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;
--
-- AUTO_INCREMENT for table `user_merchant_google`
--
ALTER TABLE `user_merchant_google`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `product_category`
--
ALTER TABLE `product_category`
  ADD CONSTRAINT `fk_product_category_1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_product_category_2` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_image`
--
ALTER TABLE `product_image`
  ADD CONSTRAINT `fk_table_images` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_order_detail`
--
ALTER TABLE `product_order_detail`
  ADD CONSTRAINT `fk_table_orders_item` FOREIGN KEY (`order_id`) REFERENCES `product_order` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
