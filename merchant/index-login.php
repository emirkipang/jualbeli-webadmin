<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=0.5">

    <meta name="description" content="">
    <meta name="author" content="">

    <title>Penjualan</title>

    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Theme CSS -->
    <link href="css/freelancer.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top" class="index">
    

<?php
//Include GP config file && User class
include_once 'gpConfig.php';
include_once 'User.php';

if(isset($_GET['code'])){
	$gClient->authenticate($_GET['code']);
	$_SESSION['token'] = $gClient->getAccessToken();
	header('Location: ' . filter_var($redirectURL, FILTER_SANITIZE_URL));
}

if (isset($_SESSION['token'])) {
	$gClient->setAccessToken($_SESSION['token']);
}

if ($gClient->getAccessToken()) {
	//Get user profile data from google
	$gpUserProfile = $google_oauthV2->userinfo->get();
	
	//Initialize User class
	$user = new User();
	
	//Insert or update user data to the database
    $gpUserData = array(
        'oauth_provider'=> 'google',
        'oauth_uid'     => $gpUserProfile['id'],
        'first_name'    => $gpUserProfile['given_name'],
        'last_name'     => $gpUserProfile['family_name'],
        'email'         => $gpUserProfile['email'],
        'gender'        => $gpUserProfile['gender'],
        'locale'        => $gpUserProfile['locale'],
        'picture'       => $gpUserProfile['picture'],
        'link'          => $gpUserProfile['link']
    );
    $userData = $user->checkUser($gpUserData);
	
	//Storing user data into session
	$_SESSION['userData'] = $userData;
	
	//Render facebook profile data
    if(empty($userData)){
       $output = '<h3 style="color:red">Some problem occurred, please try again.</h3>';
		
    }else{
        
			$page=$_REQUEST['page'];
			$table=$_REQUEST['table'];
			
			
			/* $output = '<h1>Google+ Profile Details </h1>';
			$output .= '<img src="'.$userData['picture'].'" width="300" height="220">';
			$output .= '<br/>Google ID : ' . $userData['oauth_uid'];
			$output .= '<br/>Name : ' . $userData['first_name'].' '.$userData['last_name'];
			$output .= '<br/>Email : ' . $userData['email'];
			$output .= '<br/>Gender : ' . $userData['gender'];
			$output .= '<br/>Locale : ' . $userData['locale'];
			$output .= '<br/>Logged in with : Google';
			$output .= '<br/><a href="'.$userData['link'].'" target="_blank">Click to Visit Google+ Page</a>';
			$output .= '<br/>Logout from <a href="logout.php">Google</a>';  */

			header_logged();
				
			if($page=="")$page="item";	
			if($page!=""){
				
				echo "<section id=\"content\">
					<div class=\"container\">";
					
				include "pages/".$page.".php";
			
				echo "</div></section>";
				
			}else{
				include "pages/default.php";
				echo "<script src=\"vendor/jquery/jquery.min.js\"></script>";
				include "pages/default.php";
			}
	
		}
	} else {
		header_login();
		$authUrl = $gClient->createAuthUrl();
		$output = '<a href="'.filter_var($authUrl, FILTER_SANITIZE_URL).'"><img src="img/glogin.png" alt=""/></a>';
		body_login($output);
	}

?>


<?php
echo "<script src=\"vendor/bootstrap/js/bootstrap.min.js\"></script>";
?>
	
    <!-- Footer -->
    <footer class="text-center">
        <div class="footer-below">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        Copyright &copy; MTT 2017
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <!-- jQuery -->
<!--    <script src="vendor/jquery/jquery.min.js"></script>
-->
    <!-- Bootstrap Core JavaScript -->
    <!--<script src="vendor/bootstrap/js/bootstrap.min.js"></script>-->

    <!-- Plugin JavaScript -->
    <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
-->
    <!-- Contact Form JavaScript -->
   <!-- <script src="js/jqBootstrapValidation.js"></script>
    <script src="js/contact_me.js"></script>
-->
    <!-- Theme JavaScript -->
    <!--<script src="js/freelancer.min.js"></script-->

</body>

</html>

<?php
function header_login(){
	
echo "<!-- Navigation -->
    <nav id=\"mainNav\" class=\"navbar navbar-default navbar-fixed-top navbar-custom\">
        <div class=\"container\">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class=\"navbar-header page-scroll\">
                <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-1\">
                    <span class=\"sr-only\">Toggle navigation</span> Menu <i class=\"fa fa-bars\"></i>
                </button>
                <a class=\"navbar-brand\" href=\"index.php\">Jual Beli</a>
            </div>

            
        </div>
        <!-- /.container-fluid -->
    </nav>";
}
function header_logged(){
	
echo "<!-- Navigation -->
    <nav id=\"mainNav\" class=\"navbar navbar-default navbar-fixed-top navbar-custom\">
        <div class=\"container\">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class=\"navbar-header page-scroll\">
                <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-1\">
                    <span class=\"sr-only\">Toggle navigation</span> Menu <i class=\"fa fa-bars\"></i>
                </button>
                <a class=\"navbar-brand\" href=\"index.php\">Jual Beli</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">
                <ul class=\"nav navbar-nav navbar-right\">
                    <li class=\"hidden\">
                        <a href=\"#page-top\"></a>
                    </li>
                    <li class=\"page-scroll\">
                        <a href=\"index.php?page=item\">Produk Kamu</a>
                    </li>
                    <li class=\"page-scroll\">
                        <a href=\"index.php?page=order\">Order Masuk</a>
                    </li>
					 <li class=\"page-scroll\">
                        <a href=\"logout.php\">Logout</a>
                    </li>
                    
                </ul>

				
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>";
}

function body_login($output){
	echo "<header>
	<div class=\"container\" id=\"maincontent\" tabindex=\"-1\">
		<div class=\"row\">
			<div class=\"col-lg-12\">
				<img class=\"img-responsive\" src=\"img/profile.png\" alt=\"\">
			   <div class=\"intro-text\">
                        <hr class=\"star-light\">
                        <span class=\"skills\">Disini kamu bisa menjual produk yang nanti akan ditampilkan di aplikasi \"jual beli\"</span>
						<br>".$output."
                    </div>
				</div>
			</div>
		</div>
	</header>";

}
?>