Project ini berisi :

/admin untuk web admin dan api

/mechant untuk pejual

Guide buat development

1. install xampp di windows, versi v3.2.2
2. git clone repo jualbeli-webadmin dibawah path htdocs
3. export dan import db, ambil dari server http://128.199.190.210/pma
4. pastikan running di http://localhost/   
5. set konfigurasi db admin/services/conf.php
6. sample cara buat API untuk login merchant
	a. add/edit file services/API.php
	
	```
	require_once("table/UserMerchant.php");
	```
	
	```
	private $user_merchant = NULL;
	```
	
	```
	$this->user_merchant = new UserMerchant($this->db);
	```
	
	```
	private function loginMerchant(){
		$this->user_merchant->processLogin();
	}
	```
	
	b. add/edit file admin/services/table/UserMerchant.php
	```
	public function processLogin(){
		if($this->get_request_method() != "GET") $this->response('',406);
		$username = $customer["username"];
		$password = $customer["password"]; */
		$username = $this->_request['username'];
		$password = $this->_request['password'];
		....
		
		}
	```
	c. test di localhost
	```
	http://localhost/jualbeli-webadmin/admin/services/loginMerchant?username=admin&password=admin123
	```
	response api harus json
	```
	{"status":"success","user":{"id":5,"username":"admin","phone":8118003585,"email":"qomarullah.mail@gmail.comxx","password":"0192023a7bbd73250516f069df18b500","google_id":null,"google_email":null,"google_photo":null,"fb_id":null,"fb_email":null,"fb_photo":null,"last_update":null}}
	```
	
	d. lakukan git push, agar bs ditest lewat android 
	- git fetch/pull (ambil latest update, pastikan conflict aman)
	- git add . (add file ke stage commit)
	- git commit -m "fixing login merchant" (komentar perubahan)
	- git push
	
	e. agar file di server juga update, lakukan git pull dr server
	- login 128.199.190.210
	- cd /apps/www/jualbeli-webadmin
	- git pull
	
	f. test login dari android

7. Sample consume API di android
	a. add/edit file Activity, sample call api 
	
	```
	private void requestUser(final String username, final String password ) {
        API api = RestAdapter.createAPI();
        Call<CallbackUser> callbackCall = api.getUser(username, password); //methode yg harus dibuat di connection/API.java
        callbackCall.enqueue(new Callback<CallbackUser>() {
            @Override
            public void onResponse(Call<CallbackUser> call, Response<CallbackUser> response) {
                CallbackUser resp = response.body();
                if (resp != null && resp.status.equals("success") && resp.user!= null) {
                    UserProfile userProfile = sharedPref.setInfoUser(resp.user);

                    showProgress(false);
                    Intent i = new Intent(ActivityLogin.this, ActivityProfile.class);
                    startActivity(i);
                    ThisApplication.getInstance().obtainFirebaseToken();
					finish();

                }else if (resp != null && resp.status.equals("failed") && resp.user!= null) {
                    showProgress(false);
                    Snackbar.make(parent_view, R.string.incorrect_password, Snackbar.LENGTH_SHORT).show();
                    return;
                }else {
                    showProgress(false);
                    Snackbar.make(parent_view, R.string.invalid_login , Snackbar.LENGTH_SHORT).show();
                    return;
                }

            }

            @Override
            public void onFailure(Call<CallbackUser> call, Throwable t) {
                Log.e("onFailure", t.getMessage());
                dialogServerNotConnect();
                showProgress(false);

            }
        });
    }
	```
	
	b. Add/edit file yang dibutuhkan
	- edit connection/API.java untuk menambahkan fungsi api.getUser(username, password); 
		```
		@Headers({CACHE, AGENT})
    @GET("services/loginMerchant")
    Call<CallbackUser> getUser(
            @Query("username") String username,
            @Query("password") String password

            );
			
		```
		
	- Add/edit connection/callback/CallbackUser.java untuk consume model response API yg dihit
	
	```
	CallbackUser resp = response.body(); (fungsi dari call api)
	```
	```
	public class CallbackUser implements Serializable {
		public String status = "";
		public UserProfile user = null;
	}
	```
	response json "user" akan dimapping ke model UserProfile
	
	- Add/edit file untuk model UserProfile
	pastikan response API harus mengikuti model UserProfile ini
	
	respAPI
	```
	//inside userwwww
	{"status":"success",	"user":{"id":5,"username":"admin","phone":8118003585,"email":"qomarullah.mail@gmail.com","password":"0192023a7bbd73250516f069df18b500","google_id":null,"google_email":null,"google_photo":null,"fb_id":null,"fb_email":null,"fb_photo":null,"last_update":null}
	}
	```
	
	```
	//file mode/UserProfile.java
	public class UserProfile implements Serializable {

		public String id;
		public String name;
		public String username;
		public String email;
		public String password;
		public String phone;
		public String address;
		public String google_id;
		public String google_email;
		public String google_photo;
		public String fb_id;
		public String fb_email;
		public String fb_photo;
	}
	c. untuk tracing bisa menggunakan logger Log.d atau Log.e 
	
8. Selamat mencoba :D