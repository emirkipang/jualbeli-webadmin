<?php
require_once(realpath(dirname(__FILE__) . "/../tools/rest.php"));
require_once(realpath(dirname(__FILE__) . "/../conf.php"));

class UserMerchant extends REST{
 
	private $mysqli = NULL;
	private $db = NULL;
	
	public function __construct($db) {
		parent::__construct();
		$this->db = $db;
		$this->mysqli = $db->mysqli;
		$this->conf = new CONF(); // Create conf class
    }
	
	// security for filter manipulate data		
	public function checkAuthorization(){
		$resp = array("status" => 'Failed', "msg" => 'Unauthorized' );
		if(isset($this->_header['Token']) && !empty($this->_header['Token'])){
			$token = $this->_header['Token'];
			$query = "SELECT id FROM user_merchant WHERE password='$token' ";
			$r = $this->mysqli->query($query) or die($this->mysqli->error.__LINE__);
			if($r->num_rows < 1) {
				$this->show_response($resp);
			}
		} else {
			$this->show_response($resp);
		}
	}		

	public function processLogin(){
		if($this->get_request_method() != "GET") $this->response('',406);
		/* $customer = json_decode(file_get_contents("php://input"),true);
		$username = $customer["username"];
		$password = $customer["password"]; */
		$username = $this->_request['username'];
		$password = $this->_request['password'];
		
		
		if(!empty($username) and !empty($password)){ // empty checker
			$query="SELECT * FROM user_merchant WHERE password = '".md5($password)."' AND (username = '$username' or email='$username') LIMIT 1";
			$r = $this->mysqli->query($query) or die($this->mysqli->error.__LINE__);
			
			if($r->num_rows > 0) {
				$result = $r->fetch_assoc();
			    $resp = array('status' => "success", "user" => $result);
				$this->show_response($resp);
			}
			$error = array('status' => "failed", "msg" => "Username or Password not found");
			$this->show_response($error);
		}
		$error = array('status' => "failed", "msg" => "Invalid username or Password");
		$this->show_response($error);
	}

	public function findOne(){
		if($this->get_request_method() != "GET") $this->response('',406);
		$id = (int)$this->_request['id'];
		$query="SELECT id, name, username, email FROM user_merchant WHERE id=$id";
		$this->show_response($this->db->get_one($query));
	}

	public function updateOne(){
		if($this->get_request_method() != "POST") $this->response('',406);
		if($this->conf->DEMO_VERSION){
			$m = array('status' => "failed", "msg" => "Ops, this is demo version", "data" => null);
			$this->show_response($m);
		}
		$user = json_decode(file_get_contents("php://input"),true);
		if(!isset($user['id'])) $this->responseInvalidParam();
		$id = (int)$user['id'];
		$password = $user['user']['password'];
		if($password == '*****'){
			$column_names = array('id', 'name', 'username', 'email');
		}else{
			$user['user']['password'] = md5($password);
			$column_names = array('id', 'name', 'username', 'email', 'password');
		}
		$table_name = 'user';
		$pk = 'id';
		$resp = $this->db->post_update($id, $user, $pk, $column_names, $table_name);
		$this->show_response($resp);
	}

	/* public function insertOne(){
		if($this->get_request_method() != "POST") $this->response('',406);
		if($this->conf->DEMO_VERSION){
			$m = array('status' => "failed", "msg" => "Ops, this is demo version", "data" => null);
			$this->show_response($m);
		}
		$user = json_decode(file_get_contents("php://input"),true);
		$user['password'] = md5($user['password']);
		$column_names = array('name', 'username', 'email', 'password');
		$table_name = 'user';
		$pk = 'id';
		$resp = $this->db->post_one($user, $pk, $column_names, $table_name);
		$this->show_response($resp);
	} */	
	
	public function insertOne(){
		if($this->get_request_method() != "GET") $this->response('',406);
		if($this->conf->DEMO_VERSION){
			$m = array('status' => "failed", "msg" => "Ops, this is demo version", "data" => null);
			$this->show_response($m);
		}
		
		//$user = json_decode(file_get_contents("php://input"),true);
		$user['username'] = $this->_request['username'];
		$user['phone']  = $this->_request['phone'];
		$user['email']  = $this->_request['email'];
		$password = $this->_request['password'];
		
		$query="SELECT id FROM user_merchant WHERE username='".$user['username']."' or email='".$user['email']."'";
		$resp=$this->db->get_one($query);
		
		//print_r($resp);
		if(sizeof($resp)>0){
			$m = array('status' => "failed", "msg" => "Username or email sudah terdaftar", "data" => null);
			$this->show_response($m);
			
		}else{
			
			$user['password'] = md5($password);
			$column_names = array('username', 'phone', 'email', 'password');
			$table_name = 'user_merchant';
			$pk = 'id';
			$resp = $this->db->post_one($user, $pk, $column_names, $table_name);
			$this->show_response($resp);
		}
	}	
	
	public function resetOne(){
		if($this->get_request_method() != "GET") $this->response('',406);
		if($this->conf->DEMO_VERSION){
			$m = array('status' => "failed", "msg" => "Ops, this is demo version", "data" => null);
			$this->show_response($m);
		}
		$user['email'] = $this->_request['email'];
		
		$query="SELECT id, username FROM user_merchant WHERE email='".$user['email']."'";
		$resp=$this->db->get_one($query);
		if(sizeof($resp)>0){
			
			$password = $this->randomPassword();
			$user['user']['password'] = md5($password);
			//$user['id']=$resp['id'];
			$id = (int)$resp['id'];
			$username=$resp['username'];
			
			$column_names = array('password');
			$table_name = 'user';
			$pk = 'id';
			$resp = $this->db->post_update($id, $user, $pk, $column_names, $table_name);
			$this->show_response($resp);
			
			$this->resetViaEmail($username, $email, $password);
				
		}else{
			$m = array('status' => "failed", "msg" => "Email not found", "data" => null);
			$this->show_response($m);
		}
		
	}
	public function randomPassword() {
		$alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
		$pass = array(); //remember to declare $pass as an array
		$alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
		for ($i = 0; $i < 8; $i++) {
			$n = rand(0, $alphaLength);
			$pass[] = $alphabet[$n];
		}
		return implode($pass); //turn the array into a string
	}
	
	public function resetViaEmail($userid, $email,$newpass){
		// Always set content-type when sending HTML email
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

		// More headers
		$headers .= 'From: <Admin@market.mtt.or.id>' . "\r\n";
		
		$msg ="<html><body>";
		$msg .= "<h3>Dear Merchant Jual Beli,</h3>";
		$msg .="<br>";
		$msg .= "Terima kasih sudah menjadi merchant kami, berikut ini informasi akun kamu:";
		$msg .="<br>";
		$msg .= "<table rules=\"all\" style=\"border-color: #666;\" cellpadding=\"10\">";
		$msg .= "<tr style=\"background: #eee;\"><td><strong>Username:</strong> </td><td>" . $userid . "</td></tr>";
		$msg .= "<tr style=\"background: #eee;\"><td><strong>Email:</strong> </td><td>" . $email . "</td></tr>";
		$msg .= "<tr style=\"background: #eee;\"><td><strong>Password:</strong> </td><td>" . $newpass. "</td></tr>";
		$msg .= "</table>";
		$msg .="<br>";
		$msg .="<br>";
		$msg .="Salam,";
		$msg .="<br>";
		$msg .="Admin Jual Beli";
		$msg .="</body><html>";
		
		// use wordwrap() if lines are longer than 70 characters
		$msg = wordwrap($msg,70);

		// send email
		mail($email,"Jual Beli : Reset Password", $msg, $headers);

	}
}	
?>
